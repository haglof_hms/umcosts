// PricelistsListFrameView.cpp : implementation file
//

#include "stdafx.h"
#include "MDICostsFormView.h"
#include "MDICostTmplListFrameView.h"

#include "ResLangFileReader.h"

#include "UMCostsTmplDB.h"

// CCostTmplListFrameView

IMPLEMENT_DYNCREATE(CCostTmplListFrameView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCostTmplListFrameView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_ERASEBKGND()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, IDC_TEMPLATE_LIST, OnReportClick)
END_MESSAGE_MAP()

CCostTmplListFrameView::CCostTmplListFrameView()
	: CXTResizeFormView(CCostTmplListFrameView::IDD)
{
	m_bInitialized = FALSE;
}

CCostTmplListFrameView::~CCostTmplListFrameView()
{
	setupDoPopulate();

	CXTResizeFormView::OnClose();
}

void CCostTmplListFrameView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP

}

void CCostTmplListFrameView::OnClose()
{
}

BOOL CCostTmplListFrameView::OnEraseBkgnd(CDC *pDC)
{

	CRect clip;
	m_wndTemplates.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_BTNFACE));

	return FALSE;
}

void CCostTmplListFrameView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTemplates.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTemplates,1,1,rect.right - 1,rect.bottom - 2);
	}
	
}

BOOL CCostTmplListFrameView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CCostTmplListFrameView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (!	m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		m_sAbrevLangSet = getLangSet();
		CMDICostsFormView *pView = (CMDICostsFormView *)getFormViewByID(IDD_FORMVIEW);
		if (pView)
		{
			m_nDBIndex = pView->getDBIndex();
		}

		if (setupReport())
		{
			getTemplatesFromDB();
			populateData();
		}

		m_bInitialized = TRUE;
	}	// if (!	m_bInitialized )

}

BOOL CCostTmplListFrameView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CCostTmplListFrameView diagnostics

#ifdef _DEBUG
void CCostTmplListFrameView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CCostTmplListFrameView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// Protected
void CCostTmplListFrameView::setupDoPopulate(void)
{
	CMDICostsFormView *pView = (CMDICostsFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pView)
	{
		pView->doPouplate(m_nDBIndex);
	}
}

// Create and add PricelistsList reportwindow
BOOL CCostTmplListFrameView::setupReport(void)
{

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (m_wndTemplates.GetSafeHwnd() == 0)
	{
		// Create the Reportcontrol
		if (!m_wndTemplates.Create(this, IDC_TEMPLATE_LIST ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndTemplates.GetSafeHwnd() != NULL)
				{

					m_wndTemplates.ShowWindow( SW_NORMAL );
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING500)), 100));
					pCol->AllowRemove(FALSE);
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING600)), 100));
					pCol = m_wndTemplates.AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING700)), 100));

					m_wndTemplates.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndTemplates.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndTemplates.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndTemplates.GetReportHeader()->SetAutoColumnSizing( TRUE );
					m_wndTemplates.SetMultipleSelection( FALSE );
					m_wndTemplates.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndTemplates.AllowEdit(FALSE);
					m_wndTemplates.FocusSubItems(TRUE);

					RedrawWindow(0,0, RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE | RDW_ALLCHILDREN);

					RECT rect;
					GetClientRect(&rect);
					// resize window = display window in tab; 060327 p�d
					setResize(&m_wndTemplates,1,30,rect.right - 1,rect.bottom - 30);

				}	// if (m_wndTemplates.GetSafeHwnd() != NULL)

			}	// if (xml->Load(m_sLangFN))
			delete xml;

	}	// if (fileExists(m_sLangFN))

	return TRUE;

}

BOOL CCostTmplListFrameView::populateData(void)
{
	CTransaction_costtempl tmplSel;
	CXTPReportRecord *pRec;
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			CTransaction_costtempl tmpl = m_vecTransaction_costtempl[i];
			if (i == m_nDBIndex)
				pRec = m_wndTemplates.AddRecord(new CTemplateListReportDataRec(i+1,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
			else
				m_wndTemplates.AddRecord(new CTemplateListReportDataRec(i+1,tmpl.getTemplateName(),tmpl.getCreatedBy(),tmpl.getCreated()));
		}
		m_wndTemplates.Populate();
		m_wndTemplates.UpdateWindow();
		if (pRec != NULL)
		{
			CXTPReportRow *pRow = m_wndTemplates.GetRows()->Find(pRec);
			if (pRow != NULL)
			{
				m_wndTemplates.SetFocusedRow(pRow);
			}	// if (pRow != NULL)
		}	// if (pRec != NULL)

		return TRUE;
	}
	return FALSE;
}

void CCostTmplListFrameView::getTemplatesFromDB(void)
{
	if (m_bConnected)
	{
		CUMCostsTmplDB *pDB = new CUMCostsTmplDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			m_bConnected = pDB->getCostTmpls(m_vecTransaction_costtempl,COST_TYPE_1);
			delete pDB;
		}
	}
}

// CCostTmplListFrameView message handlers

void CCostTmplListFrameView::OnReportClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;

	if (m_wndTemplates.GetSafeHwnd() == NULL)
		return;

	if (pItemNotify->pRow)
	{
		CTemplateListReportDataRec *pRec = (CTemplateListReportDataRec *)pItemNotify->pItem->GetRecord();
		if (pRec)
		{
			m_nDBIndex = pRec->GetIndex();
			setupDoPopulate();
		}

	}
	*pResult = 0;
}
