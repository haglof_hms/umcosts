
#include "StdAfx.h"
#include "CostsFrame.h"
#include "MDICostsFormView.h"
#include "MDIInfrCostsFormView.h"

#include "ResLangFileReader.h"

#include "PrlViewAndPrint.h"

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
	// TODO: add one-time construction code here

}

CMDIFrameDoc::~CMDIFrameDoc()
{
}


BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDICostsFormFrame

IMPLEMENT_DYNCREATE(CMDICostsFormFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICostsFormFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICostsFormFrame)
	ON_WM_SYSCOMMAND()
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
//	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CMDICostsFormFrame construction/destruction

XTPDockingPanePaintTheme CMDICostsFormFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDICostsFormFrame::CMDICostsFormFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bConnected = FALSE;
}

CMDICostsFormFrame::~CMDICostsFormFrame()
{
}

void CMDICostsFormFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_TEMPLATE_ENTRY_KEY);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;
}

void CMDICostsFormFrame::OnClose(void)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
/*
	CMDICostsFormView *pView = (CMDICostsFormView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->isHasDataChanged();
	}
*/
	CMDIChildWnd::OnClose();
}

void CMDICostsFormFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	BOOL bOKToClose = TRUE;
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CMDICostsFormView *pView = (CMDICostsFormView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			bOKToClose =	pView->isOKToClose();
		}
		// Just save data for active Template, before closing; 071010 p�d
		if (bOKToClose)
		{
				CMDIChildWnd::OnSysCommand(nID,lParam);
		}	// if (saveCostTemplateToDB() == -1)
	}
	else
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

int CMDICostsFormFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	EnableDocking(CBRS_ALIGN_ANY);

	// Send message to HMSShell, please send back the DB connection; 070430 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CMDICostsFormFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return FALSE;
}

BOOL CMDICostsFormFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDICostsFormFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDICostsFormFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDICostsFormFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_TEMPLATE_ENTRY_KEY);
		LoadPlacement(this, csBuf);

		// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
		// with ID_LPARAM_COMMAND2.
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);
  }
}

void CMDICostsFormFrame::OnSetFocus(CWnd* pWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICostsFormFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW1,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}
/*
LRESULT CMDICostsFormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}
*/

// CMDICostsFormFrame diagnostics

#ifdef _DEBUG
void CMDICostsFormFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICostsFormFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDICostsFormFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CMDICostsFormFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICostsFormFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// PRIVATE
void CMDICostsFormFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
		}	// if (xml->Load(sLangFN))
		xml.clean();
	}	// if (fileExists(sLangFN))

}

// CMDICostsFormFrame message handlers



/////////////////////////////////////////////////////////////////////////////
// CMDICostsListFrame


IMPLEMENT_DYNCREATE(CMDICostsListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICostsListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICostsListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICostsListFrame::CMDICostsListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDICostsListFrame::~CMDICostsListFrame()
{
}

void CMDICostsListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_TEMPLATE_LIST_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

int CMDICostsListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDICostsListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICostsListFrame diagnostics

#ifdef _DEBUG
void CMDICostsListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICostsListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDICostsListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDICostsListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDICostsListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_TEMPLATE_LIST_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICostsListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDICostsListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICostsListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICostsListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

// MY METHODS
void CMDICostsListFrame::setLanguage()
{
}


/////////////////////////////////////////////////////////////////////////////
// CMDIInfrCostsFormFrame

IMPLEMENT_DYNCREATE(CMDIInfrCostsFormFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIInfrCostsFormFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIInfrCostsFormFrame)
	ON_WM_SYSCOMMAND()
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_COMMAND(ID_TBTN_IMPORT, OnImportTBtnClick)
	ON_COMMAND(ID_TBTN_EXPORT, OnExportTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_EXPORT, OnUpdateExportTBtn)
	ON_COMMAND(ID_TBTN_ADD_SPECIES, OnAddSpeciesTBtnClick)
	ON_COMMAND(ID_TBTN_PREVIEW, OnTBtnPreview)

	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD_SPECIES, OnUpdateAddSpeciesTBtn)
	ON_COMMAND(ID_TBTN_DEL_SPECIE, OnDelSpecieTBtnClick)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL_SPECIE, OnUpdateDelSpecieTBtn)
	ON_UPDATE_COMMAND_UI(ID_TBTN_PREVIEW, OnUpdatePreviewTBtn)

//	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CMDIInfrCostsFormFrame construction/destruction

XTPDockingPanePaintTheme CMDIInfrCostsFormFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CMDIInfrCostsFormFrame::CMDIInfrCostsFormFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bConnected = FALSE;
}

CMDIInfrCostsFormFrame::~CMDIInfrCostsFormFrame()
{
}

void CMDIInfrCostsFormFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_INFR_TEMPLATE_ENTRY_KEY);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;
}

void CMDIInfrCostsFormFrame::OnClose(void)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);


	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		//Lagt till en metod f�r att kontrollera om kostnaden ing�r i en best�ndsmall innan man st�nger f�nstret 20110829 J� Bug#2297
		if(pView->IsOkToClose())
			CMDIChildWnd::OnClose();
	}

}

void CMDIInfrCostsFormFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	BOOL bOKToClose = TRUE;
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CMDICostsFormView *pView = (CMDICostsFormView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			bOKToClose =	pView->isOKToClose();
		}
		// Just save data for active Template, before closing; 071010 p�d
		if (bOKToClose)
		{
				CMDIChildWnd::OnSysCommand(nID,lParam);
		}	// if (saveCostTemplateToDB() == -1)
	}
	else
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

int CMDIInfrCostsFormFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	EnableDocking(CBRS_ALIGN_ANY);

	// Send message to HMSShell, please send back the DB connection; 070430 p�d
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 080211 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	setLanguage();

	m_bFirstOpen = TRUE;

	BOOL bWindowsOpen = FALSE;
  CString sDocName = L"", sMsg = L"", sWindows = L"";
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();

	// Loop through doc templates
  POSITION pos = pApp->GetFirstDocTemplatePosition();
  while(pos)
  {
		pTemplate = pApp->GetNextDocTemplate(pos);
    pTemplate->GetDocString(sDocName, CDocTemplate::docName);

    // Check for instances of this document (ignore database manager document)
    POSITION posDOC = pTemplate->GetFirstDocPosition();
    if (posDOC && (sDocName.CompareNoCase(_T("View5000")) == 0 || // UMEstimate
									 sDocName.CompareNoCase(_T("Module5100")) == 0 ||	// UMTemplates (Best�ndsmall)
									 sDocName.CompareNoCase(_T("Module5200")) == 0))	// UMLandValue
    {
			CDocument* pDoc = (CDocument*)pTemplate->GetNextDoc(posDOC);
			sWindows += _T(" * ") + pDoc->GetTitle() + _T("\n");
			bWindowsOpen = TRUE;
    }
  }
  if( bWindowsOpen )
  {
		// Display list of open child windows
     sMsg.Format(_T("%s\n%s\n"),m_sMsgWindowsOpen,sWindows);
     ::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
  }

	return 0; // creation ok
}

int CMDIInfrCostsFormFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return FALSE;
}

BOOL CMDIInfrCostsFormFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CMDIInfrCostsFormFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CMDIChildWnd::OnCopyData(pWnd, pData);
}

void CMDIInfrCostsFormFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIInfrCostsFormFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_INFR_TEMPLATE_ENTRY_KEY);
		LoadPlacement(this, csBuf);

		// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
		// with ID_LPARAM_COMMAND2.
		// The return message holds a _user_msg structure, collected in the
		// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);
  }
}

void CMDIInfrCostsFormFrame::OnSetFocus(CWnd* pWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView *)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->setNavigationButtonsEx();
		pView->doSetNavigationBar();
	}

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIInfrCostsFormFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_FORMVIEW7,m_sLangFN,lParam);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}
/*
LRESULT CMDIInfrCostsFormFrame::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}
*/

// CMDIInfrCostsFormFrame diagnostics

#ifdef _DEBUG
void CMDIInfrCostsFormFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIInfrCostsFormFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIInfrCostsFormFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CMDIInfrCostsFormFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_INFR;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_INFR;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIInfrCostsFormFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CMDIInfrCostsFormFrame::OnUpdateExportTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsImportTBtn );
}

void CMDIInfrCostsFormFrame::OnUpdateAddSpeciesTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsAddSpeciesTBtn );
}

void CMDIInfrCostsFormFrame::OnUpdateDelSpecieTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsDelSpeciesTBtn );
}

void CMDIInfrCostsFormFrame::OnUpdatePreviewTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPreviewTBtn );
}

// PRIVATE
void CMDIInfrCostsFormFrame::setLanguage(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			///////////////////////////////////////////////////////////////////////////
			// Setup icons in toolbar and assign a tooltip
			if (fileExists(sTBResFN))
			{
				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR1)
						{		
							setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml->str(IDS_STRING1400));	//
							setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_EXPORT,xml->str(IDS_STRING1401));	//
							setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_ADD,xml->str(IDS_STRING1402));	//
							setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_DEL,xml->str(IDS_STRING1403));	//
							setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_PREVIEW,xml->str(IDS_STRING1404));	//
						}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())
			}	// if (fileExists(sTBResFN))

			m_sMsgCap  = xml->str(IDS_STRING800);
			m_sMsgWindowsOpen.Format(_T("%s\n%s\n\n"),
					xml->str(IDS_STRING2300),
					xml->str(IDS_STRING2301));


		}	// if (xml->Load(sLangFN))
		delete xml;
	}	// if (fileExists(sLangFN))

}

// CMDIInfrCostsFormFrame message handlers

void CMDIInfrCostsFormFrame::OnImportTBtnClick(void)
{
	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->importTemplate();
	}
}

void CMDIInfrCostsFormFrame::OnExportTBtnClick(void)
{
	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->exportTemplate();
	}
}

void CMDIInfrCostsFormFrame::OnAddSpeciesTBtnClick(void)
{
	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->addSpeciesToTemplate();
	}
}

void CMDIInfrCostsFormFrame::OnDelSpecieTBtnClick(void)
{
	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		pView->delSelectedSpecieInTemplate();
	}
}

void CMDIInfrCostsFormFrame::OnTBtnPreview(void)
{
	CString sName;
	CString sXMLFile;
	CPrlViewAndPrint *pPrintView = NULL;

	CMDIInfrCostsFormView *pView = (CMDIInfrCostsFormView*)getFormViewByID(IDD_FORMVIEW2);
	if (pView != NULL)
	{
		sXMLFile = pView->getCostTmplXML();	
		pPrintView = (CPrlViewAndPrint*)showFormView(IDD_FORMVIEW8,m_sLangFN,(LPARAM)(LPCTSTR)sXMLFile);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMDICostsInfrListFrame


IMPLEMENT_DYNCREATE(CMDICostsInfrListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICostsInfrListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICostsInfrListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICostsInfrListFrame::CMDICostsInfrListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
}

CMDICostsInfrListFrame::~CMDICostsInfrListFrame()
{
}

void CMDICostsInfrListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_INFR_TEMPLATE_LIST_ENTRY_KEY);
	SavePlacement(this, csBuf);
}

int CMDICostsInfrListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString sToolTip1;
	CString sToolTip2;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sAbrevLangSet	= getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDICostsInfrListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICostsInfrListFrame diagnostics

#ifdef _DEBUG
void CMDICostsInfrListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICostsInfrListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDICostsInfrListFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

// PRIVATE
void CMDICostsInfrListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDICostsInfrListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_COSTS_INFR_TEMPLATE_LIST_ENTRY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICostsInfrListFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDICostsInfrListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_LIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_LIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICostsInfrListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICostsInfrListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

// MY METHODS
void CMDICostsInfrListFrame::setLanguage()
{
}
