#pragma once

#include "UMCostsTmplDB.h"

#include "Resource.h"

// CTransportFormView form view

class CTransportFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTransportFormView)
	BOOL m_bInitialized;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sSpcDistance;

	CXTResizeGroupBox m_wndGrpTransport;
	CXTResizeGroupBox m_wndGrpTransport2;
	CXTResizeGroupBox m_wndGrpTransport3;
	CXTResizeGroupBox m_wndGrpTransport4;

	CMyExtStatic m_wndLbl1;
	CMyExtEdit m_wndEdit1;
	CMyExtStatic m_wndLbl2;

	CXTButton m_wndBtnAddCols;
	CXTButton m_wndBtnDelCol;
	CXTButton m_wndBtnAddRow;
	CXTButton m_wndBtnDelRow;

	CXTButton m_wndBtnAddRow2;
	CXTButton m_wndBtnDelRow2;

	enumCostTmplState m_enumCostTmplState;

	vecTransaction_costtempl m_vecTransaction_costtempl;

	std::map<int,bool> m_mapSpecieUsed;

	vecTransactionSpecies m_vecTransactionSpecies;
	void getSpeciesFromDB(void);
	int getSpecieID(LPCTSTR spc_name);
	CString getSpcNameFromDBSpecies(int id);

	void getCostTemplateFromDB();

	void setEnable(BOOL);

	// Database connection datamemebers; 070228 p�d
	CUMCostsTmplDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	CTransportFormView();           // protected constructor used by dynamic creation
	virtual ~CTransportFormView();

	CMyReportCtrl m_wndReport;
	CMyReportCtrl m_wndReport2;
	BOOL setupReport(void);
	BOOL setupReport2(void);

public:
	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void setupNewCostTemplate(BOOL do_first = TRUE,BOOL do_second = TRUE);
	// Set/Get from DB
	BOOL getTransportTable(CString&);
	void setTransportTable(vecObjectCostTemplate_table &);
	BOOL getTransportTable2(CString&);
	void setTransportTable2(vecTransaction_costtempl_transport &);

	// Add species from selection dialog; 080325 p�d
	void addSpecies(vecTransactionSpecies &);
	void getSpecies(vecIntegers &spc_used);
	void delSpecie(void);

	CString getLastEnteredSpecie(void);

	void setLockTransportView(BOOL lock)
	{
		m_wndReport.EnableWindow(!lock);
		m_wndReport2.EnableWindow(!lock);
		m_wndEdit1.EnableWindow(!lock);
		m_wndBtnAddCols.EnableWindow(!lock);
		m_wndBtnDelCol.EnableWindow(!lock);
		m_wndBtnAddRow.EnableWindow(!lock);
		m_wndBtnDelRow.EnableWindow(!lock);
		m_wndBtnAddRow2.EnableWindow(!lock);
		m_wndBtnDelRow2.EnableWindow(!lock);
	}

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CPageOneFormView)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnClose();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnReport1Click(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReport1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReport2Click(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReport2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddCols();
	afx_msg void OnBnClickedDelCol();
	afx_msg void OnBnClickedAddRow();
	afx_msg void OnBnClickedDelRow();
	afx_msg void OnBnClickedAddRow2();
	afx_msg void OnBnClickedDelRow2();
};


