#pragma once
#include "UMCostsTmplDB.h"

#include "Resource.h"

// CAddToTemplateDlg dialog

class CAddToTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddToTemplateDlg)
	CString m_sAbrevLangSet;
	// Setup language filename; 051214 p�d
  CString m_sLangFN;

	CMyReportCtrl m_wndReport;
	BOOL setupReport(void);

	CMyExtStatic m_wndLbl1;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	CUMCostsTmplDB *m_pDB;

	int m_nActiveCostID;
	void populateData(void);

	CTransaction_costtempl m_costtmplSelected;

public:
	CAddToTemplateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddToTemplateDlg();

	void setActiveCostID(int id)
	{
		m_nActiveCostID = id;
	}

	CTransaction_costtempl &getSelectedTemplateRecord(void)
	{
		return m_costtmplSelected;
	}
// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSelectSpeciesDlg)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
