#pragma once

#include "Resource.h"

#include "UMCostsTmplDB.h"

// CMDICostsFormView form view

class CMDICostsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDICostsFormView)

	//private:
	BOOL m_bInitialized;

	CXTResizeGroupBox m_wndGrpTransport;
	CXTResizeGroupBox m_wndGrpCutting;
	CXTResizeGroupBox m_wndGrpOther;
	CXTResizeGroupBox m_wndGrpNotes;

	CButton m_wndBtnAddSpecie;
	CButton m_wndBtnRemoveSpecie;

	CMyReportCtrl m_wndReport;

	CMyExtStatic m_wndLbl0_1;
	CMyExtStatic m_wndLbl0_2;
	CMyExtStatic m_wndLbl0_3;

	CMyExtStatic m_wndLbl2_1;
	CMyExtStatic m_wndLbl2_2;
	CMyExtStatic m_wndLbl2_3;
	CMyExtStatic m_wndLbl2_4;

	CMyExtEdit m_wndEdit0_1;	
	CMyExtEdit m_wndEdit0_2;	
	CMyExtEdit m_wndEdit0_3;	
	CMyExtEdit m_wndEdit0_4;	

	CMyExtEdit m_wndEdit2_1;
	CMyExtEdit m_wndEdit2_2;
	CMyExtEdit m_wndEdit2_3;
	CMyExtEdit m_wndEdit2_4;

	CMyExtStatic m_wndLbl3_1;
	CMyExtStatic m_wndLbl3_2;

	CMyExtEdit m_wndEdit3_1;
	CMyExtEdit m_wndEdit3_2;

	CString m_sLangFN;
	CString m_sLangAbrev;

	CString m_sMessageCap;
	CString m_sMessage1;
	CString m_sMessage2;
	CString m_sMessage3;

	int m_nDBIndex;
	enumCostTmplState m_enumCostTmplState;

	CUMCostsTmplDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setupReport(void);

	void setLanguage(void);

	void populateData(UINT idx);

	vecTransactionSpecies m_vecSpecies;
	void getSpeciesFromDB(void);

	void addNewCostTempl(void);
	void delCostTempl(void);

	CTransaction_costtempl m_recActiveCostTmpl;
	vecTransaction_costtempl m_vecTransaction_costtempl;
	void getCostTemplateFromDB(void);
	int saveCostTemplateToDB(void);

	int getSpecieID(LPCTSTR spc_name);

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void addSpeciesConstraintsToReport(void);

	// Check if name already been used, not allowed; 080520 p�d
	BOOL isNameOfTemplateOK(void);

protected:
	CMDICostsFormView();           // protected constructor used by dynamic creation
	virtual ~CMDICostsFormView();

public:
	int getDBIndex(void)
	{
		return m_nDBIndex;
	}

	void doPouplate(UINT idx)
	{
		populateData(idx);
	}

	BOOL isOKToClose(void);

	void doSetNavigationBar();

	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy();
	afx_msg void OnReportClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddSpecie();
	afx_msg void OnBnClickedRemoveSpecie();
	afx_msg void OnEnKillfocusEdit8();
};


