#if !defined(__COSTSFRAME_H__)
#define __COSTSFRAME_H__

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CMDICostsFormFrame

class CMDICostsFormFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDICostsFormFrame)
		

//private:
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	CXTResizeGroupBox m_wndGeneralInfoGroup;

	CString m_sLangFN;

	void setLanguage(void);

	BOOL m_bFirstOpen;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CMDICostsFormFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CXTPTabClientWnd m_MTIClientWnd;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDICostsFormFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDICostsFormFrame)
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
//	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDICostsListFrame frame

class CMDICostsListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDICostsListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDICostsListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDICostsListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// class CMDIInfrCostsFormFrame

class CMDIInfrCostsFormFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIInfrCostsFormFrame)
		

//private:
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	CXTResizeGroupBox m_wndGeneralInfoGroup;

	CString m_sLangFN;
	CString m_sMsgCap;
	CString m_sMsgWindowsOpen;

	BOOL m_bIsImportTBtn;
	BOOL m_bIsAddSpeciesTBtn;
	BOOL m_bIsDelSpeciesTBtn;
	BOOL m_bIsPreviewTBtn;

	void setLanguage(void);

	BOOL m_bFirstOpen;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CMDIInfrCostsFormFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CXTPTabClientWnd m_MTIClientWnd;

	void setImportTBtn(BOOL enable)
	{
			m_bIsImportTBtn = enable;
			m_bIsPreviewTBtn = enable;
	}

	void setSpeciesTBtn(BOOL enable)
	{
		m_bIsAddSpeciesTBtn = enable;
		m_bIsDelSpeciesTBtn = enable;
	}


// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CMDIInfrCostsFormFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CMDIInfrCostsFormFrame)
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnUpdateAddTBtn(CCmdUI* pCmdUI);
//	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnImportTBtnClick(void);
	afx_msg void OnExportTBtnClick(void);
	afx_msg void OnUpdateExportTBtn(CCmdUI* pCmdUI);
	afx_msg void OnAddSpeciesTBtnClick(void);
	afx_msg void OnTBtnPreview(void);

	afx_msg void OnUpdateAddSpeciesTBtn(CCmdUI* pCmdUI);
	afx_msg void OnDelSpecieTBtnClick(void);
	afx_msg void OnUpdateDelSpecieTBtn(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePreviewTBtn(CCmdUI*);

	//}}AFX_MSG
DECLARE_MESSAGE_MAP()
};




///////////////////////////////////////////////////////////////////////////////////////////
// CMDICostsInfrListFrame frame

class CMDICostsInfrListFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDICostsInfrListFrame)

//private:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;

	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDICostsInfrListFrame();           // protected constructor used by dynamic creation
	virtual ~CMDICostsInfrListFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif