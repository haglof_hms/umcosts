#pragma once

#include "Resource.h"

#include "UMCostsTmplDB.h"

#include "TransportFormView.h"
#include "CuttingFormView.h"
#include "RemainingFormView.h"

#include "ResLangFileReader.h"

// CMDIInfrCostsFormView form view

class CMDIInfrCostsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIInfrCostsFormView)

	BOOL m_bInitialized;
	BOOL m_bIsDataEnabled;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	StrMap m_mapLngStr;

	CString m_sMessageCap;
	CString m_sMessage1;
	CString m_sMsgDuplicateTemplName;
	CString m_sMsgTemplNameMissing;
	CString m_sMsgTemplInStandTemplate;
	CString m_sMsgContinueWithoutSaving;
	CString m_sMsgTemplCopyFrom;
	CString m_sMsgTemplRemoveSpecie;
	CString m_sMsgSpecie;
	CString m_sMsgNoData;
	CString m_sCostTmplExistsMsg;
	CString m_sMsgCharError;
	
	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CXTResizeGroupBox m_wndGroup1;
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CMyComboBox m_wndCBox1;
	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;

	int m_nPrevStatus;

	enumCostTmplState m_enumTemplateState;

	CUMCostsTmplDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	vecTransaction_costtempl m_vecTransaction_costtempl;
	CTransaction_costtempl m_recActive_costtempl;
	void getCostTemplateFromDB(void);

	void setLanguage(void);

	void setEnable(BOOL);

	void clearData(void);

	void saveObjectCostData();

	void removeObjectData();

	int m_nDBIndex;
	void populateData(int, bool bLoadStatusFromDB = TRUE);

	BOOL isOkToSave(BOOL show_msg,BOOL show_msg2,int *do_continue);
	BOOL IsOkToClose();

	BOOL isCostTemplInStandTamplate(void);

	BOOL m_bDoLock;

protected:
	CMDIInfrCostsFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIInfrCostsFormView();

public:
	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	//Lagt till en koll p� vilken tab som �r aktiverad, om �vriga kostnader sl�ck tr�dslagsknappar
	//Bug #2565 20111122 J�
	void CheckTabView(int nDoLock);

	vecTransaction_costtempl &getTransactionCostTempl(void);


	CTransportFormView *getTransportPage(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(0);
		if (m_tabManager)
		{
			CTransportFormView* pView = DYNAMIC_DOWNCAST(CTransportFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CTransportFormView, pView);
			return pView;
		}
		return NULL;
	}

	CCuttingFormView *getCuttingPage(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(1);
		if (m_tabManager)
		{
			CCuttingFormView* pView = DYNAMIC_DOWNCAST(CCuttingFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CCuttingFormView, pView);
			return pView;
		}
		return NULL;
	}

	CRemainingFormView *getRemainingPage(void)
	{
		m_tabManager = m_wndTabControl.getTabPage(2);
		if (m_tabManager)
		{
			CRemainingFormView* pView = DYNAMIC_DOWNCAST(CRemainingFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
			ASSERT_KINDOF(CRemainingFormView, pView);
			return pView;
		}
		return NULL;
	}

	int getDBIndex(void)
	{
		return m_nDBIndex;
	}

	void doPouplate(UINT idx)
	{
		UINT nItems = m_vecTransaction_costtempl.size();
		populateData(idx);
		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080207 p�d
		if (nItems == 1)
		{
			setEnable(TRUE);
			setNavigationButtons(FALSE,FALSE);	
		}
		else if (nItems > 1)
		{
			if (idx > 0 && idx < nItems)
			{
				setEnable(TRUE);
				setNavigationButtons(TRUE,TRUE);	
			}
			if (idx == 0)
			{
				setEnable(TRUE);
				setNavigationButtons(FALSE,TRUE);	
			}
			if (idx == nItems-1)
			{
				setEnable(TRUE);
				setNavigationButtons(TRUE,FALSE);	
			}
		}
		else
		{
			setEnable(FALSE);
			setNavigationButtons(FALSE,FALSE);	
		}

	}

	BOOL getNavBtnStart(void)
	{
		return (m_nDBIndex > 0);
	}
	BOOL getNavBtnEnd(void)
	{
		return (m_nDBIndex < (m_vecTransaction_costtempl.size()-1));
	}

	CString getCostTmplXML(void)	{ return m_recActive_costtempl.getTemplateFile(); }

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setNavigationButtonsEx(void);

	void importTemplate();
	void exportTemplate();

	void addSpeciesToTemplate();
	void delSelectedSpecieInTemplate();

	void doSetNavigationBar();

	void LockControls();

protected:
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	//{{AFX_VIRTUAL(CSetupStandReportView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDI1950ForrestNormFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd*);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
	afx_msg void OnEnKillfocusEdit1();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnCBox1Changed(void);
};


