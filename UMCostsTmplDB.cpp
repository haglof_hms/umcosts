#include "StdAfx.h"

#include "UMCostsTmplDB.h"

//////////////////////////////////////////////////////////////////////////////////
// CUMCostsTmplDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMCostsTmplDB::CUMCostsTmplDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMCostsTmplDB::CUMCostsTmplDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMCostsTmplDB::CUMCostsTmplDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE

BOOL CUMCostsTmplDB::templateExist(CTransaction_costtempl &rec)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where cost_id=%d"),
		TBL_COSTS_TEMPLATE,rec.getID());
	return exists(sSQL);
}


// PUBLIC
BOOL CUMCostsTmplDB::getCostTmpls(vecTransaction_costtempl &vec,int tmpl_type)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where cost_type_of=:1"),TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= tmpl_type;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();

			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));

		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMCostsTmplDB::getCostTmpls(vecTransaction_costtempl &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where cost_type_of=:1 or cost_type_of=:2"),
			TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= COST_TYPE_2;
		m_saCommand.Param(2).setAsShort()		= COST_TYPE_NOT_DONE_2;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));

		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMCostsTmplDB::getCostTmplsExclude(vecTransaction_costtempl &vec,int tmpl_id)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s where (cost_type_of=:1 or cost_type_of=:2) and cost_id <> :3"),
			TBL_COSTS_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= COST_TYPE_2;
		m_saCommand.Param(2).setAsShort()		= COST_TYPE_NOT_DONE_2;
		m_saCommand.Param(3).setAsShort()		= tmpl_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_costtempl(m_saCommand.Field(1).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
															  	 (LPCTSTR)m_saCommand.Field(4).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(5).asLongChar(),
															  	 (LPCTSTR)m_saCommand.Field(6).asString(),
																   (LPCTSTR)convertSADateTime(saDateTime)));

		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CUMCostsTmplDB::addCostTmpl(CTransaction_costtempl &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!templateExist(rec))
		{
			sSQL.Format(_T("insert into %s (cost_name,cost_type_of,cost_template,cost_notes,created_by) values(:1,:2,:3,:4,:5)"),
									TBL_COSTS_TEMPLATE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsLongChar()	= rec.getTemplateFile();
			m_saCommand.Param(4).setAsLongChar()	= rec.getTemplateNotes();
			m_saCommand.Param(5).setAsString()	= rec.getCreatedBy();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			m_saConnection.Commit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMCostsTmplDB::updCostTmpl(CTransaction_costtempl &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (templateExist(rec))
		{

			sSQL.Format(_T("update %s set cost_name=:1,cost_type_of=:2,created_by=:3,cost_template=:4,cost_notes=:5 \
									 where cost_id=:6"),TBL_COSTS_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getTemplateName();
			m_saCommand.Param(2).setAsShort()		= rec.getTypeOf();
			m_saCommand.Param(3).setAsString()	= rec.getCreatedBy();
			m_saCommand.Param(4).setAsLongChar()	= rec.getTemplateFile();
			m_saCommand.Param(5).setAsLongChar()	= rec.getTemplateNotes();

			m_saCommand.Param(6).setAsShort()		= rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMCostsTmplDB::delCostTmpl(CTransaction_costtempl &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (templateExist(rec))
		{

			sSQL.Format(_T("delete from %s where cost_id=:1"),TBL_COSTS_TEMPLATE);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			m_saConnection.Commit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		m_saConnection.Rollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CUMCostsTmplDB::getSpecies(vecTransactionSpecies &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_species(-1,
				m_saCommand.Field(1).asShort(),
				m_saCommand.Field(_T("p30_spec")).asShort(),
				(LPCTSTR)m_saCommand.Field(2).asString(),
				(LPCTSTR)m_saCommand.Field(3).asString(),
				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CUMCostsTmplDB::getTemplates(vecTransactionTemplate &vec)
{

	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_TEMPLATE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			SADateTime saDateTime = m_saCommand.Field(7).asDateTime();
			vec.push_back(CTransaction_template(m_saCommand.Field(1).asShort(),
																	(LPCTSTR)m_saCommand.Field(2).asString(),
																	 m_saCommand.Field(3).asShort(),
																	(LPCTSTR)m_saCommand.Field(4).asString(),
															  	(LPCTSTR)m_saCommand.Field(5).asString(),
															  	(LPCTSTR)m_saCommand.Field(6).asString(),
																  (LPCTSTR)convertSADateTime(saDateTime)));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCTSTR)e.ErrText());
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}
