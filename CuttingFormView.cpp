// CuttingFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDIInfrCostsFormView.h"

#include "CuttingFormView.h"

#include "ResLangFileReader.h"

// CCuttingFormView

IMPLEMENT_DYNCREATE(CCuttingFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCuttingFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT_DGV, OnReport1KeyDown)
	ON_NOTIFY(NM_CLICK, IDC_REPORT_DGV, OnReport1Click)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT_M3FUB, OnReport2KeyDown)
	ON_NOTIFY(NM_CLICK, IDC_REPORT_M3FUB, OnReport2Click)

	ON_BN_CLICKED(IDC_ADD_COLS2, &CCuttingFormView::OnBnClickedAddCols)
	ON_BN_CLICKED(IDC_DEL_COLS2, &CCuttingFormView::OnBnClickedDelCol)
	ON_BN_CLICKED(IDC_ADD_ROW2, &CCuttingFormView::OnBnClickedAddRow)
	ON_BN_CLICKED(IDC_DEL_ROW2, &CCuttingFormView::OnBnClickedDelRow)
	ON_BN_CLICKED(IDC_RADIO1, &CCuttingFormView::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CCuttingFormView::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CCuttingFormView::OnBnClickedRadio3)
END_MESSAGE_MAP()

CCuttingFormView::CCuttingFormView()
	: CXTResizeFormView(CCuttingFormView::IDD)
{
	m_bInitialized = FALSE;
	m_enumCostTmplState = COSTTMPL_NONE;
}


CCuttingFormView::~CCuttingFormView()
{
}

void CCuttingFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP_CUTTING2_1, m_wndGrpCutting);
	DDX_Control(pDX, IDC_GROUP_CUTTING2_2, m_wndGrpCutting2);
	DDX_Control(pDX, IDC_GROUP_CUTTING2_3, m_wndGrpCutting3);
	DDX_Control(pDX, IDC_GROUP_CUTTING2_4, m_wndGrpCutting4);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL2_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL2_5, m_wndLbl5);

	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT2_3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT2_4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT2_5, m_wndEdit5);

	DDX_Control(pDX, IDC_ADD_COLS2, m_wndBtnAddCols);
	DDX_Control(pDX, IDC_DEL_COLS2, m_wndBtnDelCol);
	DDX_Control(pDX, IDC_ADD_ROW2, m_wndBtnAddRow);
	DDX_Control(pDX, IDC_DEL_ROW2, m_wndBtnDelRow);
	
	DDX_Control(pDX, IDC_RADIO1, m_wndRBtn1);
	DDX_Control(pDX, IDC_RADIO2, m_wndRBtn2);
	DDX_Control(pDX, IDC_RADIO3, m_wndRBtn3);
	
}

void CCuttingFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit1.SetEnabledColor(BLACK,WHITE);
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit1.SetAsNumeric();

		m_wndEdit2.SetEnabledColor(BLACK,WHITE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit2.SetAsNumeric();

		m_wndEdit3.SetEnabledColor(BLACK,WHITE);
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit3.SetAsNumeric();

		m_wndEdit4.SetEnabledColor(BLACK,WHITE);
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit4.SetAsNumeric();

// "�vriga kostnader. medelpris (kr/m3) Anv�nds inte"; 2009-02-04 P�D
//		m_wndEdit5.SetEnabledColor(BLACK,WHITE);
//		m_wndEdit5.SetDisabledColor(BLACK,COL3DFACE);
//		m_wndEdit5.SetAsNumeric();
		m_wndEdit5.ShowWindow(SW_HIDE);

		m_wndBtnAddCols.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelCol.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnAddRow.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelRow.SetXButtonStyle(BS_XT_WINXP_COMPAT );

		m_wndBtnAddRow.ShowWindow( SW_HIDE );
		m_wndBtnDelRow.ShowWindow( SW_HIDE );

		m_wndGrpCutting.ShowWindow( SW_HIDE );

		getSpeciesFromDB();
		
		getCostTemplateFromDB();

		setupReport();

		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080207 p�d
		setEnable(m_vecTransaction_costtempl.size() > 0);
		// Setup selected cuttinclass enabled; 080313 p�d
		setEnable2(TRUE,FALSE,FALSE);

		m_bInitialized = TRUE;
	}
}


// CCuttingFormView diagnostics

#ifdef _DEBUG
void CCuttingFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCuttingFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG
// CCuttingFormView message handlers
BOOL CCuttingFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CCuttingFormView::OnSize(UINT nType,int cx,int cy)
{
	CRect rect;
/*
	if (m_wndGrpCutting.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpCutting,2,4,130,cy-8);
	}
*/
	if (m_wndGrpCutting2.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpCutting2,4,120,cx-8,cy-128,TRUE);
	}

	if (m_wndGrpCutting3.m_hWnd)
	{
		m_wndGrpCutting3.GetClientRect(&rect);
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpCutting3,4,45,cx-8,rect.Height());
	}

	if (m_wndGrpCutting4.m_hWnd)
	{
		m_wndGrpCutting4.GetClientRect(&rect);
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpCutting4,4,4,cx-8,rect.Height());
	}

	if (m_wndReport_DGV.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndReport_DGV,134,140,cx-145,cy-155);
	}

	if (m_wndReport_M3FUB.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndReport_M3FUB,134,140,cx-145,cy-155);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

LRESULT CCuttingFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			setEnable(TRUE);
			m_enumCostTmplState = COSTTMPL_NEW;
			setupNewCostTemplate();
		}
		break;

		case ID_SAVE_ITEM :
		{
			m_enumCostTmplState = COSTTMPL_OPEN;
		}
		break;
		// Added 071127 p�d
		case ID_DELETE_ITEM :
		{
			m_enumCostTmplState = COSTTMPL_OPEN;
		}
		break;

	};
	return 0L;
}
///////////////////////////////////////////////////////////////////////
// Database handling methods; 080207 p�d

void CCuttingFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getSpecies(m_vecTransactionSpecies);
		}
	}
}

int CCuttingFormView::getSpecieID(LPCTSTR spc_name)
{
	CString S;
	// Use species defined in database table: fst_species_table
	// to establish id of specie; 071011 p�d
	if (m_vecTransactionSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionSpecies.size();i++)
		{
			CTransaction_species rec = m_vecTransactionSpecies[i];
			if (rec.getSpcName().CompareNoCase(spc_name) == 0)
				return rec.getSpcID();
		}
	}
	return -1;
}

CString CCuttingFormView::getSpcNameFromDBSpecies(int id)
{
	// Use species defined in database table: fst_species_table
	// to establish id of specie; 071011 p�d
	if (m_vecTransactionSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionSpecies.size();i++)
		{
			CTransaction_species rec = m_vecTransactionSpecies[i];
			if (rec.getSpcID() == id)
				return rec.getSpcName();
		}
	}
	return _T("");
}

///////////////////////////////////////////////////////////////////////
// Get data by getFormViewByID(...); 080207 p�d
void CCuttingFormView::getCostTemplateFromDB(void)
{
	m_vecTransaction_costtempl.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Get Object costtemplates (Redy to use and Under development); 080212 p�d	
			m_pDB->getCostTmpls(m_vecTransaction_costtempl);
		}
	}
}

// CCuttingFormView message handlers
BOOL CCuttingFormView::setupReport(short set_rep)
{
	if (set_rep == 0 || set_rep == 1)
	{
		if (m_wndReport_DGV.GetSafeHwnd() == 0)
		{
			// Create the sheet1 list box.
			if (!m_wndReport_DGV.Create(this, IDC_REPORT_DGV, TRUE, FALSE))
			{
				TRACE0( "Failed to create sheet1.\n" );
				return FALSE;
			}
		}
	}

	if (set_rep == 0 || set_rep == 2)
	{
		if (m_wndReport_M3FUB.GetSafeHwnd() == 0)
		{
			// Create the sheet1 list box.
			if (!m_wndReport_M3FUB.Create(this, IDC_REPORT_M3FUB, TRUE, FALSE))
			{
				TRACE0( "Failed to create sheet1.\n" );
				return FALSE;
			}
		}
	}

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Setup interface strings; 080207 p�d
			m_sSpcDGV = (xml->str(IDS_STRING1200));
			m_sAvgTrunkM3FUB = (xml->str(IDS_STRING1206));	// Added 091106 p�d
			m_sPriceM3FUB	 	 = (xml->str(IDS_STRING1207));	// Added 091125 p�d

			m_sGrpHeadDGV = xml->str(IDS_STRING4203);
			m_sGrpHeadM3FUB = xml->str(IDS_STRING4204);

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING1201)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING2000)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING2001)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING2002)));
			// "�vriga kostnader. medelpris (kr/m3) Anv�nds inte"; 2009-02-04 P�D
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING2003)));
			m_wndLbl5.ShowWindow(SW_HIDE);

			m_wndBtnAddCols.SetWindowText((xml->str(IDS_STRING1202)));
			m_wndBtnDelCol.SetWindowText((xml->str(IDS_STRING1203)));
			m_wndBtnAddRow.SetWindowText((xml->str(IDS_STRING1204)));
			m_wndBtnDelRow.SetWindowText((xml->str(IDS_STRING1205)));

			m_wndGrpCutting3.SetWindowText((xml->str(IDS_STRING4202)));

			m_wndRBtn1.SetWindowText((xml->str(IDS_STRING4202)));
			m_wndRBtn2.SetWindowText((xml->str(IDS_STRING4203)));
			m_wndRBtn3.SetWindowText((xml->str(IDS_STRING4204)));

			if (set_rep == 0 || set_rep == 1)
			{
				if (m_wndReport_DGV.GetSafeHwnd() != NULL)
				{
				
//					m_wndReport_DGV.ShowWindow( SW_NORMAL );

					// Make specie column non scrollable; 080207 p�d
					m_wndReport_DGV.SetFreezeColumnsCount(1);

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport_DGV.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	//				pCol->GetEditOptions()->AddComboButton();

					m_wndReport_DGV.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport_DGV.SetMultipleSelection( FALSE );
					m_wndReport_DGV.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport_DGV.FocusSubItems(TRUE);
					m_wndReport_DGV.AllowEdit(TRUE);
					m_wndReport_DGV.ShowHeader( FALSE );
					m_wndReport_DGV.SetFocus();
				}	// if (m_wndReport_DGV.GetSafeHwnd() != NULL)
			} // if (set_rep == 0 || set_rep == 1)
			if (set_rep == 0 || set_rep == 2)
			{
				if (m_wndReport_M3FUB.GetSafeHwnd() != NULL)
				{
				
//					m_wndReport_M3FUB.ShowWindow( SW_NORMAL );

					// Make specie column non scrollable; 080207 p�d
					m_wndReport_M3FUB.SetFreezeColumnsCount(1);

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport_M3FUB.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	//				pCol->GetEditOptions()->AddComboButton();

					m_wndReport_M3FUB.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport_M3FUB.SetMultipleSelection( FALSE );
					m_wndReport_M3FUB.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport_M3FUB.FocusSubItems(TRUE);
					m_wndReport_M3FUB.AllowEdit(TRUE);
					m_wndReport_M3FUB.ShowHeader( FALSE );
					m_wndReport_M3FUB.SetFocus();
				}	// if (m_wndReport_M3FUB.GetSafeHwnd() != NULL)
			}	// if (set_rep == 0 || set_rep == 2)
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CCuttingFormView::setupNewCostTemplate(short set_item)
{
	CXTPReportColumns *pCols = NULL;
	CXTPReportColumn *pCol = NULL;

	// Clear data; 080313 p�d
	if (set_item == -1)
	{
		m_wndRBtn1.SetCheck(TRUE);	// Set "Hunningskostnad i kr/m3"; 080313 p�d
		m_wndRBtn2.SetCheck(FALSE);	
		m_wndRBtn3.SetCheck(FALSE);	
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit3.SetWindowText(_T(""));
		m_wndEdit4.SetWindowText(_T(""));
		setEnable2(TRUE,FALSE,FALSE);
	}
// "�vriga kostnader. medelpris (kr/m3) Anv�nds inte"; 2009-02-04 P�D
//	m_wndEdit5.SetWindowText(_T(""));

	////////////////////////////////////////////////////////////////
	// Setup table in Specie/DGV
	////////////////////////////////////////////////////////////////
	if (set_item == -1 || set_item == 0)
	{
		m_wndReport_DGV.ResetContent();
		// Remove columns; 080213 p�d
		if ((pCols = m_wndReport_DGV.GetColumns()) != NULL)
			pCols->Clear();
		// Recreate first column in report; 080213 p�d
		pCol = m_wndReport_DGV.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		// Add first record; 080213 p�d
		m_wndReport_DGV.AddRecord(new CTransportReportRec_DGV(1,(m_sSpcDGV)));
		m_wndReport_DGV.Populate();
		m_wndReport_DGV.UpdateWindow();
	}
	////////////////////////////////////////////////////////////////
	// Setup table in Specie/m3fub
	////////////////////////////////////////////////////////////////
	if (set_item == -1 || set_item == 1)
	{
		m_wndReport_M3FUB.ResetContent();
		// Remove columns; 080213 p�d
		if ((pCols = m_wndReport_M3FUB.GetColumns()) != NULL)
			pCols->Clear();
		// Recreate first column in report; 080213 p�d
		pCol = m_wndReport_M3FUB.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		// Add first record; 080213 p�d
		m_wndReport_M3FUB.AddRecord(new CTransportReportRec_M3FUB(1,m_sAvgTrunkM3FUB,sz2dec));
		m_wndReport_M3FUB.AddRecord(new CTransportReportRec_M3FUB(1,m_sPriceM3FUB,sz0dec));
		m_wndReport_M3FUB.Populate();
		m_wndReport_M3FUB.UpdateWindow();
	}


}

//------------------------------------------------------------------------
// Create the transport table in xml-format; 080211 p�d
BOOL CCuttingFormView::getCuttingTable(CString &xml,int *set_as)
{
	int nSpcID = -1;
	CString sSpcName;
	CString sData;
	CString sCutting;
	CXTPReportColumns *pCols_DGV = m_wndReport_DGV.GetColumns();
	CXTPReportRecords *pRecs_DGV = m_wndReport_DGV.GetRecords();
	CTransportReportRec_DGV *pRec_DGV = NULL;

	CXTPReportColumns *pCols_M3FUB = m_wndReport_M3FUB.GetColumns();
	CXTPReportRecords *pRecs_M3FUB = m_wndReport_M3FUB.GetRecords();
	CTransportReportRec_M3FUB *pRec_M3FUB = NULL;

	if (m_wndRBtn1.GetCheck() == 1)
		*set_as = 1;	// "Huggningskostnader i kr/m3"; 080313 p�d
	if (m_wndRBtn2.GetCheck() == 1)
		*set_as = 2;	// "Huggningskostnader enligt tabell dgv"; 080313 p�d
	if (m_wndRBtn3.GetCheck() == 1)
		*set_as = 3;	// "Huggningskostnader enligt tabell m3fub"; 091106 p�d
	// Check which option's marked; 080313 p�d
//	if (m_wndRBtn1.GetCheck() == 1)
//	{
//		*set_as = 1;	// "Huggningskostnader i kr/m3"; 080313 p�d
		//--------------------------------------------------------------------
		// Add cutting tag data; 071010 p�d
		sData = formatData(NODE_OBJ_COSTTMPL_CUTTING2_CUT1,m_wndEdit2.getFloat());
		sData += formatData(NODE_OBJ_COSTTMPL_CUTTING2_CUT2,m_wndEdit3.getFloat());
		sData += formatData(NODE_OBJ_COSTTMPL_CUTTING2_CUT3,m_wndEdit4.getFloat());
// "�vriga kostnader. medelpris (kr/m3) Anv�nds inte"; 2009-02-04 P�D
//		sData += formatData(NODE_OBJ_COSTTMPL_CUTTING2_CUT4,m_wndEdit5.getFloat());
		sData += formatData(NODE_OBJ_COSTTMPL_CUTTING2_CUT4,0.0);
		xml = sData;
//	}
//	else if (m_wndRBtn2.GetCheck() == 1)
//	{
//		*set_as = 2;	// "Huggningskostnader enligt tabell dgv"; 080313 p�d
		///////////////////////////////////////////////////////////////////////////////////////////////
		//	Get table for cutting set in DGV; 091106 p�d
		///////////////////////////////////////////////////////////////////////////////////////////////
		// Check for inforamtion
		if (pRecs_DGV == NULL || pCols_DGV == NULL)
			return NULL;
		// Only one row and one column. I.e. no data entered; 080212 p�d
//		if (pRecs_DGV->GetCount() == 1 && pCols_DGV->GetCount() == 1)
//			return FALSE;

		for (int i = 0;i < pRecs_DGV->GetCount();i++)
		{
			pRec_DGV = (CTransportReportRec_DGV *)pRecs_DGV->GetAt(i);
			if (i > 0)
				sData = "\n";
			for (int ii = 0;ii < pCols_DGV->GetCount();ii++)
			{
				if (i == 0 && ii == 0)	// First row and column
				{
					sData = formatData((NODE_OBJ_COSTTMPL_CUTTING_ATTR1),-1,_T(""));
				} // if (i == 0 && ii == 0)	// First row and column
				//-----------------------------------------------------------
				// Read transport-distances (m)
				else if (i == 0 && ii > 0)	// First row and and columns
				{
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING_ATTR2),ii,pRec_DGV->getColumnInt(ii));
				}
				//-----------------------------------------------------------
				// Read specie information
				else if (i > 0 && ii == 0)	// Next rows and first column
				{
					// Get specie id; 080212 p�d
					sSpcName = pRec_DGV->getColumnText(ii);
					nSpcID = getSpecieID(pRec_DGV->getColumnText(ii));
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING_ATTR1),nSpcID,sSpcName);
				}
				//-----------------------------------------------------------
				// Read data price per transport-distance (m)
				else if (i > 0 && ii > 0)	// Next rows and next columns
				{
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING_ATTR2),ii,pRec_DGV->getColumnInt(ii));
				}
			}	// for (int ii = 0;ii < pCols_DGV->GetCount();ii++)
			sCutting += formatData((NODE_OBJ_COSTTMPL_CUTTING_ITEM),sData);
		}	// for (int i = 0;i < pRecs_DGV->GetCount();i++)
		xml += sCutting;
		sCutting.Empty();
		///////////////////////////////////////////////////////////////////////////////////////////////
		//	Get table for cutting set in M3FUB; 091106 p�d
		///////////////////////////////////////////////////////////////////////////////////////////////

		// Check for inforamtion
		if (pRecs_M3FUB == NULL || pCols_M3FUB == NULL)
			return NULL;
		// Only one row and one column. I.e. no data entered; 080212 p�d
//		if (pRecs_M3FUB->GetCount() == 1 && pCols_M3FUB->GetCount() == 1)
//			return FALSE;
		
		// Changed from pRecs_M3FUB->GetCount() to 2. I.e. only two rows; 091125 p�d
		int nNumOf_RecsM3Fub = pRecs_M3FUB->GetCount();
		if (nNumOf_RecsM3Fub > 2) nNumOf_RecsM3Fub = 2;
		for (int i = 0;i < nNumOf_RecsM3Fub /*pRecs_M3FUB->GetCount()*/;i++)
		{
			pRec_M3FUB = (CTransportReportRec_M3FUB *)pRecs_M3FUB->GetAt(i);
			if (i > 0)
				sData = "\n";
			for (int ii = 0;ii < pCols_M3FUB->GetCount();ii++)
			{
				if (i == 0 && ii == 0)	// First row and column
				{
					sData = formatData((NODE_OBJ_COSTTMPL_CUTTING2_ATTR1),-1,_T(""));
				} // if (i == 0 && ii == 0)	// First row and column
				//-----------------------------------------------------------
				// Read transport-distances (m)
				else if (i == 0 && ii > 0)	// First row and and columns
				{
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING2_ATTR2),ii,pRec_M3FUB->getColumnFloat(ii));
				}
				//-----------------------------------------------------------
				// Read specie information
				// Changed from i > 0 to i == 1; 091125 p�d
				else if (i == 1 && ii == 0)	// Second row and first column
				{
					// Get specie id; 080212 p�d
					sSpcName = pRec_M3FUB->getColumnText(ii);
					nSpcID = getSpecieID(pRec_M3FUB->getColumnText(ii));
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING2_ATTR1),nSpcID,sSpcName);
				}
				//-----------------------------------------------------------
				// Read data price per transport-distance (m)
				// Changed from i > 0 to i == 1; 091125 p�d
				else if (i == 1 && ii > 0)	// Second row and next columns
				{
					sData += formatData((NODE_OBJ_COSTTMPL_CUTTING2_ATTR2),ii,pRec_M3FUB->getColumnFloat(ii));
				}
			}	// for (int ii = 0;ii < pCols_M3FUB->GetCount();ii++)
			sCutting += formatData((NODE_OBJ_COSTTMPL_CUTTING2_ITEM),sData);
		}	// for (int i = 0;i < pRecs_M3FUB->GetCount();i++)
		xml += sCutting;
	//	} // else if (m_wndRBtn2.GetCheck() == 1)
	return TRUE;
}

void CCuttingFormView::setCuttingTable(vecObjectCostTemplate_table &vecDGV,vecObjectCostTemplate_table &vecM3FUB,CTransaction_costtempl_cutting &rec,int set_as,int bDoLock)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportColumns *pCols = NULL;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRecords *pRecs = NULL;
	
	CTransportReportRec_DGV *pTranspRec_DGV = NULL;
	CTransportReportRec_M3FUB *pTranspRec_M3FUB = NULL;
	CTransportReportRec_M3FUB *pTranspRec_M3FUBPrice = NULL;
	
	CObjectCostTemplate_table	table;
	CObjectCostTemplate_table	table1;
	int nRow,nCol,nRow1;
	
	BOOL bIsEqualSpecies; // The same number of species in DGV and M3FUB; 091118 p�d
	BOOL bAlreadyInM3FUBTable;

	if (set_as == 1)	// Cutting k3/m3
	{
		m_wndRBtn1.SetCheck(1);
		m_wndRBtn2.SetCheck(0);
		m_wndRBtn3.SetCheck(0);
		setEnable2(TRUE,FALSE,FALSE,!bDoLock);
	}
	else if (set_as == 2)	// Cutting DGV
	{
		m_wndRBtn2.SetCheck(1);
		m_wndRBtn1.SetCheck(0);
		m_wndRBtn3.SetCheck(0);
		setEnable2(FALSE,TRUE,FALSE,!bDoLock);
		m_wndGrpCutting2.SetWindowText(m_sGrpHeadDGV);

	}
	else if (set_as == 3)	// Cutting M3FUB
	{
		m_wndRBtn3.SetCheck(1);
		m_wndRBtn1.SetCheck(0);
		m_wndRBtn2.SetCheck(0);
		setEnable2(FALSE,FALSE,TRUE,!bDoLock);
		m_wndGrpCutting2.SetWindowText(m_sGrpHeadM3FUB);
	}
	m_wndEdit2.setFloat(rec.getCut1(),0);
	m_wndEdit3.setFloat(rec.getCut2(),0);
	m_wndEdit4.setFloat(rec.getCut3(),0);

	// If there's only one row, no data's been entered; 090512 p�d
	//if (pRows != NULL)
	//		if (pRows->GetCount() == 1) return;

	if (vecDGV.size() == 0 )
	{
		setupNewCostTemplate(0);
	}
	if (vecM3FUB.size() == 0)
	{
		setupNewCostTemplate(1);
	}
	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Set up table for cutting set in DGV; 091106 p�d
	///////////////////////////////////////////////////////////////////////////////////////////////
	if (vecDGV.size() > 0)
	{
		pRows = m_wndReport_DGV.GetRows();
		// Always reset Report; 090310 p�d
		m_wndReport_DGV.ResetContent();

		// Check for columns in report. If there's already columns
		// remove them, before entering new data; 080213 p�d
		if ((pCols = m_wndReport_DGV.GetColumns()) != NULL)
		{
			pCols->Clear();
		}
		setupReport(1);

		table = vecDGV[0];	// Just to get number of columns (in getValues()); 080212 p�d
		//----------------------------------------------------------------------------
		// Start by adding columns to Report; 080213 p�d
		if (table.getValues_int().size() > 0)
		{
			for (nCol = 0;nCol < table.getValues_int().size();nCol++)
			{
				pCol = m_wndReport_DGV.AddColumn(new CXTPReportColumn(nCol+1,_T(""),50));
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
			
			}	// for (nCol = 0;nCol < table.getValues().size();nCol++)
		}	// if (table.getValue().size() > 0)

		//----------------------------------------------------------------------------
		// Add a record for each row; 080212 p�d
		for (nRow = 0;nRow < vecDGV.size();nRow++)
		{
			// Get information on e.g. number of columns and
			// data in each column/row; 080213 p�d
			table = vecDGV[nRow];
			// Try to get the name of specie set in "fst_species_table" table.
			// Makin' sure the name is up to date in cost-template; 090608 p�d
			table.setSpcName(getSpcNameFromDBSpecies(table.getSpcID()));
			pTranspRec_DGV = (CTransportReportRec_DGV*)m_wndReport_DGV.AddRecord(new	CTransportReportRec_DGV(table.getValues_int().size()+1,table.getSpcName()));
			if (pTranspRec_DGV != NULL)
			{
				if (nRow == 0)
					pTranspRec_DGV->setColumnText(0,(m_sSpcDGV));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < table.getValues_int().size();nCol++)
				{
					pTranspRec_DGV->setColumnInt(nCol+1,table.getValues_int()[nCol]);
				}
			}
		}	// for (nRow = 0;nRow < vec.size();nRow++)
		m_wndReport_DGV.Populate();
		m_wndReport_DGV.UpdateWindow();
	}
	//---------------------------------------------------------------------
	// I think we must check, to see if number of species in M3FUB-table
	// corresponds to number in DGV-table. If not we'll match the species
	// in M3FUB-table to those in DGV-table; 091118 p�d

	//bIsEqualSpecies = (vecDGV.size() == vecM3FUB.size());

	///////////////////////////////////////////////////////////////////////////////////////////////
	//	Set up table for cutting set in M3FUB; 091106 p�d
	///////////////////////////////////////////////////////////////////////////////////////////////
	if (vecM3FUB.size() > 0)
	{
		pRows = m_wndReport_M3FUB.GetRows();
		// Always reset Report; 090310 p�d
		m_wndReport_M3FUB.ResetContent();

		// Check for columns in report. If there's already columns
		// remove them, before entering new data; 091106 p�d
		if ((pCols = m_wndReport_M3FUB.GetColumns()) != NULL)
		{
			pCols->Clear();
		}
		setupReport(2);

		table = vecM3FUB[0];	// Just to get number of columns (in getValues()); 080212 p�d
		//----------------------------------------------------------------------------
		// Start by adding columns to Report; 080213 p�d
		if (table.getValues_float().size() > 0)
		{
			for (nCol = 0;nCol < table.getValues_float().size();nCol++)
			{
				pCol = m_wndReport_M3FUB.AddColumn(new CXTPReportColumn(nCol+1,_T(""),50));
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;			
			}	// for (nCol = 0;nCol < table.getValues().size();nCol++)
		}	// if (table.getValue().size() > 0)

		//----------------------------------------------------------------------------
		// Add a record for each row; 080212 p�d
		// Changed from vecM3FUB.size() to 2, i.e. 2 rows; 091125 p�d
		int nNumOfRow_m3fub = vecM3FUB.size();
		if (nNumOfRow_m3fub > 2) nNumOfRow_m3fub = 2;
		for (nRow = 0;nRow < nNumOfRow_m3fub /*vecM3FUB.size()*/;nRow++)
		{
			// Get information on e.g. number of columns and
			// data in each column/row; 080213 p�d
			table = vecM3FUB[nRow];
			// Try to get the name of specie set in "fst_species_table" table.
			// Makin' sure the name is up to date in cost-template; 090608 p�d
			table.setSpcName(getSpcNameFromDBSpecies(table.getSpcID()));
			if (nRow == 0)
			{
				pTranspRec_M3FUB = (CTransportReportRec_M3FUB*)m_wndReport_M3FUB.AddRecord(new	CTransportReportRec_M3FUB(table.getValues_float().size()+1,m_sAvgTrunkM3FUB /*table.getSpcName()*/,sz2dec));
				pTranspRec_M3FUBPrice = (CTransportReportRec_M3FUB*)m_wndReport_M3FUB.AddRecord(new	CTransportReportRec_M3FUB(table.getValues_float().size()+1,m_sPriceM3FUB /*table.getSpcName()*/,sz0dec));
			}

			if (pTranspRec_M3FUB != NULL)
			{
				if (nRow == 0)
					pTranspRec_M3FUB->setColumnText(0,(m_sAvgTrunkM3FUB));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < table.getValues_float().size();nCol++)
				{
					if (nRow == 0)
						pTranspRec_M3FUB->setColumnFloat(nCol+1,table.getValues_float()[nCol]);
					else
						pTranspRec_M3FUBPrice->setColumnFloat(nCol+1,table.getValues_float()[nCol]);
				}
			}
		}	// for (nRow = 0;nRow < vec.size();nRow++)
/*	COMMENTED OUT 2009-11-25 P�D
		Method for pricelist for avg. trunk changed. 
		Set as one price/avg. trunk instead of one for each specie; 091125 p�d
		// Try to match species in M3FUB-table to those in DGV-table; 091118 p�d
		if (!bIsEqualSpecies)
		{

			//----------------------------------------------------------------------------
			// Add a record for each row; 080212 p�d
			for (nRow = 0;nRow < vecDGV.size();nRow++)
			{
				// Get information on e.g. number of columns and
				// data in each column/row; 080213 p�d
				table = vecDGV[nRow];
				// Match to find in M3FUB-table, if not add to M3FUB-table; 091118 p�d
				bAlreadyInM3FUBTable = FALSE;
				for (nRow1 = 0;nRow1 < vecM3FUB.size();nRow1++)
				{
					table1 = vecM3FUB[nRow1];
					if (bAlreadyInM3FUBTable = (table.getSpcID() == table1.getSpcID()))
						break;
				}
				if (!bAlreadyInM3FUBTable)
				{
					// Try to get the name of specie set in "fst_species_table" table.
					// Makin' sure the name is up to date in cost-template; 090608 p�d
					table.setSpcName(getSpcNameFromDBSpecies(table.getSpcID()));
					pTranspRec_M3FUB = (CTransportReportRec_M3FUB*)m_wndReport_M3FUB.AddRecord(new	CTransportReportRec_M3FUB(table1.getValues_float().size()+1,table.getSpcName(),sz0dec));
				}	// if (!bAlreadyInM3FUBTable)
			}	// for (nRow = 0;nRow < vec.size();nRow++)
		}
*/
		m_wndReport_M3FUB.Populate();
		m_wndReport_M3FUB.UpdateWindow();
	} // if (vecM3FUB.size() > 0)
}

void CCuttingFormView::addSpecies(vecTransactionSpecies &vec)
{
	CXTPReportRows *pRows_DGV = m_wndReport_DGV.GetRows();
	CXTPReportColumns *pCols_DGV = m_wndReport_DGV.GetColumns();
	CTransportReportRec_DGV *pRec_DGV = NULL;

	CXTPReportRows *pRows_M3FUB = m_wndReport_M3FUB.GetRows();
	CXTPReportColumns *pCols_M3FUB = m_wndReport_M3FUB.GetColumns();
	CTransportReportRec_M3FUB *pRec_M3FUB = NULL;
	
	int nNumOfCols,nCol;
	if (vec.size() > 0)
	{

		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// "Huggning DGV"; 080325 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		if (pCols_DGV != NULL)
		{
			nNumOfCols = pCols_DGV->GetCount();
		}
		if (pRows_DGV != NULL)
		{
			if (pRows_DGV->GetCount() == 0)	// Add first row
				setupNewCostTemplate();

			if (pRows_DGV->GetCount() > 1)
				pRec_DGV = (CTransportReportRec_DGV *)pRows_DGV->GetAt(pRows_DGV->GetCount() - 1)->GetRecord();
		}

		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_species rec = vec[i];
			pRec_DGV = (CTransportReportRec_DGV*)m_wndReport_DGV.AddRecord(new	CTransportReportRec_DGV(nNumOfCols+1,_T("")));
			if (pRec_DGV != NULL)
			{
				pRec_DGV->setColumnText(COLUMN_0,(rec.getSpcName()));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < nNumOfCols;nCol++)
				{
					if (nCol > 0)
					{
						if (pRec_DGV != NULL)				
							pRec_DGV->setColumnInt(nCol,pRec_DGV->getColumnInt(nCol));
						else
							pRec_DGV->setColumnInt(nCol,0);
					}	// if (nCol > 0)
				}
			}
		}	// for (int i = 0;i < vec.size();i++)
		m_wndReport_DGV.Populate();
		m_wndReport_DGV.UpdateWindow();
		m_wndReport_DGV.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);


		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// "Huggning M3FUB"; 091106 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		if (pCols_M3FUB != NULL)
		{
			nNumOfCols = pCols_M3FUB->GetCount();
		}
		if (pRows_M3FUB != NULL)
		{
			if (pRows_M3FUB->GetCount() == 0)	// Add first row
				setupNewCostTemplate();

//			if (pRows_M3FUB->GetCount() > 1)
//				pRec_M3FUB = (CTransportReportRec_M3FUB *)pRows_M3FUB->GetAt(pRows_M3FUB->GetCount() - 1)->GetRecord();
		}
/*
		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_species rec = vec[i];
			pRec_M3FUB = (CTransportReportRec_M3FUB*)m_wndReport_M3FUB.AddRecord(new	CTransportReportRec_M3FUB(nNumOfCols+1,_T(""),sz0dec));
			if (pRec_M3FUB != NULL)
			{
				pRec_M3FUB->setColumnText(COLUMN_0,(rec.getSpcName()));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < nNumOfCols;nCol++)
				{
					if (nCol > 0)
					{
						if (pRec_M3FUB != NULL)				
							pRec_M3FUB->setColumnFloat(nCol,pRec_M3FUB->getColumnFloat(nCol));
						else
							pRec_M3FUB->setColumnFloat(nCol,0.0);
					}	// if (nCol > 0)
				}
			}
		}	// for (int i = 0;i < vec.size();i++)
		m_wndReport_M3FUB.Populate();
		m_wndReport_M3FUB.UpdateWindow();
		m_wndReport_M3FUB.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
*/

	}
}

void CCuttingFormView::delSpecie(void)
{
	CXTPReportRows *pRows_DGV = NULL;
	CXTPReportRows *pRows_M3FUB = NULL;

	pRows_DGV = m_wndReport_DGV.GetRows();
	if (pRows_DGV != NULL)
	{
		if (pRows_DGV->GetCount() > 1)
		{
			CXTPReportRecord *pRec_DGV = pRows_DGV->GetAt(pRows_DGV->GetCount()-1)->GetRecord();		
			if (pRec_DGV != NULL)
			{
				pRec_DGV->Delete();			
				m_wndReport_DGV.Populate();
				m_wndReport_DGV.UpdateWindow();
			}
		}
	}	// if (pRows_DGV != NULL)
	pRows_M3FUB = m_wndReport_M3FUB.GetRows();
	if (pRows_M3FUB != NULL)
	{
		if (pRows_M3FUB->GetCount() > 1)
		{
			CXTPReportRecord *pRec_M3FUB = pRows_M3FUB->GetAt(pRows_M3FUB->GetCount()-1)->GetRecord();		
			if (pRec_M3FUB != NULL)
			{
				pRec_M3FUB->Delete();			
				m_wndReport_M3FUB.Populate();
				m_wndReport_M3FUB.UpdateWindow();
			}
		}
	}	// if (pRows != NULL)
}

//------------------------------------------------------------------------


void CCuttingFormView::OnReport1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
	if (!m_wndReport_DGV.GetSafeHwnd())
		return;

	CXTPReportRow *pRow_DGV = m_wndReport_DGV.GetFocusedRow();
	CXTPReportColumn *pCol_DGV = m_wndReport_DGV.GetFocusedColumn();
	if (pRow_DGV != NULL && pCol_DGV != NULL)
	{
		if (pCol_DGV->GetItemIndex() == COLUMN_0)
		{
			if (pRow_DGV->GetIndex() == 0)
			{
				pCol_DGV->SetEditable(FALSE);
			}
			else
			{
				pCol_DGV->SetEditable(TRUE);
			}
		}
		else
		{
			pCol_DGV->SetEditable(TRUE);
		}
	}	// if (pRows_DGV != NULL && pCol_DGV != NULL)
}

void CCuttingFormView::OnReport1Click(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (!m_wndReport_DGV.GetSafeHwnd())
		return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			if (pItemNotify->pRow->GetIndex() == 0)
			{
				m_wndReport_DGV.EditItem(NULL);
			}
			else
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&m_wndReport_DGV, pItemNotify->pRow, pItemNotify->pColumn);
				m_wndReport_DGV.EditItem(&itemArgs);
			}
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
	}	// if (pItemNotify != NULL)

}

void CCuttingFormView::OnReport2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
	if (!m_wndReport_M3FUB.GetSafeHwnd())
		return;

	CXTPReportRow *pRow_M3FUB = m_wndReport_M3FUB.GetFocusedRow();
	CXTPReportColumn *pCol_M3FUB = m_wndReport_M3FUB.GetFocusedColumn();
	if (pRow_M3FUB != NULL && pCol_M3FUB != NULL)
	{
		if (pCol_M3FUB->GetItemIndex() == COLUMN_0)
		{
			if (pRow_M3FUB->GetIndex() == 0)
			{
				pCol_M3FUB->SetEditable(FALSE);
			}
			else
			{
				pCol_M3FUB->SetEditable(TRUE);
			}
		}
		else
		{
			pCol_M3FUB->SetEditable(TRUE);
		}
	}	// if (pRows_M3FUB != NULL && pCol_M3FUB != NULL)
}

void CCuttingFormView::OnReport2Click(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (!m_wndReport_M3FUB.GetSafeHwnd())
		return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			if (pItemNotify->pRow->GetIndex() == 0)
			{
				m_wndReport_M3FUB.EditItem(NULL);
			}
			else
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&m_wndReport_M3FUB, pItemNotify->pRow, pItemNotify->pColumn);
				m_wndReport_M3FUB.EditItem(&itemArgs);
			}
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
	}	// if (pItemNotify != NULL)

}


void CCuttingFormView::OnBnClickedAddCols()
{
	CXTPReportRows *pRows_DGV = m_wndReport_DGV.GetRows();
	CXTPReportColumn *pCol_DGV = NULL;
	CTransportReportRec_DGV *pOldRec_DGV = NULL;
	CTransportReportRec_DGV *pNewRec_DGV = NULL;

	CXTPReportRows *pRows_M3FUB = m_wndReport_DGV.GetRows();
	CXTPReportColumn *pCol_M3FUB = NULL;
	CTransportReportRec_M3FUB *pOldRec_M3FUB = NULL;
	CTransportReportRec_M3FUB *pNewRec_M3FUB = NULL;

	int nNumOfColumns = 0;
	int nAddNumOfCols = m_wndEdit1.getInt();
	//Always add 1 column if no value in number of columns to add. Bug #2214 20110809 J�
	if(nAddNumOfCols==0)
		nAddNumOfCols=1;
	/////////////////////////////////////////////////////////////////////////////
	// Add columns for table DGV; 091106 p�d
	/////////////////////////////////////////////////////////////////////////////
	if (m_wndRBtn2.GetCheck())
	{
		// Check if there's any rows. If not enter the first row
		// holding Headline; 080303 p�d
		if (pRows_DGV != NULL)
		{
			if (pRows_DGV->GetCount() == 0)
			{
				m_wndReport_DGV.ClearReport();
				// Remove columns; 080213 p�d
				CXTPReportColumns *pCols_DGV = m_wndReport_DGV.GetColumns();
				if (pCols_DGV != NULL)
				{
					pCols_DGV->Clear();
				}
				// Recreate first column in report; 080213 p�d
				pCol_DGV = m_wndReport_DGV.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
				pCol_DGV->AllowRemove(FALSE);
				pCol_DGV->GetEditOptions()->m_bAllowEdit = FALSE;
	//			pCol->GetEditOptions()->AddComboButton();

				// Add first record; 080213 p�d
				m_wndReport_DGV.AddRecord(new CTransportReportRec_DGV(1,(m_sSpcDGV)));
				m_wndReport_DGV.Populate();
				m_wndReport_DGV.UpdateWindow();
			}
		}
		nNumOfColumns = m_wndReport_DGV.GetColumns()->GetCount();
		for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)
		{
			pCol_DGV = m_wndReport_DGV.AddColumn(new CXTPReportColumn(i,_T(""),50));
			pCol_DGV->SetAlignment(DT_RIGHT);
			pCol_DGV->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol_DGV->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
			pCol_DGV->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

			m_wndReport_DGV.Populate();
			m_wndReport_DGV.UpdateWindow();

			CXTPReportRecords *pRecords_DGV = m_wndReport_DGV.GetRecords();
			CXTPReportColumns *pColumns_DGV = m_wndReport_DGV.GetColumns();
			CXTPReportRows *pRows_DGV = m_wndReport_DGV.GetRows();
			if (pColumns_DGV != NULL && pRows_DGV != NULL && pRecords_DGV != NULL)
			{
				for (int nRow = 0;nRow < pRows_DGV->GetCount();nRow++)
				{
					pOldRec_DGV = (CTransportReportRec_DGV*)pRecords_DGV->GetAt(nRow);
					if (pOldRec_DGV != NULL)
					{
						pNewRec_DGV = new CTransportReportRec_DGV(pColumns_DGV->GetCount(),_T(""));
						for (int nCol = 0;nCol < pColumns_DGV->GetCount();nCol++)
						{
							if (nCol == 0)
							{
								pNewRec_DGV->setColumnText(nCol,(pOldRec_DGV->getColumnText(nCol)));
							}
							else
							{
								// As long we are readding columns, use already entered
								// information. Otherwise set new column to 0; 080207 p�d
								if (nCol < nNumOfColumns)
									pNewRec_DGV->setColumnInt(nCol,pOldRec_DGV->getColumnInt(nCol));
								else
									pNewRec_DGV->setColumnInt(nCol,0);
							}
						}	// for (int nCol = 0;nCol < pColumns->GetCount();nCol++)

						pRecords_DGV->RemoveAt(nRow);
						pRecords_DGV->InsertAt(nRow,pNewRec_DGV);
					}	// if (pOldRec != NULL)
				}	// for (int nRow = 0;nRow < pRows->GetCount();nRow++)
			}	// if (pColumns != NULL && pRows != NULL)
		}	// for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)

		m_wndReport_DGV.Populate();
		m_wndReport_DGV.UpdateWindow();
	}
	/////////////////////////////////////////////////////////////////////////////
	// Add columns for table m3fub; 091106 p�d
	/////////////////////////////////////////////////////////////////////////////
	if (m_wndRBtn3.GetCheck())
	{
		// Check if there's any rows. If not enter the first row
		// holding Headline; 080303 p�d
		if (pRows_M3FUB != NULL)
		{
			if (pRows_M3FUB->GetCount() == 0)
			{
				m_wndReport_M3FUB.ClearReport();
				// Remove columns; 080213 p�d
				CXTPReportColumns *pCols_M3FUB = m_wndReport_M3FUB.GetColumns();
				if (pCols_M3FUB != NULL)
				{
					pCols_M3FUB->Clear();
				}
				// Recreate first column in report; 080213 p�d
				pCol_M3FUB = m_wndReport_DGV.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
				pCol_M3FUB->AllowRemove(FALSE);
				pCol_M3FUB->GetEditOptions()->m_bAllowEdit = FALSE;
	//			pCol->GetEditOptions()->AddComboButton();

				// Add first record; 080213 p�d
				m_wndReport_M3FUB.AddRecord(new CTransportReportRec_M3FUB(1,(m_sAvgTrunkM3FUB),sz2dec));
				m_wndReport_M3FUB.Populate();
				m_wndReport_M3FUB.UpdateWindow();
			}
		}
		nNumOfColumns = m_wndReport_M3FUB.GetColumns()->GetCount();
		for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)
		{
			pCol_M3FUB = m_wndReport_M3FUB.AddColumn(new CXTPReportColumn(i,_T(""),50));
			pCol_M3FUB->SetAlignment(DT_RIGHT);
			pCol_M3FUB->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol_M3FUB->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

			m_wndReport_M3FUB.Populate();
			m_wndReport_M3FUB.UpdateWindow();

			CXTPReportRecords *pRecords_M3FUB = m_wndReport_M3FUB.GetRecords();
			CXTPReportColumns *pColumns_M3FUB = m_wndReport_M3FUB.GetColumns();
			CXTPReportRows *pRows_M3FUB = m_wndReport_M3FUB.GetRows();
			if (pColumns_M3FUB != NULL && pRows_M3FUB != NULL && pRecords_M3FUB != NULL)
			{
				for (int nRow = 0;nRow < pRows_M3FUB->GetCount();nRow++)
				{
					pOldRec_M3FUB = (CTransportReportRec_M3FUB*)pRecords_M3FUB->GetAt(nRow);
					if (pOldRec_M3FUB != NULL)
					{
						if (nRow == 0)
							pNewRec_M3FUB = new CTransportReportRec_M3FUB(pColumns_M3FUB->GetCount(),_T(""),sz2dec);
						else
							pNewRec_M3FUB = new CTransportReportRec_M3FUB(pColumns_M3FUB->GetCount(),_T(""),sz0dec);
						for (int nCol = 0;nCol < pColumns_M3FUB->GetCount();nCol++)
						{
							if (nCol == 0)
							{
								pNewRec_M3FUB->setColumnText(nCol,(pOldRec_M3FUB->getColumnText(nCol)));
							}
							else
							{
								// As long we are readding columns, use already entered
								// information. Otherwise set new column to 0; 080207 p�d
								if (nCol < nNumOfColumns)
									pNewRec_M3FUB->setColumnFloat(nCol,pOldRec_M3FUB->getColumnFloat(nCol));
								else
									pNewRec_M3FUB->setColumnFloat(nCol,0.0);
							}
						}	// for (int nCol = 0;nCol < pColumns->GetCount();nCol++)

						pRecords_M3FUB->RemoveAt(nRow);
						pRecords_M3FUB->InsertAt(nRow,pNewRec_M3FUB);
					}	// if (pOldRec != NULL)
				}	// for (int nRow = 0;nRow < pRows->GetCount();nRow++)
			}	// if (pColumns != NULL && pRows != NULL)
		}	// for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)

		m_wndReport_M3FUB.Populate();
		m_wndReport_M3FUB.UpdateWindow();
	}

}

void CCuttingFormView::OnBnClickedDelCol()
{
	int nColIndex = 0;
	CXTPReportColumn *pCol = NULL;
	CXTPReportColumns *pColumns_DGV = m_wndReport_DGV.GetColumns();
	CXTPReportRecords *pRecords_DGV = m_wndReport_DGV.GetRecords();

	CXTPReportColumns *pColumns_M3FUB = m_wndReport_M3FUB.GetColumns();
	CXTPReportRecords *pRecords_M3FUB = m_wndReport_M3FUB.GetRecords();

	if (m_wndRBtn2.GetCheck())
	{
		if (pColumns_DGV != NULL && pRecords_DGV != NULL)
		{
			nColIndex = pColumns_DGV->GetCount() - 1;
			// Don't remove first column (name of specie); 080207 p�d
			if (nColIndex > 0)
			{
				pCol = pColumns_DGV->GetAt(nColIndex);
				if (pCol != NULL)
				{
					//pRecords->RemoveAt(pCol->GetItemIndex());
					pColumns_DGV->Remove(pCol);
				}	// if (pCol)
			}	// if (nColIndex > 1)
			m_wndReport_DGV.Populate();
		}
	}
	else if (m_wndRBtn3.GetCheck())
	{
		if (pColumns_M3FUB != NULL && pRecords_M3FUB != NULL)
		{
			nColIndex = pColumns_M3FUB->GetCount() - 1;
			// Don't remove first column (name of specie); 080207 p�d
			if (nColIndex > 0)
			{
				pCol = pColumns_M3FUB->GetAt(nColIndex);
				if (pCol != NULL)
				{
					//pRecords->RemoveAt(pCol->GetItemIndex());
					pColumns_M3FUB->Remove(pCol);
				}	// if (pCol)
			}	// if (nColIndex > 1)
			m_wndReport_M3FUB.Populate();
		}
	}
}

// Add a row (Specie); 080207 p�d
void CCuttingFormView::OnBnClickedAddRow()
{
	int nNumOfColumns = 0;
	if (m_wndRBtn2.GetCheck())
	{
		nNumOfColumns = m_wndReport_DGV.GetColumns()->GetCount();
		m_wndReport_DGV.AddRecord(new CTransportReportRec_DGV(nNumOfColumns,_T("")));
	
		m_wndReport_DGV.Populate();
		m_wndReport_DGV.UpdateWindow();
	}
	else 	if (m_wndRBtn3.GetCheck())
	{
		CXTPReportRows *pRows = m_wndReport_M3FUB.GetRows();
		if (pRows) if (pRows->GetCount() == 2) return;
		nNumOfColumns = m_wndReport_M3FUB.GetColumns()->GetCount();
		m_wndReport_M3FUB.AddRecord(new CTransportReportRec_M3FUB(nNumOfColumns,_T(""),sz0dec));
	
		m_wndReport_M3FUB.Populate();
		m_wndReport_M3FUB.UpdateWindow();
	}
}

void CCuttingFormView::OnBnClickedDelRow()
{
	CXTPReportRow *pRow = NULL;
	CXTPReportRecord *pRec = NULL;
	if (m_wndRBtn2.GetCheck())
	{
		if (m_wndReport_DGV.GetRows()->GetCount() > 0)
		{
			pRow = m_wndReport_DGV.GetFocusedRow(); 
			if (pRow != NULL)
			{
				if (pRow->GetIndex() > 0)
				{
					pRec = pRow->GetRecord();		
					if (pRec != NULL)
					{
						pRec->Delete();			
						m_wndReport_DGV.Populate();
						m_wndReport_DGV.UpdateWindow();
					}
				}	// if (pRow->GetIndex() > 0)
			}	// if (pRow != NULL)
		}	// if (m_wndReport_DGV.GetRows()->GetCount() > 0)
	}
/*
	else	if (m_wndRBtn3.GetCheck())
	{
		if (m_wndReport_M3FUB.GetRows()->GetCount() > 0)
		{
			pRow = m_wndReport_M3FUB.GetFocusedRow(); 
			if (pRow != NULL)
			{
				if (pRow->GetIndex() > 0)
				{
					pRec = pRow->GetRecord();		
					if (pRec != NULL)
					{
						pRec->Delete();			
						m_wndReport_M3FUB.Populate();
						m_wndReport_M3FUB.UpdateWindow();
					}
				}	// if (pRow->GetIndex() > 0)
			}	// if (pRow != NULL)
		}	// if (m_wndReport_DGV.GetRows()->GetCount() > 0)
	}
*/
}

// "Huggningskostnader i kr/m3"
void CCuttingFormView::OnBnClickedRadio1()
{
	m_wndRBtn2.SetCheck(FALSE);	
	m_wndRBtn3.SetCheck(FALSE);	
	setEnable2(TRUE,FALSE,FALSE);
}

// "Huggningskostnader i tabell dgv"
void CCuttingFormView::OnBnClickedRadio2()
{
	m_wndGrpCutting2.SetWindowText(m_sGrpHeadDGV);

	m_wndRBtn1.SetCheck(FALSE);	
	m_wndRBtn3.SetCheck(FALSE);	
	setEnable2(FALSE,TRUE,FALSE);
}

// "Huggningskostnader i tabell m3fub"
void CCuttingFormView::OnBnClickedRadio3()
{
	m_wndGrpCutting2.SetWindowText(m_sGrpHeadM3FUB);

	m_wndRBtn1.SetCheck(FALSE);	
	m_wndRBtn2.SetCheck(FALSE);	
	setEnable2(FALSE,FALSE,TRUE);
}
