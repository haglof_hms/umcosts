// CMDICostsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDICostsFormView.h"

#include "ResLangFileReader.h"

// CMDICostsFormView

IMPLEMENT_DYNCREATE(CMDICostsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDICostsFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_ADD_SPECIE, &CMDICostsFormView::OnBnClickedAddSpecie)
	ON_BN_CLICKED(IDC_DEL_SPECIE, &CMDICostsFormView::OnBnClickedRemoveSpecie)
	ON_EN_KILLFOCUS(IDC_EDIT8, &CMDICostsFormView::OnEnKillfocusEdit8)
END_MESSAGE_MAP()

CMDICostsFormView::CMDICostsFormView()
	: CXTResizeFormView(CMDICostsFormView::IDD)
{
	m_bInitialized = FALSE;
	m_enumCostTmplState = COSTTMPL_NONE;	// Nothing yet
	m_pDB = NULL;
}

CMDICostsFormView::~CMDICostsFormView()
{
}

void CMDICostsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GRP_TRANSPORT, m_wndGrpTransport);
	DDX_Control(pDX, IDC_GRP_CUTTING, m_wndGrpCutting);
	DDX_Control(pDX, IDC_GRP_OTHER, m_wndGrpOther);
	DDX_Control(pDX, IDC_GRP_NOTES, m_wndGrpNotes);

	DDX_Control(pDX, IDC_ADD_SPECIE, m_wndBtnAddSpecie);
	DDX_Control(pDX, IDC_DEL_SPECIE, m_wndBtnRemoveSpecie);

	DDX_Control(pDX, IDC_LBL0_1, m_wndLbl0_1);
	DDX_Control(pDX, IDC_LBL0_2, m_wndLbl0_2);
	DDX_Control(pDX, IDC_LBL0_3, m_wndLbl0_3);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl2_1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2_2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl2_3);
	DDX_Control(pDX, IDC_LBL2_4, m_wndLbl2_4);

	DDX_Control(pDX, IDC_LBL3_1, m_wndLbl3_1);
	DDX_Control(pDX, IDC_LBL3_2, m_wndLbl3_2);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit2_1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2_2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit2_3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit2_4);

	DDX_Control(pDX, IDC_EDIT6, m_wndEdit3_1);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit3_2);

	DDX_Control(pDX, IDC_EDIT8, m_wndEdit0_1);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit0_2);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit0_3);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit0_4);
	//}}AFX_DATA_MAP

}

void CMDICostsFormView::OnDestroy()
{

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();
}

int CMDICostsFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupReport();

	return 0;
}

BOOL CMDICostsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDICostsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit2_1.SetAsNumeric();
	m_wndEdit2_2.SetAsNumeric();
	m_wndEdit2_3.SetAsNumeric();
	m_wndEdit2_4.SetAsNumeric();

	m_wndEdit3_1.SetAsNumeric();

	m_wndEdit0_3.SetEnabledColor(BLACK,INFOBK);
	m_wndEdit0_3.SetDisabledColor(BLACK,INFOBK);
	m_wndEdit0_3.SetWindowText(getDateEx());
	m_wndEdit0_3.SetReadOnly();

	if (!m_bInitialized )
	{

		getSpeciesFromDB();
		getCostTemplateFromDB();
		m_nDBIndex = m_vecTransaction_costtempl.size() - 1;	// Point to last item
		populateData(m_nDBIndex);

		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080207 p�d
		if (m_vecTransaction_costtempl.size() == 1)
		{
			setNavigationButtons(FALSE,FALSE);	
		}
		else if (m_vecTransaction_costtempl.size() > 1)
		{
			setNavigationButtons(TRUE,FALSE);	
		}
		else
		{
			setNavigationButtons(FALSE,FALSE);	
		}
		
		setLanguage();

		m_bInitialized = TRUE;

	}

}

BOOL CMDICostsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDICostsFormView::OnSetFocus(CWnd *wnd)
{
/*
	if (!m_vecTransaction_costtempl.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTransaction_costtempl.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
*/

	m_wndEdit0_1.SetFocus();
}

void CMDICostsFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType,cx,cy);

	if (m_wndReport.GetSafeHwnd() != NULL)
	{
		// resize window = display window in tab; 070219 p�d
		setResize(&m_wndReport,330,50,320,110);
	}
}

void CMDICostsFormView::doSetNavigationBar()
{
	if (m_vecTransaction_costtempl.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}

LRESULT CMDICostsFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addNewCostTempl();
		}
		break;

		case ID_SAVE_ITEM :
		{
			if (isNameOfTemplateOK())
				saveCostTemplateToDB();
		}
		break;
		// Added 071127 p�d
		case ID_DELETE_ITEM :
		{
			delCostTempl();
		}
		break;

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (isNameOfTemplateOK())
			{
				saveCostTemplateToDB();
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}

			break;

		}
		case ID_DBNAVIG_PREV :
		{
			if (isNameOfTemplateOK())
			{
				saveCostTemplateToDB();
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;

		}
		case ID_DBNAVIG_NEXT :
		{
			if (isNameOfTemplateOK())
			{
				saveCostTemplateToDB();
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTransaction_costtempl.size() - 1))
					m_nDBIndex = (UINT)m_vecTransaction_costtempl.size() - 1;
						
				if (m_nDBIndex == (UINT)m_vecTransaction_costtempl.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}

			break;

		}
		case ID_DBNAVIG_END :
		{
			if (isNameOfTemplateOK())
			{
				saveCostTemplateToDB();
				m_nDBIndex = (UINT)m_vecTransaction_costtempl.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}

			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

// CMDICostsFormView diagnostics

#ifdef _DEBUG
void CMDICostsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMDICostsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMDICostsFormView message handlers


void CMDICostsFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (m_wndReport.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT, FALSE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport.GetSafeHwnd() != NULL)
				{
					m_wndReport.ShowWindow( SW_NORMAL );

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING1001)), 100));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();

					addSpeciesConstraintsToReport();

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING1002)), 70));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING1003)), 60));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING1004)), 60));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);

					m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
					m_wndReport.FocusSubItems(TRUE);
					m_wndReport.AllowEdit(TRUE);
					m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);

				}	// if (m_wndReport.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
}

void CMDICostsFormView::addSpeciesConstraintsToReport(void)
{
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pColumns != NULL)
	{
		// Add species in Pricelist to Combobox in Column 1; 070509 p�d
		CXTPReportColumn *pCol = pColumns->Find(COLUMN_0);
		if (pCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 070509 p�d
			pCons = pCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					pCol->GetEditOptions()->AddConstraint((m_vecSpecies[i].getSpcName()),m_vecSpecies[i].getSpcID());
				}
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pCol != NULL)
	}	// if (pView != NULL && pColumns != NULL)
}

// Check if name already been used, not allowed; 080520 p�d
BOOL CMDICostsFormView::isNameOfTemplateOK(void)
{
	CString sTemplName;
	sTemplName = m_wndEdit0_1.getText();
	int nActiveID = m_recActiveCostTmpl.getID();
	if (m_vecTransaction_costtempl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		{
			if (m_vecTransaction_costtempl[i].getID() != nActiveID && 
				  sTemplName.CompareNoCase(m_vecTransaction_costtempl[i].getTemplateName()) == 0)
			{
				::MessageBox(this->GetSafeHwnd(),(m_sMessage3),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
				m_wndEdit0_1.SetWindowText((m_recActiveCostTmpl.getTemplateName()));
				m_wndEdit0_1.SetFocus();
				return FALSE;
			}
		}
	}

	return TRUE;
}

void CMDICostsFormView::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wndGrpTransport.SetWindowText((xml->str(IDS_STRING100)));
			m_wndGrpCutting.SetWindowText((xml->str(IDS_STRING200)));
			m_wndGrpOther.SetWindowText((xml->str(IDS_STRING300)));
			m_wndGrpNotes.SetWindowText((xml->str(IDS_STRING400)));

			m_wndBtnAddSpecie.SetWindowText((xml->str(IDS_STRING1000)));
			m_wndBtnRemoveSpecie.SetWindowText((xml->str(IDS_STRING1005)));

			m_wndLbl0_1.SetWindowText((xml->str(IDS_STRING500)));
			m_wndLbl0_2.SetWindowText((xml->str(IDS_STRING600)));
			m_wndLbl0_3.SetWindowText((xml->str(IDS_STRING700)));

			m_wndLbl2_1.SetWindowText((xml->str(IDS_STRING2000)));
			m_wndLbl2_2.SetWindowText((xml->str(IDS_STRING2001)));
			m_wndLbl2_3.SetWindowText((xml->str(IDS_STRING2002)));
			m_wndLbl2_4.SetWindowText((xml->str(IDS_STRING2003)));

			m_wndLbl3_1.SetWindowText((xml->str(IDS_STRING3001)));
			m_wndLbl3_2.SetWindowText((xml->str(IDS_STRING3002)));

			m_sMessageCap = (xml->str(IDS_STRING800));
			m_sMessage1.Format(_T("%s\n\n%s"),
						(xml->str(IDS_STRING8010)),
						(xml->str(IDS_STRING8011)));

			m_sMessage2 = (xml->str(IDS_STRING900));

			m_sMessage3.Format(_T("%s\n%s\n\n%s"),
				(xml->str(IDS_STRING4000)),
				(xml->str(IDS_STRING4001)),
				(xml->str(IDS_STRING4002)));
		}	// if (xml->Load(sLangFN))
		delete xml;
	}
}

void CMDICostsFormView::populateData(UINT idx)
{
	TCHAR szTemplName[127];
	TCHAR szTemplDoneBy[127];
	TCHAR szTemplDate[127];
	CTransaction_costtempl_cutting recCutting;
	CTransaction_costtempl_transport recTransport;
	vecTransaction_costtempl_transport vecTransport;
	CTransaction_costtempl_other_costs recOtherCosts;

	if (m_vecTransaction_costtempl.size() > 0 &&
			idx >= 0 && 
			idx < m_vecTransaction_costtempl.size())
	{
		m_recActiveCostTmpl = m_vecTransaction_costtempl[idx];

		CostsTmplParser *pars = new CostsTmplParser();
		if (pars != NULL)
		{

			if (pars->LoadFromBuffer(m_recActiveCostTmpl.getTemplateFile()))
			{

				pars->getCostsTmplName(szTemplName);
				pars->getCostsTmplDoneBy(szTemplDoneBy);
				pars->getCostsTmplDate(szTemplDate);

				// Get cutting costs; 071010 p�d
				pars->getCostsTmplCutting(recCutting);

				// Get transport costs; 071010 p�d
				pars->getCostsTmplTransport(vecTransport);

				// Get other costs; 071010 p�d
				pars->getCostsTmplOthers(recOtherCosts);

			}	// if (pars->LoadFromBuffer(m_recActiveTemplate.getTemplateFile()))

			delete pars;


			// Header data
			m_wndEdit0_1.SetWindowText(szTemplName);
			m_wndEdit0_2.SetWindowText(szTemplDoneBy);
			m_wndEdit0_3.SetWindowText(szTemplDate);
			m_wndEdit0_4.SetWindowText(m_recActiveCostTmpl.getTemplateNotes());

			// Transport costs
			if (vecTransport.size() > 0)
			{
				m_wndReport.ClearReport();

				for (UINT i = 0;i < vecTransport.size();i++)
				{
					// Add Species to Constraint; 071127 p�d
					addSpeciesConstraintsToReport();
					
					recTransport = vecTransport[i];
					m_wndReport.AddRecord(new CSpecieReportRec(recTransport.getSpecieName(),
																										 recTransport.getDistance(),
																										 recTransport.getCost(),
																										 recTransport.getMaxCost()));
				}
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
			else
			{
				m_wndReport.ClearReport();
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}

			// Cutting costs
			m_wndEdit2_1.setFloat(recCutting.getCut1(),0);
			m_wndEdit2_2.setFloat(recCutting.getCut2(),0);
			m_wndEdit2_3.setFloat(recCutting.getCut3(),0);
			m_wndEdit2_4.setFloat(recCutting.getCut4(),0);

			// Other costs
			m_wndEdit3_1.setFloat(recOtherCosts.getCost(),0);
			m_wndEdit3_2.SetWindowText(recOtherCosts.getTypeOfCOst());

		}
	}

	m_enumCostTmplState = COSTTMPL_OPEN;	// Show already entered cost templates; 071010 p�d

	doSetNavigationBar();

}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDICostsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}


void CMDICostsFormView::addNewCostTempl(void)
{
	// Header data
	m_wndEdit0_1.SetWindowText(_T(""));
	m_wndEdit0_2.SetWindowText(_T(""));
	m_wndEdit0_3.SetWindowText((getDateEx()));
	m_wndEdit0_4.SetWindowText(_T(""));

	// Transport costs
	m_wndReport.ClearReport();
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	
	// Cutting costs
	m_wndEdit2_1.SetWindowText(_T(""));
	m_wndEdit2_2.SetWindowText(_T(""));
	m_wndEdit2_3.SetWindowText(_T(""));
	m_wndEdit2_4.SetWindowText(_T(""));

	// Other costs
	m_wndEdit3_1.SetWindowText(_T(""));
	m_wndEdit3_2.SetWindowText(_T(""));

	addSpeciesConstraintsToReport();

	m_enumCostTmplState = COSTTMPL_NEW;	// A new cost template

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

}

void CMDICostsFormView::delCostTempl(void)
{
		if (m_bConnected)
		{
				if (::MessageBox(this->GetSafeHwnd(),(m_sMessage2),(m_sMessageCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				{		
					if (m_pDB->delCostTmpl(m_recActiveCostTmpl))
					{
						getCostTemplateFromDB();
						if (m_vecTransaction_costtempl.size() > 0)
						{
							m_nDBIndex = m_vecTransaction_costtempl.size() - 1;
						}
						else
						{
							m_nDBIndex = -1;
						}
						populateData(m_nDBIndex);
					}
				}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sMsgDelTemplate),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
		}	// if (m_bConnected)
}


void CMDICostsFormView::OnBnClickedAddSpecie()
{
	// Add species; 071010 p�d
	addSpeciesConstraintsToReport();
	// Add a row in the "Transport kostnader", specie; 071010 p�d
	m_wndReport.AddRecord(new CSpecieReportRec());
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}
// Remove last row in Report; 071127 p�d
void CMDICostsFormView::OnBnClickedRemoveSpecie()
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
		if (pRow != NULL)
		{
			pRecs->RemoveAt(pRow->GetIndex());
			m_wndReport.Populate();
			m_wndReport.UpdateWindow();

		}	// if (pRow != NULL)
	}	// if (pRecs != NULL)
}

// CSpecieDataFormView message handlers
void CMDICostsFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getSpecies(m_vecSpecies);
		}
	}
}

void CMDICostsFormView::getCostTemplateFromDB(void)
{
	m_vecTransaction_costtempl.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_bConnected = m_pDB->getCostTmpls(m_vecTransaction_costtempl,COST_TYPE_1);
		}
	}
}

int CMDICostsFormView::saveCostTemplateToDB(void)
{
	int nCostTmplID;
	CStringArray arrBuff;
	CString sBuff,S;

	if (m_enumCostTmplState != COSTTMPL_NONE)
	{
		// Start by adding STARTTAG; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_START);
		//--------------------------------------------------------------------
		// Add header tag data; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_HEADER_START);

		sBuff = m_wndEdit0_1.getText();
		TextToHtml(&sBuff);
		S.Format(NODE_COSTTMPL_NAME, sBuff);
		arrBuff.Add(S);

		sBuff = m_wndEdit0_2.getText();
		TextToHtml(&sBuff);
		S.Format(NODE_COSTTMPL_DONE_BY, sBuff);
		arrBuff.Add(S);

		sBuff = m_wndEdit0_3.getText();
		TextToHtml(&sBuff);
		S.Format(NODE_COSTTMPL_DATE, sBuff);
		arrBuff.Add(S);

		arrBuff.Add(NODE_COSTTMPL_HEADER_END);

		//--------------------------------------------------------------------
		// Add cutting tag data; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_CUTTING_START);
		S.Format(NODE_COSTTMPL_CUTTING_CUT_1,m_wndEdit2_1.getFloat());
		arrBuff.Add(S);
		S.Format(NODE_COSTTMPL_CUTTING_CUT_2,m_wndEdit2_2.getFloat());
		arrBuff.Add(S);
		S.Format(NODE_COSTTMPL_CUTTING_CUT_3,m_wndEdit2_3.getFloat());
		arrBuff.Add(S);
		S.Format(NODE_COSTTMPL_CUTTING_CUT_4,m_wndEdit2_4.getFloat());
		arrBuff.Add(S);
		arrBuff.Add(NODE_COSTTMPL_CUTTING_END);

		//--------------------------------------------------------------------
		// Add transport tag data; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_TRANSP_START);
		
		int nNumOfRecords = m_wndReport.GetRecords()->GetCount();
		CString sSpcName;
		int nSpcID = 0;
		double fDistance = 0.0;
		double fCost = 0.0;
		double fMaxCost = 0.0;

		if (nNumOfRecords > 0) 
		{
			for (int i = 0;i < nNumOfRecords;i++)
			{
				CSpecieReportRec *pRec = (CSpecieReportRec*)m_wndReport.GetRecords()->GetAt(i);

				sSpcName = pRec->getColumnText(COLUMN_0);
				nSpcID = getSpecieID(sSpcName);
				fDistance = pRec->getColumnFloat(COLUMN_1);
				fCost = pRec->getColumnFloat(COLUMN_2);
				fMaxCost = pRec->getColumnFloat(COLUMN_3);
				S.Format(NODE_COSTTMPL_TRANSP_SPECIE,nSpcID,sSpcName,fDistance,fCost,fMaxCost);
				arrBuff.Add(S);

			}	// for (int i = 0;i < m_wndReport.GetRecords()->GetCount();i++)
		}	// if (m_wndReport.GetRecords()->GetCount() > 0) 

		arrBuff.Add(NODE_COSTTMPL_TRANSP_END);

		//--------------------------------------------------------------------
		// Add others tag data; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_OTHERS_START);
		S.Format(NODE_COSTTMPL_OTHERS_COST,m_wndEdit3_1.getFloat());
		arrBuff.Add(S);
		S.Format(NODE_COSTTMPL_OTHERS_TYPEOF,m_wndEdit3_2.getText());
		arrBuff.Add(S);
		arrBuff.Add(NODE_COSTTMPL_OTHERS_END);

		//--------------------------------------------------------------------
		// End by adding ENDTAG; 071010 p�d
		arrBuff.Add(NODE_COSTTMPL_END);

		if (arrBuff.GetCount() > 0)
		{
			for (int i = 0;i < arrBuff.GetCount();i++)
			{
				sBuff += arrBuff.GetAt(i);
			}

			// Save Cost template to Database; 071010 p�d
			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					if (m_enumCostTmplState == COSTTMPL_NEW)
					{
						nCostTmplID = -1;	// New template
					}
					else
					{
						nCostTmplID = m_recActiveCostTmpl.getID();
					}

					CTransaction_costtempl rec = CTransaction_costtempl(nCostTmplID,
																												 m_wndEdit0_1.getText(),
																												 COST_TYPE_1,
																												 sBuff,
																												 m_wndEdit0_4.getText(),
																												 m_wndEdit0_2.getText());
					if (!m_pDB->addCostTmpl(rec))
						m_pDB->updCostTmpl(rec);

				}	// if (m_pDB != NULL)
			}	// if (m_bConnected)
			// Reload information
			getCostTemplateFromDB();
			
			if (m_enumCostTmplState == COSTTMPL_NEW)
			{
				m_nDBIndex = m_vecTransaction_costtempl.size() - 1;	// Point to last entered; 071010 p�d
			}

			// Also set: m_enumCostTmplState = COSTTMPL_OPEN; 071205 p�d
			populateData(m_nDBIndex);	
		}	// if (arrBuff.GetCount() > 0)
		return 1;

	}	// if (m_enumCostTmplState != COSTTMPL_NONE)

	return 0;

}

BOOL CMDICostsFormView::isOKToClose(void)
{
	int nReturn = 0;
	if (isNameOfTemplateOK())
	{
		nReturn = saveCostTemplateToDB();
		if (nReturn == -1)
		{
			if (::MessageBox(this->GetSafeHwnd(),(m_sMessage1),(m_sMessageCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
				return TRUE;
			else	
				return FALSE;
		}	// if (nReturn == -1)

		return TRUE;
	}

	return FALSE;
}


int CMDICostsFormView::getSpecieID(LPCTSTR spc_name)
{
	// Use species defined in database table: fst_species_table
	// to establish id of specie; 071011 p�d
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			CTransaction_species rec = m_vecSpecies[i];
			if (rec.getSpcName().Compare(spc_name) == 0)
				return rec.getSpcID();
		}
	}
	return -1;
}

void CMDICostsFormView::OnEnKillfocusEdit8()
{
	isNameOfTemplateOK();
}
