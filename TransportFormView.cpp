// TransportFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDIInfrCostsFormView.h"

#include "TransportFormView.h"

#include "ResLangFileReader.h"

// CTransportFormView

IMPLEMENT_DYNCREATE(CTransportFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTransportFormView, CXTResizeFormView)
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT_1_1, OnReport1KeyDown)
	ON_NOTIFY(NM_CLICK, IDC_REPORT_1_1, OnReport1Click)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT_1_2, OnReport2KeyDown)
	ON_NOTIFY(NM_CLICK, IDC_REPORT_1_2, OnReport2Click)
	ON_BN_CLICKED(IDC_ADD_COLS, &CTransportFormView::OnBnClickedAddCols)
	ON_BN_CLICKED(IDC_DEL_COL, &CTransportFormView::OnBnClickedDelCol)

	ON_BN_CLICKED(IDC_ADD_ROW, &CTransportFormView::OnBnClickedAddRow)
	ON_BN_CLICKED(IDC_DEL_ROW, &CTransportFormView::OnBnClickedDelRow)
	ON_BN_CLICKED(IDC_ADD_ROW2, &CTransportFormView::OnBnClickedAddRow2)
	ON_BN_CLICKED(IDC_DEL_ROW2, &CTransportFormView::OnBnClickedDelRow2)

END_MESSAGE_MAP()

CTransportFormView::CTransportFormView()
	: CXTResizeFormView(CTransportFormView::IDD)
{
	m_bInitialized = FALSE;
	m_enumCostTmplState = COSTTMPL_NONE;
}

CTransportFormView::~CTransportFormView()
{
}

void CTransportFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP_TRANSPORT1_1, m_wndGrpTransport);
	DDX_Control(pDX, IDC_GROUP_TRANSPORT1_2, m_wndGrpTransport2);
	DDX_Control(pDX, IDC_GROUP_TRANSPORT1_3, m_wndGrpTransport3);
	DDX_Control(pDX, IDC_GROUP_TRANSPORT1_4, m_wndGrpTransport4);
	
	DDX_Control(pDX, IDC_LBL1_1, m_wndLbl1);

	DDX_Control(pDX, IDC_EDIT1_1, m_wndEdit1);

	DDX_Control(pDX, IDC_LBL_SET, m_wndLbl2);

	DDX_Control(pDX, IDC_ADD_COLS, m_wndBtnAddCols);
	DDX_Control(pDX, IDC_DEL_COL, m_wndBtnDelCol);
	DDX_Control(pDX, IDC_ADD_ROW, m_wndBtnAddRow);
	DDX_Control(pDX, IDC_DEL_ROW, m_wndBtnDelRow);

	DDX_Control(pDX, IDC_ADD_ROW2, m_wndBtnAddRow2);
	DDX_Control(pDX, IDC_DEL_ROW2, m_wndBtnDelRow2);
	//}}AFX_DATA_MAP

}

void CTransportFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

void CTransportFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit1.SetEnabledColor(BLACK,WHITE);
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit1.SetAsNumeric();

		m_wndBtnAddCols.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelCol.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnAddRow.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelRow.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnAddRow.ModifyStyle(0,BS_MULTILINE);
		m_wndBtnAddRow2.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelRow2.SetXButtonStyle(BS_XT_WINXP_COMPAT );

//		m_wndBtnAddRow.ShowWindow( SW_HIDE );
		m_wndBtnDelRow.ShowWindow( SW_HIDE );
		m_wndBtnAddRow2.ShowWindow( SW_HIDE );
		m_wndBtnDelRow2.ShowWindow( SW_HIDE );

		m_wndGrpTransport.ShowWindow( SW_HIDE );
		m_wndGrpTransport4.ShowWindow( SW_HIDE );

		getSpeciesFromDB();
		
		getCostTemplateFromDB();

		setupReport();
		setupReport2();

		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080207 p�d
		setEnable(m_vecTransaction_costtempl.size() > 0);

		m_bInitialized = TRUE;
	}
}

BOOL CTransportFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CTransportFormView::OnSize(UINT nType,int cx,int cy)
{
/*
	if (m_wndGrpTransport.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpTransport,2,4,130,cy-(cy/2));
	}
*/
	if (m_wndGrpTransport2.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpTransport2,2,4,cx-4,cy-(cy/2),TRUE);
	}

	if (m_wndReport.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndReport,140,20,cx-150,cy-(cy/2)-30);
	}

	if (m_wndGrpTransport3.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpTransport3,2,cy/2+10,cx-4,cy-(cy/2)-14,TRUE);
	}

	if (m_wndReport2.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndReport2,140,cy/2+30,cx-150,cy-(cy/2)-40);
	}
/*
	if (m_wndGrpTransport4.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpTransport4,2,cy/2+10,130,cy-(cy/2)-14);
	}
*/
	CRect rect;
	m_wndBtnDelRow.GetWindowRect(&rect);
	if (m_wndBtnAddRow2.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndBtnAddRow2,15,cy/2+30,rect.Width(),rect.Height());
	}

	if (m_wndBtnDelRow2.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndBtnDelRow2,15,cy/2+57,rect.Width(),rect.Height());
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

// CTransportFormView diagnostics

#ifdef _DEBUG
void CTransportFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTransportFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

LRESULT CTransportFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			setEnable(TRUE);
			m_enumCostTmplState = COSTTMPL_NEW;
			setupNewCostTemplate(TRUE,TRUE);
		}
		break;

		case ID_SAVE_ITEM :
		{
			m_enumCostTmplState = COSTTMPL_OPEN;
		}
		break;
		// Added 071127 p�d
		case ID_DELETE_ITEM :
		{
			m_enumCostTmplState = COSTTMPL_OPEN;
		}
		break;
/*
		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			break;
		}
		case ID_DBNAVIG_END :
		{
			break;
		}	// case ID_NEW_ITEM :
*/
	};
	return 0L;
}
///////////////////////////////////////////////////////////////////////
// Database handling methods; 080207 p�d

void CTransportFormView::getSpeciesFromDB(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getSpecies(m_vecTransactionSpecies);
		}
	}
}

int CTransportFormView::getSpecieID(LPCTSTR spc_name)
{
	// Use species defined in database table: fst_species_table
	// to establish id of specie; 071011 p�d
	if (m_vecTransactionSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionSpecies.size();i++)
		{
			CTransaction_species rec = m_vecTransactionSpecies[i];
			if (rec.getSpcName().Compare(spc_name) == 0)
				return rec.getSpcID();
		}
	}
	return -1;
}

CString CTransportFormView::getSpcNameFromDBSpecies(int id)
{
	// Use species defined in database table: fst_species_table
	// to establish id of specie; 071011 p�d
	if (m_vecTransactionSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecTransactionSpecies.size();i++)
		{
			CTransaction_species rec = m_vecTransactionSpecies[i];
			if (rec.getSpcID() == id)
				return rec.getSpcName();
		}
	}
	return _T("");
}

void CTransportFormView::setEnable(BOOL enabled)
{
	m_wndEdit1.EnableWindow(enabled);
	m_wndEdit1.SetReadOnly(!enabled);
	m_wndBtnAddCols.EnableWindow(enabled);
	m_wndBtnDelCol.EnableWindow(enabled);
	m_wndBtnAddRow.EnableWindow(enabled);
	m_wndBtnDelRow.EnableWindow(enabled);
}

///////////////////////////////////////////////////////////////////////
// Get data by getFormViewByID(...); 080207 p�d
void CTransportFormView::getCostTemplateFromDB(void)
{
	m_vecTransaction_costtempl.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Get Object costtemplates (Redy to use and Under development); 080212 p�d	
			m_pDB->getCostTmpls(m_vecTransaction_costtempl);
		}
	}
}
///////////////////////////////////////////////////////////////////////
// CTransportFormView message handlers
BOOL CTransportFormView::setupReport(void)
{
	if (m_wndReport.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT_1_1, TRUE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Setup interface strings; 080207 p�d
			m_sSpcDistance = (xml->str(IDS_STRING1100));
			m_wndLbl1.SetWindowText((xml->str(IDS_STRING1101)));
			m_wndBtnAddCols.SetWindowText((xml->str(IDS_STRING1102)));
			m_wndBtnDelCol.SetWindowText((xml->str(IDS_STRING1103)));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING1104));
			m_wndBtnAddRow.SetWindowText(xml->str(IDS_STRING1105));
			//m_wndBtnDelRow.SetWindowText((xml->str(IDS_STRING1105)));
			//m_wndBtnAddRow2.SetWindowText((xml->str(IDS_STRING1104)));
			//m_wndBtnDelRow2.SetWindowText((xml->str(IDS_STRING1105)));

			m_wndGrpTransport2.SetWindowText((xml->str(IDS_STRING4200)));
			m_wndGrpTransport3.SetWindowText((xml->str(IDS_STRING4201)));

			if (m_wndReport.GetSafeHwnd() != NULL)
			{
			
				m_wndReport.ShowWindow( SW_NORMAL );

				// Make specie column non scrollable; 080207 p�d
				m_wndReport.SetFreezeColumnsCount(1);

				// Set Dialog caption; 070219 p�d

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_wndReport.GetReportHeader()->AllowColumnRemove(TRUE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.ShowHeader( FALSE );
				m_wndReport.SetFocus();
			}	// if (m_wndReport.GetSafeHwnd() != NULL)
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
	return TRUE;
}

BOOL CTransportFormView::setupReport2(void)
{
	if (m_wndReport2.GetSafeHwnd() == 0)
	{
		// Create the sheet1 list box.
		if (!m_wndReport2.Create(this, IDC_REPORT_1_2, TRUE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			if (m_wndReport2.GetSafeHwnd() != NULL)
			{
			
				m_wndReport2.ShowWindow( SW_NORMAL );

				// Make specie column non scrollable; 080207 p�d
				m_wndReport2.SetFreezeColumnsCount(1);

				// Set Dialog caption; 070219 p�d

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING1001)),110,TRUE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING1002)),100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetVisible( FALSE );

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING1003)),100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING1004)),100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_RIGHT);

				m_wndReport2.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport2.SetMultipleSelection( FALSE );
				m_wndReport2.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport2.FocusSubItems(TRUE);
				m_wndReport2.AllowEdit(TRUE);
				m_wndReport2.SetFocus();
				m_wndReport2.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_wndReport2.GetSafeHwnd() != NULL)
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))
	return TRUE;
}


void CTransportFormView::setupNewCostTemplate(BOOL do_first,BOOL do_second)
{

	CXTPReportColumn *pCol = NULL;
	if (do_first)
	{
		// "Transport till v�g"; 080213 p�d
		m_wndReport.ResetContent();
		// Remove columns; 080213 p�d
		CXTPReportColumns *pCols = m_wndReport.GetColumns();
		if (pCols != NULL)
		{
			pCols->Clear();
		}
		// Recreate first column in report; 080213 p�d
		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""),110,FALSE));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
	
		// Add first record; 080213 p�d
		m_wndReport.AddRecord(new CTransportReportRec_DGV(1,(m_sSpcDistance)));
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}
	
	if (do_second)
	{
		// "Transprt till Industri"; 080325 p�d
		// Just reset content; 090203 p�d
		m_wndReport2.ResetContent();
		// Add first record; 080325 p�d
		//m_wndReport2.AddRecord(new CSpecieReportRec());
		//m_wndReport2.Populate();
		//m_wndReport2.UpdateWindow();
	}

}

//------------------------------------------------------------------------
// Create the transport table in xml-format; 080211 p�d
BOOL CTransportFormView::getTransportTable(CString &xml)
{
	int nSpcID = -1;
	CString sSpcName;
	CString sData;
	CString sTransp;
	CXTPReportColumns *pCols = m_wndReport.GetColumns();
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CTransportReportRec_DGV *pRec = NULL;
	// Check for inforamtion
	if (pRecs == NULL || pCols == NULL) 
		return FALSE;
	// Only one row and one column. I.e. no data entered; 080212 p�d
	if (pRecs->GetCount() == 1 && pCols->GetCount() == 1)
		return FALSE;

	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (CTransportReportRec_DGV *)pRecs->GetAt(i);
		if (i > 0)
			sData = "\n";
		for (int ii = 0;ii < pCols->GetCount();ii++)
		{
			if (i == 0 && ii == 0)	// First row and column
			{
				sData = formatData((NODE_OBJ_COSTTMPL_TRANSP_ATTR1),-1,_T(""));
			} // if (i == 0 && ii == 0)	// First row and column
			//-----------------------------------------------------------
			// Read transport-distances (m)
			else if (i == 0 && ii > 0)	// First row and and columns
			{
				sData += formatData((NODE_OBJ_COSTTMPL_TRANSP_ATTR2),ii,pRec->getColumnInt(ii));
			}
			//-----------------------------------------------------------
			// Read specie information
			else if (i > 0 && ii == 0)	// Next rows and first column
			{
				// Get specie id; 080212 p�d
				sSpcName = pRec->getColumnText(ii);
				nSpcID = getSpecieID(pRec->getColumnText(ii));
				sData += formatData((NODE_OBJ_COSTTMPL_TRANSP_ATTR1),nSpcID,sSpcName);
			}
			//-----------------------------------------------------------
			// Read data price per transport-distance (m)
			else if (i > 0 && ii > 0)	// Next rows and next columns
			{
				sData += formatData((NODE_OBJ_COSTTMPL_TRANSP_ATTR2),ii,pRec->getColumnInt(ii));
			}
		}	// for (int ii = 0;ii < pCols->GetCount();ii++)
		sTransp += formatData((NODE_OBJ_COSTTMPL_TRANSP_ITEM),sData);
	}	// for (int i = 0;i < pRecs->GetCount();i++)
	xml = sTransp;


	return TRUE;
}

void CTransportFormView::setTransportTable(vecObjectCostTemplate_table &vec)
{
	CString S;
	CXTPReportRows *pRows = NULL;
	CXTPReportColumns *pCols = NULL;
	CXTPReportColumn *pCol = NULL;
	CXTPReportRecords *pRecs = NULL;
	CTransportReportRec_DGV *pTranspRec = NULL;
	CObjectCostTemplate_table	table;
	int nRow,nCol;

	pRows = m_wndReport.GetRows();

	// If there's only one row, no data's been entered; 090512 p�d
//	if (pRows != NULL)
//		if (pRows->GetCount() == 1) return;
	
	pCols = m_wndReport.GetColumns();
	// Check for columns in report. If there's already columns
	// remove them, before entering new data; 080213 p�d
	if (pCols != NULL)
			pCols->Clear();

	m_wndReport.ResetContent();

	setupReport();

	if (vec.size() > 0)
	{
		table = vec[0];	// Just to get number of columns (in getValues_int()); 080212 p�d
		//----------------------------------------------------------------------------
		// Start by adding columns to Report; 080213 p�d
		if (table.getValues_int().size() > 0)
		{
			for (nCol = 0;nCol < table.getValues_int().size();nCol++)
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(nCol+1,_T(""),50));
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
			
			}	// for (nCol = 0;nCol < table.getValues_int().size();nCol++)
		}	// if (table.getValue().size() > 0)

		//----------------------------------------------------------------------------
		// Add a record for each row; 080212 p�d
		for (nRow = 0;nRow < vec.size();nRow++)
		{
			// Get information on e.g. number of columns and
			// data in each column/row; 080213 p�d
			table = vec[nRow];
			// Try to get the name of specie set in "fst_species_table" table.
			// Makin' sure the name is up to date in cost-template; 090608 p�d
			table.setSpcName(getSpcNameFromDBSpecies(table.getSpcID()));
			m_mapSpecieUsed[table.getSpcID()] = true;
			pTranspRec = (CTransportReportRec_DGV*)m_wndReport.AddRecord(new	CTransportReportRec_DGV(table.getValues_int().size()+1,table.getSpcName()));
			if (pTranspRec != NULL)
			{
				if (nRow == 0)
					pTranspRec->setColumnText(0,(m_sSpcDistance));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < table.getValues_int().size();nCol++)
				{
					pTranspRec->setColumnInt(nCol+1,table.getValues_int()[nCol]);
				}
			}
		}	// for (int i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	else
	{
		setupNewCostTemplate(TRUE,FALSE);
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


BOOL CTransportFormView::getTransportTable2(CString &xml)
{
	int nSpcID = -1;
	CString sSpcName = _T("");
	double fDistance = 0.0;
	double fCost = 0.0;
	double fMaxCost = 0.0;
	CString sTransp;

	CXTPReportColumns *pCols = m_wndReport2.GetColumns();
	CXTPReportRecords *pRecs = m_wndReport2.GetRecords();
	CSpecieReportRec *pRec = NULL;
	// Check for inforamtion
	if (pRecs == NULL || pCols == NULL) 
		return FALSE;
	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (CSpecieReportRec *)pRecs->GetAt(i);
		sSpcName = (pRec->getColumnText(COLUMN_0));
		nSpcID = getSpecieID(sSpcName);
		fDistance = pRec->getColumnFloat(COLUMN_1);
		fCost = pRec->getColumnFloat(COLUMN_2);
		fMaxCost = pRec->getColumnFloat(COLUMN_3);

		sTransp += formatData((NODE_OBJ_COSTTMPL_TRANSP2_ITEM),
				nSpcID,
				sSpcName,
				fDistance,
				fCost,
				fMaxCost);
	}	// for (int i = 0;i < pRecs->GetCount();i++)
	xml = sTransp;


	return TRUE;
}

void CTransportFormView::setTransportTable2(vecTransaction_costtempl_transport &vec)
{
	CTransaction_costtempl_transport rec;
	if (!m_wndReport2.GetSafeHwnd())
		return;

	m_wndReport2.ResetContent();

	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			rec = vec[i];
			// Try to get the name of specie set in "fst_species_table" table.
			// Makin' sure the name is up to date in cost-template; 090608 p�d
			rec.setSpecieName(getSpcNameFromDBSpecies(rec.getSpcID()));
			if (m_mapSpecieUsed[rec.getSpcID()])
			{
				m_wndReport2.AddRecord(new CSpecieReportRec(rec.getSpecieName(),rec.getDistance(),rec.getCost(),rec.getMaxCost()));
				m_mapSpecieUsed[rec.getSpcID()] = false; // Already used; 092216 p�d
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();

	m_wndReport2.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);

}

void CTransportFormView::addSpecies(vecTransactionSpecies &vec)
{
	CXTPReportRows *pRows1 = m_wndReport.GetRows();
	CXTPReportColumns *pCols1 = m_wndReport.GetColumns();
	CXTPReportRows *pRows2 = m_wndReport2.GetRows();
	CTransportReportRec_DGV *pRec1_1 = NULL;
	CTransportReportRec_DGV *pRec1_2 = NULL;
	CSpecieReportRec *pRec2 = NULL;
	int nNumOfCols,nCol;
	if (vec.size() > 0)
	{

		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// "Transport till V�g"; 080325 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		if (pCols1 != NULL)
		{
			nNumOfCols = pCols1->GetCount();
		}
		if (pRows1 != NULL)
		{
			if (pRows1->GetCount() == 0)	// Add first row
				setupNewCostTemplate(TRUE,FALSE);

			if (pRows1->GetCount() > 1)
				pRec1_1 = (CTransportReportRec_DGV *)pRows1->GetAt(pRows1->GetCount() - 1)->GetRecord();
		}

		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_species rec = vec[i];
			pRec1_2 = (CTransportReportRec_DGV *)m_wndReport.AddRecord(new	CTransportReportRec_DGV(nNumOfCols+1,_T("")));
			if (pRec1_2 != NULL)
			{
				pRec1_2->setColumnText(COLUMN_0,(rec.getSpcName()));
				// Get record and add data to each column for row; 080213 p�d
				for (nCol = 0;nCol < nNumOfCols;nCol++)
				{
					if (nCol > 0)
					{
						if (pRec1_1 != NULL)				
							pRec1_2->setColumnInt(nCol,pRec1_1->getColumnInt(nCol));
						else
							pRec1_2->setColumnInt(nCol,0);
					}	// if (nCol > 0)
				}
			}
		}	// for (int i = 0;i < vec.size();i++)
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
		m_wndReport.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
		m_wndReport.SetFocusedColumn(pCols1->GetAt(0));


		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		// "Transport till Industri"; 080325 p�d
		//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

		// If there's rows, copy data from last entered row to
		// added row and specie; 080325 p�d
		if (pRows2 != NULL)
		{
			if (pRows2->GetCount() > 0)
			{
				pRec2 = (CSpecieReportRec*)pRows2->GetAt(pRows2->GetCount()-1)->GetRecord();
			}	// if (pRows->GetCount() > 1)
		}	// if (pRows != NULL)

		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_species rec = vec[i];
			if (pRec2 == NULL) // No data from last row
				m_wndReport2.AddRecord(new CSpecieReportRec(rec.getSpcName(),0.0,0.0,0.0));
			else	// Data from last row
				m_wndReport2.AddRecord(new CSpecieReportRec(rec.getSpcName(),pRec2->getColumnFloat(COLUMN_1),
																																		 pRec2->getColumnFloat(COLUMN_2),
																																		 pRec2->getColumnFloat(COLUMN_3)));

		}	// for (UINT i = 0;i < vec.size();i++)
		m_wndReport2.Populate();
		m_wndReport2.UpdateWindow();
		m_wndReport2.RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
	}
}

// Get species already used; 080325 p�d
void CTransportFormView::getSpecies(vecIntegers &spc_used)
{
	int nSpcID = -1;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CTransportReportRec_DGV *pRec = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTransportReportRec_DGV *)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				nSpcID = getSpecieID(pRec->getColumnText(COLUMN_0));
				if (nSpcID > 0)
				{
					spc_used.push_back((int)nSpcID);
				}	// if (nSpcID > 0)
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

void CTransportFormView::delSpecie(void)
{
	CXTPReportRows *pRows = NULL;

	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		if (pRows->GetCount() > 1)
		{
			CXTPReportRecord *pRec = pRows->GetAt(pRows->GetCount()-1)->GetRecord();		
			if (pRec != NULL)
			{
				pRec->Delete();			
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
		}
	}	// if (pRows != NULL)

	pRows = m_wndReport2.GetRows();
	if (pRows != NULL)
	{
		if (pRows->GetCount() > 0)
		{
			CXTPReportRecord *pRec = pRows->GetAt(pRows->GetCount()-1)->GetRecord();		
			if (pRec != NULL)
			{
				pRec->Delete();			
				m_wndReport2.Populate();
				m_wndReport2.UpdateWindow();
			}
		}
	}	// if (pRows != NULL)
}

CString CTransportFormView::getLastEnteredSpecie(void)
{
	CString sEmpty;
	CTransportReportRec_DGV *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		if (pRows->GetCount() > 1)
		{
			pRec = (CTransportReportRec_DGV*)pRows->GetAt(pRows->GetCount()-1)->GetRecord();		
			if (pRec != NULL)
			{
				return (pRec->getColumnText(COLUMN_0));
			}	// if (pRec != NULL)
		}	// if (pRows->GetCount() > 1)
	}	// if (pRows != NULL)

	sEmpty.Empty();
	return (sEmpty);
}

//------------------------------------------------------------------------

void CTransportFormView::OnReport1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	if (!m_wndReport.GetSafeHwnd())
		return;

	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CXTPReportColumn *pCol = m_wndReport.GetFocusedColumn();
	if (pRow != NULL && pCol != NULL)
	{
		if (pCol->GetItemIndex() == COLUMN_0)
		{
			if (pRow->GetIndex() == 0)
			{
				pCol->SetEditable(FALSE);
			}
			else
			{
				pCol->SetEditable(TRUE);
			}
		}
		else
		{
			pCol->SetEditable(TRUE);
		}
	}	// if (pRows != NULL && pColumns != NULL)

}

void CTransportFormView::OnReport2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	if (!m_wndReport2.GetSafeHwnd())
		return;

}

void CTransportFormView::OnReport1Click(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (!m_wndReport.GetSafeHwnd())
		return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNMHDR;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			if (pItemNotify->pRow->GetIndex() == 0)
			{
				m_wndReport.EditItem(NULL);
			}
			else
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&m_wndReport, pItemNotify->pRow, pItemNotify->pColumn);
				m_wndReport.EditItem(&itemArgs);
			}
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
	}	// if (pItemNotify != NULL)

}

void CTransportFormView::OnReport2Click(NMHDR* pNMHDR, LRESULT* pResult)
{
	if (!m_wndReport2.GetSafeHwnd())
		return;
}
void CTransportFormView::OnBnClickedAddCols()
{
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CXTPReportColumn *pCol = NULL;
	CTransportReportRec_DGV *pOldRec = NULL;
	CTransportReportRec_DGV *pNewRec = NULL;
	int nNumOfColumns = 0;
	int nAddNumOfCols = m_wndEdit1.getInt();

	//Always add 1 column if no value in number of columns to add. Bug #2214 20110809 J�
		if(nAddNumOfCols==0)
		nAddNumOfCols=1;

	// Check if there's any rows. If not enter the first row
	// holding Headline; 080303 p�d
	if (pRows != NULL)
	{
		if (pRows->GetCount() == 0)
		{
			setupNewCostTemplate(TRUE,FALSE);
		}
	}
	nNumOfColumns = m_wndReport.GetColumns()->GetCount();

	for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)
	{
		pCol = m_wndReport.AddColumn(new CXTPReportColumn(i,_T(""),50));
		pCol->SetAlignment(DT_RIGHT);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();

		CXTPReportRecords *pRecords = m_wndReport.GetRecords();
		CXTPReportColumns *pColumns = m_wndReport.GetColumns();
		CXTPReportRows *pRows = m_wndReport.GetRows();
		if (pColumns != NULL && pRows != NULL && pRecords != NULL)
		{
			for (int nRow = 0;nRow < pRows->GetCount();nRow++)
			{
				pOldRec = (CTransportReportRec_DGV *)pRecords->GetAt(nRow);
				if (pOldRec != NULL)
				{
					pNewRec = new CTransportReportRec_DGV(pColumns->GetCount(),_T(""));
					for (int nCol = 0;nCol < pColumns->GetCount();nCol++)
					{
						if (nCol == 0)
						{
							pNewRec->setColumnText(nCol,(pOldRec->getColumnText(nCol)));
						}
						else
						{
							// As long we are readding columns, use already entered
							// information. Otherwise set new column to 0; 080207 p�d
							if (nCol < nNumOfColumns)
								pNewRec->setColumnInt(nCol,pOldRec->getColumnInt(nCol));
							else
								pNewRec->setColumnInt(nCol,0);
						}
					}	// for (int nCol = 0;nCol < pColumns->GetCount();nCol++)

					pRecords->RemoveAt(nRow);
					pRecords->InsertAt(nRow,pNewRec);
				}	// if (pOldRec != NULL)
			}	// for (int nRow = 0;nRow < pRows->GetCount();nRow++)
		}	// if (pColumns != NULL && pRows != NULL)
	}	// for (int i = nNumOfColumns;i < nAddNumOfCols + nNumOfColumns;i++)

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CTransportFormView::OnBnClickedDelCol()
{
	int nColIndex = 0;
	CXTPReportColumn *pCol = NULL;
	CXTPReportColumns *pColumns = m_wndReport.GetColumns();
	CXTPReportRecords *pRecords = m_wndReport.GetRecords();

	if (pColumns != NULL && pRecords != NULL)
	{
		nColIndex = pColumns->GetCount() - 1;
		// Don't remove first column (name of specie); 080207 p�d
		if (nColIndex > 0)
		{
			pCol = pColumns->GetAt(nColIndex);
			if (pCol != NULL)
			{
				//pRecords->RemoveAt(pCol->GetItemIndex());
				pColumns->Remove(pCol);
			}	// if (pCol)
		}	// if (nColIndex > 1)
		m_wndReport.Populate();
	}
}

// Add a row (Specie); 080207 p�d
// Changed function for this button!
// Instead of adding a row, this button' copy price on the
// first Row (Specie), to all other species/column; 081125 p�d
void CTransportFormView::OnBnClickedAddRow()
{
	CString S;
	int nRow,nCol;
	std::vector<int> vecValue;
	CTransportReportRec_DGV *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	CXTPReportColumns *pCols = m_wndReport.GetColumns();
	if (pRows != NULL && pCols != NULL)
	{
		for (nRow = 1;nRow < pRows->GetCount();nRow++)
		{
			pRec = (CTransportReportRec_DGV*)pRows->GetAt(nRow)->GetRecord();
			if (pRec != NULL)
			{
				if (nRow == 1)	// Get value to be copied; 081125 p�d
				{
					for (nCol = 1;nCol < pCols->GetCount();nCol++)
					{
						vecValue.push_back(pRec->getColumnInt(nCol));
					} // for (int nCol = 1;nCol < pCol->GetCount();nCol++)
				}	// if (nRow == 0)
				else if (nRow > 1)
				{
					for (nCol = 1;nCol < pCols->GetCount();nCol++)
					{
						if (nCol-1 < vecValue.size())
						{
							pRec->setColumnInt(nCol,vecValue[nCol-1]);
						}
					} // for (int nCol = 1;nCol < pCol->GetCount();nCol++)
				}	// else if (nRow > 1)
			}	// if (pRec != NULL)
		}	// for (int nRow = 1;nRow < pRows->GetCount();nRow++)
	}		
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CTransportFormView::OnBnClickedDelRow()
{
	if (m_wndReport.GetRows()->GetCount() > 0)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow(); 
		if (pRow != NULL)
		{
			if (pRow->GetIndex() > 0)
			{
				CXTPReportRecord *pRec = pRow->GetRecord();		
				if (pRec != NULL)
				{
					pRec->Delete();			
					m_wndReport.Populate();
					m_wndReport.UpdateWindow();
				}
			}	// if (pRow->GetIndex() > 0)
		}	// if (pRow != NULL)
	}	// if (m_wndReport.GetRows()->GetCount() > 0)
}

void CTransportFormView::OnBnClickedAddRow2()
{
	m_wndReport2.AddRecord(new CSpecieReportRec());
	
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();
}

void CTransportFormView::OnBnClickedDelRow2()
{
	if (m_wndReport2.GetRows()->GetCount() > 0)
	{
		CXTPReportRow *pRow = m_wndReport2.GetFocusedRow(); 
		if (pRow != NULL)
		{
			if (pRow->GetIndex() >= 0)
			{
				CXTPReportRecord *pRec = pRow->GetRecord();		
				if (pRec != NULL)
				{
					pRec->Delete();			
					m_wndReport2.Populate();
					m_wndReport2.UpdateWindow();
				}
			}	// if (pRow->GetIndex() > 0)
		}	// if (pRow != NULL)
	}	// if (m_wndReport.GetRows()->GetCount() > 0)
}
