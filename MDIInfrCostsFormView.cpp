// MDIInfrCostsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CostsFrame.h"
#include "MDIInfrCostsFormView.h"

#include "TransportFormView.h"
#include "CuttingFormView.h"
#include "RemainingFormView.h"

#include "AddToTemplateDlg.h"
#include "SelectSpeiceDlg.h"

// CMDIInfrCostsFormView

IMPLEMENT_DYNCREATE(CMDIInfrCostsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIInfrCostsFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL_1, OnSelectedChanged)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_EN_KILLFOCUS(IDC_EDIT1, &CMDIInfrCostsFormView::OnEnKillfocusEdit1)
	ON_EN_CHANGE(IDC_EDIT1, &CMDIInfrCostsFormView::OnEnChangeEdit1)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnCBox1Changed)
END_MESSAGE_MAP()

CMDIInfrCostsFormView::CMDIInfrCostsFormView()
	: CXTResizeFormView(CMDIInfrCostsFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDataEnabled = TRUE;
	m_enumTemplateState = COSTTMPL_NONE;
	m_bDoLock = FALSE;
	m_nPrevStatus = -1;
}

CMDIInfrCostsFormView::~CMDIInfrCostsFormView()
{
}

void CMDIInfrCostsFormView::OnDestroy()
{
	m_mapLngStr.clear();
	CXTResizeFormView::OnDestroy();
}

//Lagt till en metod f�r att kontrollera om kostnaden ing�r i en best�ndsmall innan man st�nger f�nstret 20110829 J� Bug#2297
BOOL CMDIInfrCostsFormView::IsOkToClose()
{
	int nDoContinue=0,nDoClose=TRUE;
	if (m_bIsDataEnabled)
	{
		if (isOkToSave(TRUE,TRUE,&nDoContinue))
			saveObjectCostData();
		else
			if(!nDoContinue)
				nDoClose=FALSE;
	}	// if (!m_bIsDataEnabled)

	return nDoClose;
}

void CMDIInfrCostsFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}


void CMDIInfrCostsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP, m_wndGroup1);
	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL2_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL2_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL2_5, m_wndLbl5);
	
	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	//}}AFX_DATA_MAP

}

BOOL CMDIInfrCostsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDIInfrCostsFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (m_wndTabControl.GetSafeHwnd())
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml)
			{
				xml->LoadEx(m_sLangFN,m_mapLngStr);
				delete xml;
			}
		}
		AddView(RUNTIME_CLASS(CTransportFormView), (m_mapLngStr[IDS_STRING4104]), 0);
		AddView(RUNTIME_CLASS(CCuttingFormView), (m_mapLngStr[IDS_STRING4105]), 0);
		AddView(RUNTIME_CLASS(CRemainingFormView), (m_mapLngStr[IDS_STRING4106]), 0);
	}

	return 0;

}

void CMDIInfrCostsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndCBox1.SetEnabledColor(BLACK,WHITE);
		m_wndCBox1.SetDisabledColor(BLACK,COL3DFACE);

		m_wndEdit1.SetEnabledColor(BLACK,WHITE);
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit1.SetLimitText(45);
		m_wndEdit2.SetEnabledColor(BLACK,WHITE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);

		m_wndEdit3.SetEnabledColor(BLACK,WHITE);
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit3.SetReadOnly();
		m_wndEdit3.SetWindowText(getDateEx());
		
		m_wndEdit4.SetEnabledColor(BLACK,WHITE);
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE);

		setLanguage();

		getCostTemplateFromDB();
		m_nDBIndex = m_vecTransaction_costtempl.size() - 1;	// Point to last item
		populateData(m_nDBIndex);

		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080207 p�d
		if (m_vecTransaction_costtempl.size() == 1)
		{
			setNavigationButtons(FALSE,FALSE);	
		}
		else if (m_vecTransaction_costtempl.size() > 1)
		{
			setNavigationButtons(TRUE,FALSE);	
		}
		else
		{
			setNavigationButtons(FALSE,FALSE);	
		}

		m_bInitialized = TRUE;
	}

}

BOOL CMDIInfrCostsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIInfrCostsFormView::OnSetFocus(CWnd *wnd)
{
	m_wndEdit1.SetFocus();

	CXTResizeFormView::OnSetFocus(wnd);
}

void CMDIInfrCostsFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_wndGroup1.GetSafeHwnd())
	{
		m_wndGroup1.MoveWindow(2, 0, cx-2, 105);
	}

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 110, cx, cy-112);
	}

}

void CMDIInfrCostsFormView::doSetNavigationBar()
{
	if (m_vecTransaction_costtempl.size() > 0 || m_enumTemplateState == COSTTMPL_NEW)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}
}


LRESULT CMDIInfrCostsFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	int nDoContinue=0;
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			if (m_bIsDataEnabled)
			{
				if (isOkToSave(TRUE,FALSE,&nDoContinue))
					saveObjectCostData();
			}	// if (!m_bIsDataEnabled)

			setEnable(TRUE);
			m_wndEdit1.EnableWindow(TRUE);
			m_wndEdit1.SetReadOnly(FALSE);
			m_enumTemplateState = COSTTMPL_NEW;
			clearData();			
			m_recActive_costtempl = CTransaction_costtempl();
			// Set toolbar buttons; 080211 p�d
			CMDIInfrCostsFormFrame *pFrame = (CMDIInfrCostsFormFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
			if (pFrame != NULL)
			{
				pFrame->setImportTBtn( TRUE );
				//pFrame->setSpeciesTBtn(TRUE);
			}


			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		}
		break;

		case ID_SAVE_ITEM :
		{
			if (isOkToSave(TRUE,FALSE,&nDoContinue))
				saveObjectCostData();
		}
		break;
		// Added 071127 p�d
		case ID_DELETE_ITEM :
		{
			m_enumTemplateState = COSTTMPL_OPEN;
			removeObjectData();
		}
		break;

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{		
			// Save information	only if there's a name added for
			// template; 080325 p�d
			if (isOkToSave(TRUE,TRUE,&nDoContinue))
			{
				saveObjectCostData();
				nDoContinue=1;
			}
			if(nDoContinue)
			{
					m_nDBIndex = 0;
					setNavigationButtons(FALSE,TRUE);
					populateData(m_nDBIndex);				
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Save information	only if there's a name added for
			// template; 080325 p�d
			if (isOkToSave(TRUE,TRUE,&nDoContinue))
			{
				saveObjectCostData();
				nDoContinue=1;			
			}
			if(nDoContinue)
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);			
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Save information	only if there's a name added for
			// template; 080325 p�d
			if (isOkToSave(TRUE,TRUE,&nDoContinue))
			{
				saveObjectCostData();
				nDoContinue=1;				
			}
			if(nDoContinue)
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTransaction_costtempl.size() - 1))
					m_nDBIndex = (UINT)m_vecTransaction_costtempl.size() - 1;

				if (m_nDBIndex == (UINT)m_vecTransaction_costtempl.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Save information	only if there's a name added for
			// template; 080325 p�d
			if (isOkToSave(TRUE,TRUE,&nDoContinue))
			{
				saveObjectCostData();
				nDoContinue=1;
			}
			if(nDoContinue)
			{
				m_nDBIndex = (UINT)m_vecTransaction_costtempl.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}


// CMDIInfrCostsFormView diagnostics

#ifdef _DEBUG
void CMDIInfrCostsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CMDIInfrCostsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CMDIInfrCostsFormView message handlers

void CMDIInfrCostsFormView::importTemplate()
{
	TCHAR szName[128];
	TCHAR szDoneBy[128];
	CString sXML;
	CString sXMLCompleted;
	CString sFilter;
	CString sNewName;
	int nNameRepeated = 0;
	CTransaction_costtempl recAdded;
	CTransaction_costtempl recCheck;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING1900),COSTTEMPLATE_FN_EXT,COSTTEMPLATE_FN_EXT);
		}
		delete xml;
	}

	// Handles clik on open button
	CFileDialog dlg( TRUE, COSTTEMPLATE_FN_EXT, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		// Setup transport table; 081201 p�d
		ObjectCostsTmplParser pars;
		if (pars.LoadFromFile(dlg.GetPathName()))
		{
			pars.getObjCostsTmplName(szName);
			pars.getObjCostsTmplDoneBy(szDoneBy);
			if (_tcslen(szDoneBy) == 0)
				_tcscpy(szDoneBy,getUserName().MakeUpper());

			pars.getXML(sXML);
			sXMLCompleted.Format(_T("%s%s"),XML_FILE_HEADER,sXML);
			if (m_vecTransaction_costtempl.size() > 0)
			{
				// Before we add the imported template, check to see if
				// we used the name of this template before.
				// If so, we just add a number to this one; 090210 p�d
				getCostTemplateFromDB();

				for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
				{
					recCheck = m_vecTransaction_costtempl[i];
					if (recCheck.getTemplateName().CompareNoCase(szName) == 0)
						nNameRepeated++;
				}	// for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
				// If nNameRepeated > 0 the name's been used before; 090210 p�d
				if (nNameRepeated > 0)
					sNewName.Format(_T("%s(%d)"),szName,nNameRepeated);
				else
					sNewName = szName;
			} // 			if (m_vecTransaction_costtempl.size() > 0)
			else
				sNewName = szName;
	
			recAdded = CTransaction_costtempl(-1,	// New template
																			sNewName,
																			COST_TYPE_NOT_DONE_2,	// "Under utveckling"
																			sXMLCompleted,
																			_T(""),
																			szDoneBy);

			// Only ADD A NEW "Under utveckling"; 081201 p�d
			if (m_pDB != NULL)
			{
				m_pDB->addCostTmpl(recAdded);
			}	// if (m_pDB != NULL)
			// We need to reload templates from database
			// and set m_nDBIncdex to point to the last (just created)
			// template; 081201 p�d
			getCostTemplateFromDB();
			if (m_vecTransaction_costtempl.size() > 0)
				m_nDBIndex = m_vecTransaction_costtempl.size() - 1;	// Point to last item
		}	// if (pars.LoadFromFile(dlg.GetPathName()))
		populateData(m_nDBIndex);
	}	// if(dlg.DoModal() == IDOK)
}


void CMDIInfrCostsFormView::exportTemplate()
{
	CString sName;
	CString sFilter;
	int nDoContinue=0;

	if (!isOkToSave(TRUE,FALSE,&nDoContinue)) return;
				
	saveObjectCostData();
	populateData(m_nDBIndex);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sFilter.Format(_T("%s (*%s)|*%s|"),xml->str(IDS_STRING1900),COSTTEMPLATE_FN_EXT,COSTTEMPLATE_FN_EXT);
		}
		delete xml;
	}
	sName = m_recActive_costtempl.getTemplateName();
	scanFileName(sName);
	if (sName.Right(5) != COSTTEMPLATE_FN_EXT)
		sName += COSTTEMPLATE_FN_EXT;
	// Handles clik on open button
	CFileDialog dlg( FALSE, COSTTEMPLATE_FN_EXT, sName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER , 
									 sFilter, this);
	
	if(dlg.DoModal() == IDOK)
	{
		// Setup transport table; 080212 p�d
		TemplateParser pars;
		if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
		{
			sName = dlg.GetPathName();
			if (sName.Right(5) != COSTTEMPLATE_FN_EXT)
				sName += COSTTEMPLATE_FN_EXT;
			pars.SaveToFile(sName);
		}	// if (pars.LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
	}	// if(dlg.DoModal() == IDOK)
}

void CMDIInfrCostsFormView::addSpeciesToTemplate()
{
	vecIntegers spc_used;
	// Information on Transport; 080325 p�d
	CTransportFormView *pTransport = getTransportPage();
	// Information on Cutting; 080325 p�d
	CCuttingFormView *pCutting = getCuttingPage();
	CSelectSpecieDlg * pDlg = new CSelectSpecieDlg();
	if (pDlg != NULL)
	{
		if (pTransport != NULL)
		{
			pTransport->getSpecies(spc_used);
		}

		pDlg->setSpeciesUsed(spc_used);
		if (pDlg->DoModal() == IDOK)
		{
				if (pTransport != NULL)
				{
					pTransport->addSpecies(pDlg->getSpeciesSelected());
				}

				if (pCutting != NULL)
				{
					pCutting->addSpecies(pDlg->getSpeciesSelected());
				}

		}	// if (pDlg->DoModal() == IDYES)
		delete pDlg;
	}	// if (pDlg != NULL)
}

void CMDIInfrCostsFormView::delSelectedSpecieInTemplate()
{
	CTransportFormView *pTransport = getTransportPage();
	CCuttingFormView *pCutting = getCuttingPage();
	CString sMsg,sSpecie;
	// Get last entered specie i.e. last row in report; 080326 p�d
	if (pTransport != NULL)
	{	
		sSpecie = pTransport->getLastEnteredSpecie();
	}
	sMsg.Format(_T("%s : %s\n\n%s"),m_sMsgSpecie,sSpecie,m_sMsgTemplRemoveSpecie);
	if (::MessageBox(this->GetSafeHwnd(),(sMsg),(m_sMessageCap),MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		return;	// Don't do it; 080326 p�d
	}
	// Information on Transport; 080325 p�d
	if (pTransport != NULL)
	{
		pTransport->delSpecie();
	}
	// Information on Cutting; 080325 p�d
	if (pCutting != NULL)
	{
		pCutting->delSpecie();
	}
}

BOOL  CMDIInfrCostsFormView::isOkToSave(BOOL show_msg,BOOL show_msg2,int *do_continue)
{
	int nActiveID = m_recActive_costtempl.getID(),nRet=FALSE,nCheck=0,nCheck2=0;
	CString sTemplName = _T(""),sTempMsg=_T("");
	sTemplName = m_wndEdit1.getText();
	if (!sTemplName.IsEmpty())
	{
		if (m_vecTransaction_costtempl.size() > 0)
		{
			for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
			{
				if (m_vecTransaction_costtempl[i].getID() != nActiveID && 
				  sTemplName.CompareNoCase(m_vecTransaction_costtempl[i].getTemplateName()) == 0)
				{
					//::MessageBox(this->GetSafeHwnd(),(m_sMsgDuplicateTemplName),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
					//m_wndEdit1.SetWindowText((m_recActive_costtempl.getTemplateName()));
					//m_wndEdit1.SetFocus();

					if(show_msg2)
					{
						sTempMsg=m_sMsgDuplicateTemplName+m_sMsgContinueWithoutSaving;

						if(::MessageBox(this->GetSafeHwnd(),(sTempMsg),(m_sMessageCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
							*do_continue=1;
						else
						{
							m_wndEdit1.SetWindowText((m_recActive_costtempl.getTemplateName()));
							m_wndEdit1.SetFocus();
							*do_continue=0;
						}
					}
					else
					{
						::MessageBox(this->GetSafeHwnd(),(m_sMsgDuplicateTemplName),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
						m_wndEdit1.SetWindowText((m_recActive_costtempl.getTemplateName()));
						m_wndEdit1.SetFocus();
					}
					return nRet;
				}
			}
		}
		nRet=TRUE;
	}
	// Check to see if user's entered a name of the Template.
	// If not, tell'em an get the hell out of Dodge; 080303 p�d
	if (show_msg && nRet==FALSE)
	{
		//::MessageBox(this->GetSafeHwnd(),(m_sMsgTemplNameMissing),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
		if(show_msg2)
		{
			sTempMsg=m_sMsgTemplNameMissing+m_sMsgContinueWithoutSaving;

			if(::MessageBox(this->GetSafeHwnd(),(sTempMsg),(m_sMessageCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				*do_continue=1;
			else
			{
				m_wndEdit1.SetFocus();
				*do_continue=0;
			}
		}
		else
		{
			::MessageBox(this->GetSafeHwnd(),(m_sMsgTemplNameMissing),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
			m_wndEdit1.SetFocus();
		}
	}
	else
	{
		//Kolla om kostnadsmallen ing�r i en best�ndsmall i s� fall inte kunna spara den om status inte �r satt till klar att anv�nda #2297 J�
		nCheck=isCostTemplInStandTamplate();
		nCheck2=m_wndCBox1.GetCurSel();
		if(nCheck && nCheck2== 1)
		{
			if(show_msg2)
			{
				sTempMsg=m_sMsgTemplInStandTemplate+m_sMsgContinueWithoutSaving;
				if(::MessageBox(this->GetSafeHwnd(),(sTempMsg),(m_sMessageCap),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					*do_continue=1;
				else
					*do_continue=0;
			}
			else
				::MessageBox(this->GetSafeHwnd(),(m_sMsgTemplInStandTemplate),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
			nRet=FALSE;
		}
		else
			nRet=TRUE;
	}
	return nRet;
}

BOOL CMDIInfrCostsFormView::isCostTemplInStandTamplate(void)
{
	BOOL bFoundInStandTemplates = FALSE;
	int prlID;
	CTransaction_template recTmpl;
	vecTransactionTemplate vecTmpl;
	TCHAR szFuncName[50];

	// check if current cost template is saved at all
	if( m_recActive_costtempl.getID() == -1 ) return FALSE;

	// Add a check to see if pricelist, to be deleted, maybe in a stand-template.
	// Is so, don't delete, tell user; 081215 p�d
	CUMCostsTmplDB *pDB = new CUMCostsTmplDB(m_dbConnectionData);
	if (pDB != NULL)
	{
		if (pDB->getTemplates(vecTmpl))
		delete pDB;
	}	// if (pDB != NULL)

	// Is there any data to check by; 081215 p�d
	if (vecTmpl.size() > 0)
	{
		TemplateParser pars;
		for (UINT i = 0;i < vecTmpl.size();i++)
		{
			recTmpl = vecTmpl[i];

			if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
			{
				pars.getTemplateCostsTmpl(&prlID,szFuncName);
				if (prlID == m_recActive_costtempl.getID())
				{
					bFoundInStandTemplates = TRUE;
					break;
				}	// if (prlID == recTmpl.getID())
			}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
		}	// for (UINT i = 0;i < vecTmpl.size();i++)
	}	// if (vecTmpl.size() > 0)

	return bFoundInStandTemplates;
}

void CMDIInfrCostsFormView::getCostTemplateFromDB(void)
{
	m_vecTransaction_costtempl.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Get Object costtemplates (Redy to use and Under development); 080212 p�d	
			m_pDB->getCostTmpls(m_vecTransaction_costtempl);
		}
	}
}

void CMDIInfrCostsFormView::setLanguage(void)
{
	if (m_mapLngStr.size() > 0)
	{
		m_wndGroup1.SetWindowText((m_mapLngStr[IDS_STRING400]));

		m_wndLbl1.SetWindowText((m_mapLngStr[IDS_STRING500]));
		m_wndLbl2.SetWindowText((m_mapLngStr[IDS_STRING600]));
		m_wndLbl3.SetWindowText((m_mapLngStr[IDS_STRING700]));
		m_wndLbl4.SetWindowText((m_mapLngStr[IDS_STRING701]));

		// Add to combobox; 080211 p�d
		m_wndLbl5.SetWindowText((m_mapLngStr[IDS_STRING1500]));
		m_wndCBox1.ResetContent();
		m_wndCBox1.AddString((m_mapLngStr[IDS_STRING1501]));
		m_wndCBox1.AddString((m_mapLngStr[IDS_STRING1502]));

		m_sMessageCap = (m_mapLngStr[IDS_STRING800]);
		m_sMessage1 = (m_mapLngStr[IDS_STRING900]);

		m_sMsgTemplNameMissing.Format(_T("%s\n%s"), 
			(m_mapLngStr[IDS_STRING1600]),
			(m_mapLngStr[IDS_STRING1601]));

		m_sMsgTemplInStandTemplate.Format(_T("%s\n%s\n%s"), 
			(m_mapLngStr[IDS_STRING1602]),
			(m_mapLngStr[IDS_STRING1603]),
			(m_mapLngStr[IDS_STRING1604]));

		m_sMsgContinueWithoutSaving.Format(_T("\n%s\n"), 
			(m_mapLngStr[IDS_STRING1605]));

		m_sMsgDuplicateTemplName.Format(_T("%s\n%s\n%s"),
			(m_mapLngStr[IDS_STRING4000]),
			(m_mapLngStr[IDS_STRING4001]),
			(m_mapLngStr[IDS_STRING4002]));
		m_sMsgTemplCopyFrom.Format(_T("%s\n%s\n%s\n\n%s\n\n"),
			(m_mapLngStr[IDS_STRING1708]),
			(m_mapLngStr[IDS_STRING1709]),
			(m_mapLngStr[IDS_STRING1710]),
			(m_mapLngStr[IDS_STRING1711]));

		m_sMsgTemplRemoveSpecie.Format(_T("%s\n\n%s\n\n"),
			(m_mapLngStr[IDS_STRING1712]),
			(m_mapLngStr[IDS_STRING1713]));
		
		m_sMsgSpecie =  (m_mapLngStr[IDS_STRING1001]);

		m_sMsgNoData.Format(_T("%s\n\n%s\n\n"),
			m_mapLngStr[IDS_STRING1800],
			m_mapLngStr[IDS_STRING1801]);

		m_sCostTmplExistsMsg.Format(_T("%s\n%s\n"),
			m_mapLngStr[IDS_STRING1810],
			m_mapLngStr[IDS_STRING1811]);

		m_sMsgCharError.Format(_T("%s < > /"),m_mapLngStr[IDS_STRING2100]);

	}
}

void CMDIInfrCostsFormView::clearData(void)
{
	m_wndCBox1.SetCurSel(1);	// "Under utveckling"
	LockControls();
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(getUserName().MakeUpper());
	m_wndEdit4.SetWindowText(_T(""));

	CTransportFormView *pTransp = getTransportPage();
	if (pTransp != NULL) pTransp->setupNewCostTemplate();

	CCuttingFormView *pCutting = getCuttingPage();
	if (pCutting != NULL) pCutting->setupNewCostTemplate();

	CRemainingFormView *pRemain = getRemainingPage();
	if (pRemain != NULL) pRemain->setupNewCostTemplate();

	m_wndEdit1.SetFocus();
}

void CMDIInfrCostsFormView::setEnable(BOOL enabled)
{
	m_wndCBox1.EnableWindow(enabled);
	m_wndEdit2.EnableWindow(enabled);
	m_wndEdit2.SetReadOnly(!enabled);
	m_wndEdit3.EnableWindow(enabled);
	m_wndEdit3.SetReadOnly(!enabled);
	m_wndEdit4.EnableWindow(enabled);
	m_wndEdit4.SetReadOnly(!enabled);

	m_bIsDataEnabled = enabled;

	m_wndTabControl.getTabPage(1)->SetEnabled(enabled);
	m_wndTabControl.getTabPage(2)->SetEnabled(enabled);
}

// Save object data; collect table data from 
// ALL Tabs, i.e. Transport,Cutting,Higher costs, 
//	Other infr. and Other costs; 080211 p�d
void CMDIInfrCostsFormView::saveObjectCostData()
{
	// First, we'll get general data; 080211 p�d
	CString sName;
	CString sDoneBy;
	CString sDate;
	CString sNotes;
	int nStatus;
	int nTmplID;
	int nSetAs;
	CString sTransp;
	CString sCutting;
	CString sHCosts;
	CString sOInfr;
	CString sOther;
	CString sXML;
	CString sHeader,S, csBuf;
	BOOL bOkToSaveTransport1 = FALSE;
	BOOL bOkToSaveTransport2 = FALSE;
	BOOL bOkToSaveCutting = FALSE;

	nStatus = m_wndCBox1.GetCurSel();	// 0 = Ready, 1 = Development
	sName = m_wndEdit1.getText();
	sDoneBy = m_wndEdit2.getText();
	sDate = m_wndEdit3.getText();
	m_wndEdit4.GetWindowText(sNotes);

	// Start by adding xml header (version and encoding); 080212 p�d
	sXML = XML_FILE_HEADER;
	// Start tag for object cost xml-file
	sXML += NODE_OBJ_COSTTMPL_START;	
	
	//-----------------------------------------------------------------
	// Setup xml-tags for header; 080211 p�d
	sHeader = NODE_OBJ_COSTTMPL_HEADER_START;

	csBuf = sName;
	TextToHtml(&csBuf);
	sHeader += formatData(NODE_OBJ_COSTTMPL_NAME_ITEM, csBuf);

	csBuf = sDoneBy;
	TextToHtml(&csBuf);
	sHeader += formatData(NODE_OBJ_COSTTMPL_DONE_BY_ITEM, csBuf);

	csBuf = sDate;
	TextToHtml(&csBuf);
	sHeader += formatData(NODE_OBJ_COSTTMPL_DATE_ITEM, csBuf);

	sHeader += NODE_OBJ_COSTTMPL_HEADER_END;
	sXML += sHeader;
	//-----------------------------------------------------------------
	// Compile data from tables and add to sXML; 080211 p�d

	// FIRST; information on Transport; 080212 p�d
	CTransportFormView *pTransport = getTransportPage();
	if (pTransport != NULL)
	{
		if (pTransport->getTransportTable(sTransp))
		{
			sXML += NODE_OBJ_COSTTMPL_TRANSP_START;
			sXML += sTransp;
			sXML += NODE_OBJ_COSTTMPL_TRANSP_END;
			bOkToSaveTransport1 = !sTransp.IsEmpty();
		}

		sTransp.Empty();
		if (pTransport->getTransportTable2(sTransp))
		{
			sXML += NODE_OBJ_COSTTMPL_TRANSP2_START;
			sXML += sTransp;
			sXML += NODE_OBJ_COSTTMPL_TRANSP2_END;
			bOkToSaveTransport2 = !sTransp.IsEmpty();
		}
	}

	// SECOND; information on Cutting; 080212 p�d
	CCuttingFormView *pCutting = getCuttingPage();
	if (pCutting != NULL)
	{
		if (pCutting->getCuttingTable(sCutting,&nSetAs))
		{
			sXML += formatData((NODE_OBJ_COSTTMPL_CUTTING_START),nSetAs);
			sXML += sCutting;
			sXML += NODE_OBJ_COSTTMPL_CUTTING_END;
			bOkToSaveCutting = !sCutting.IsEmpty();
		}
	}

	// THIRD; information on Higher costs,Other infring and other costs; 080212 p�d
	CRemainingFormView *pRemaining = getRemainingPage();
	if (pRemaining != NULL)
	{
		if (pRemaining->getOtherCostsTable(sOther))
		{
			sXML += NODE_OBJ_COSTTMPL_OTHER_START;
			sXML += sOther;
			sXML += NODE_OBJ_COSTTMPL_OTHER_END;
		}
	}

	//-----------------------------------------------------------------
	// End tag for object cost xml-file
	sXML += NODE_OBJ_COSTTMPL_END;	
	
	//-----------------------------------------------------------------
	// Compiling XML-file data done. Start to save to database; 080212 p�d
	
	// Check if we're about to create a new template or if it's an
	// already created template; 080212 p�d
//	if (m_enumTemplateState == COSTTMPL_NEW)
//		nTmplID = -1;
//	else if (m_enumTemplateState == COSTTMPL_OPEN)
		nTmplID = m_recActive_costtempl.getID();

	// Check if user's set to "Ready to use" or "Under development"; 080212 p�d
	CTransaction_costtempl data;
	if (nStatus == 0)	// Ready to use
		data = CTransaction_costtempl(nTmplID,sName,COST_TYPE_2,sXML,sNotes,sDoneBy);
	else if (nStatus == 1)	// Under development
		data = CTransaction_costtempl(nTmplID,sName,COST_TYPE_NOT_DONE_2,sXML,sNotes,sDoneBy);

	// Check if it's ok to save; 081128 p�d
	if ((!bOkToSaveTransport1 || !bOkToSaveTransport2 || !bOkToSaveCutting) && nStatus == 0)	// Ready to use; 081128 p�d
	{
		::MessageBox(this->GetSafeHwnd(),(m_sMsgNoData),(m_sMessageCap),MB_ICONEXCLAMATION | MB_OK);
		// Change to "Under utveckling"; 090310 p�d
		data = CTransaction_costtempl(nTmplID,sName,COST_TYPE_NOT_DONE_2,sXML,sNotes,sDoneBy);
		// Repopulate; 081128 p�d
//		populateData(m_nDBIndex);
//		return;
	}

	if (m_pDB != NULL)
	{
		if (!m_pDB->addCostTmpl(data))
			m_pDB->updCostTmpl(data);
	}

	// After save, reload data	
	getCostTemplateFromDB();

	if (m_enumTemplateState == COSTTMPL_NEW || ((!bOkToSaveTransport1 || !bOkToSaveTransport2 || !bOkToSaveCutting) && nStatus == 0))
	{
		// Make sure we're pointing to the Last entry
		m_nDBIndex = m_vecTransaction_costtempl.size() - 1;
		setNavigationButtons(TRUE,FALSE);
		populateData(m_nDBIndex);
	}

	m_enumTemplateState = COSTTMPL_OPEN;

/*
	CFile f( "c:\\xml_test.xml", CFile::modeCreate|CFile:: modeReadWrite);
	f.Write(sXML,sXML.GetLength());
*/
	
}

// Added 080213 p�d
void CMDIInfrCostsFormView::removeObjectData()
{
	BOOL bOkToRemove = TRUE;
	CTransaction_template recTmpl;
	vecTransactionTemplate vecTmpl;
	int costID;
	TCHAR szFuncName[50];

	if( m_recActive_costtempl.getID() != -1 )	// check if current cost template is saved at all (id != -1)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				// Add a check to see if pricelist, to be deleted, maybe in a stand-template.
				// Is so, don't delete, tell user; 081215 p�d
				if (m_pDB != NULL)
				{
					m_pDB->getTemplates(vecTmpl);
				}	// if (pDB != NULL)

				// Is there any data to check by; 081215 p�d
				if (vecTmpl.size() > 0)
				{
					TemplateParser pars;
					for (UINT i = 0;i < vecTmpl.size();i++)
					{
						recTmpl = vecTmpl[i];

						if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))
						{
							pars.getTemplateCostsTmpl(&costID,szFuncName);
							if (costID == m_recActive_costtempl.getID())
							{
								::MessageBox(this->GetSafeHwnd(),m_sCostTmplExistsMsg,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
								bOkToRemove = FALSE;
								break;
							}	// if (prlID == recTmpl.getID())
						}	// if (pars.LoadFromBuffer(recTmpl.getTemplateFile()))

					}	// for (UINT i = 0;i < vecTmpl.size();i++)
				}	// if (vecTmpl.size() > 0)
			}	// if (fileExists(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
	}
	if (bOkToRemove)
	{
		if (::MessageBox(this->GetSafeHwnd(),m_sMessage1,m_sMessageCap,MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
		{		
			if (m_pDB->delCostTmpl(m_recActive_costtempl))
			{
			}
		}	// if (::MessageBox(this->GetSafeHwnd(),_T(m_sMsgDelTemplate),_T(m_sMsgCap),MB_ICONEXCLAMATION | MB_YESNO) == IDYES)
	} // if (bOkToRemove)

	getCostTemplateFromDB();
	if (m_vecTransaction_costtempl.size() > 0)	m_nDBIndex = m_vecTransaction_costtempl.size() - 1;
	else m_nDBIndex = -1;

	populateData(m_nDBIndex);
	doSetNavigationBar();
}	

// Set navigation buttons in shell, depending on index of item to display; 080212 p�d
void CMDIInfrCostsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Make sure the New Toolbarbutton on Main Toolbar is enbaled; 070308 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
}

void CMDIInfrCostsFormView::setNavigationButtonsEx(void)
{
	setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < m_vecTransaction_costtempl.size()-1);	
}

void CMDIInfrCostsFormView::populateData(int idx, bool bLoadStatusFromDB)
{
	int nSetAs;
	vecObjectCostTemplate_table vec,vecDGV,vecM3FUB;
	CObjectCostTemplate_table table;
	BOOL bIsCostsInStandTmpl = FALSE;
	vecTransaction_costtempl_transport vec2;
	vecObjectCostTemplate_other_cost_table vecOtherCosts;
	CTransaction_costtempl_cutting recCutting;

	// Setup templates info; 080303 p�d
	if (m_vecTransaction_costtempl.size() > 0 &&
			idx >= 0 &&
			idx < m_vecTransaction_costtempl.size())
	{

		m_recActive_costtempl = m_vecTransaction_costtempl[idx];
		m_nDBIndex = idx;

		bIsCostsInStandTmpl = isCostTemplInStandTamplate();

		// Setup header information; 080212 p�d
		if(bLoadStatusFromDB) m_wndCBox1.SetCurSel((m_recActive_costtempl.getTypeOf() == COST_TYPE_2) ? 0 : 1);
		m_wndEdit1.SetWindowText(m_recActive_costtempl.getTemplateName());
		LockControls();

		// Set toolbar buttons; 080211 p�d
		CMDIInfrCostsFormFrame *pFrame = (CMDIInfrCostsFormFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
		if (pFrame != NULL)
		{
			pFrame->setImportTBtn( TRUE );
		}

		if (bIsCostsInStandTmpl)
		{
			m_wndEdit1.SetDisabledColor(BLACK,INFOBK);
			if (!m_bDoLock)
			{
				m_wndEdit1.EnableWindow(FALSE);
				m_wndEdit1.SetReadOnly(TRUE);
			}
			else
			{
				// Lock/Unlock
				m_wndEdit1.EnableWindow(!m_bDoLock);
				m_wndEdit1.SetReadOnly(m_bDoLock);
			}
		}
		else
		{
			m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
			if (!m_bDoLock)
			{
				m_wndEdit1.EnableWindow(TRUE);
				m_wndEdit1.SetReadOnly(FALSE);
			}
			else
			{
				// Lock/Unlock
				m_wndEdit1.EnableWindow(!m_bDoLock);
				m_wndEdit1.SetReadOnly(m_bDoLock);
			}
		}

		m_wndEdit2.SetWindowText(m_recActive_costtempl.getCreatedBy());
		m_wndEdit4.SetWindowText(m_recActive_costtempl.getTemplateNotes());

		// Setup transport table; 080212 p�d
		//ObjectCostsTmplParser *pars = new ObjectCostsTmplParser();
		xmlliteCostsParser *pars = new xmlliteCostsParser();
		if (pars != NULL)
		{
//			if (pars->LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))
			if (pars->loadStream(m_recActive_costtempl.getTemplateFile()))
			{
				// Transport table "Framk�rning ex. skotare"; 080313 p�d
				CTransportFormView *pTransport = getTransportPage();
				if (pTransport != NULL)
				{
					pars->getObjCostsTmplTransport(vec);
					pTransport->setTransportTable(vec);
					// Transport table "Transport till industri (Massaved)"; 080313 p�d
					pars->getObjCostsTmplTransport2(vec2);
					pTransport->setTransportTable2(vec2);
				}
				// Cutting table
				pars->getCutCostSetAs(&nSetAs);
				pars->getObjCostsTmplCutting2(recCutting);
				pars->getObjCostsTmplCutting(vecDGV,vecM3FUB);
				//if (vec.size() > 0)
				//{
					CCuttingFormView *pCutting = getCuttingPage();
					if (pCutting != NULL)
					{
						//Skickar vidare m_bDoLock f�r att veta om huggningskostnader kall s�ttas aktiv eller ej
						//Bug #2544 20111117 J�
						pCutting->setCuttingTable(vecDGV,vecM3FUB,recCutting,nSetAs,m_bDoLock);
					}	// if (pCutting != NULL)
				//}	// if (vec.size() > 0)
				// Higher costs,Other infr. costs and other costs; 080213 p�d
				pars->getObjCostsTmplOther(vecOtherCosts);
				CRemainingFormView *pRemaining = getRemainingPage();
				if (pRemaining != NULL)
				{
					if (vecOtherCosts.size() >= 0)
					{
						pRemaining->setOtherCostTable(vecOtherCosts);
											}
				}	// if (pRemaining != NULL)
			}	// if (pars->LoadFromBuffer(m_recActive_costtempl.getTemplateFile()))

			delete pars;

		}	// if (pars != NULL)
		// We definitly has one item open; 080212 p�d
		m_enumTemplateState = COSTTMPL_OPEN;
	}	// if (m_vecTransaction_costtempl.size() > 0 &&
	else
	{
		clearData();
		setEnable(FALSE);
		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit1.EnableWindow(FALSE);
		m_wndEdit1.SetReadOnly(TRUE);
		CTransportFormView *pTransport = getTransportPage();
		if (pTransport != NULL) pTransport->setEnable(FALSE);
		// Set toolbar buttons; 080211 p�d
		CMDIInfrCostsFormFrame *pFrame = (CMDIInfrCostsFormFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
		if (pFrame != NULL)
		{
			pFrame->setImportTBtn( FALSE );
		}

		//S�tt tabview till 0 Bug #2545 20111117 J�
		m_wndTabControl.SetSelectedItem(m_wndTabControl.getTabPage(0));
	}

	// Set Shell toolbar; 090210 p�d
	if (m_vecTransaction_costtempl.size() <= 1)
		setNavigationButtons(FALSE,FALSE);
	else
		setNavigationButtons(idx > 0,idx < m_vecTransaction_costtempl.size()-1);

	doSetNavigationBar();
}

BOOL CMDIInfrCostsFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

// PUBLIC
vecTransaction_costtempl &CMDIInfrCostsFormView::getTransactionCostTempl(void)
{
	return m_vecTransaction_costtempl;
}

void CMDIInfrCostsFormView::OnEnKillfocusEdit1()
{
}

void CMDIInfrCostsFormView::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CXTResizeFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	CString sText;
	m_wndEdit1.GetWindowTextW(sText);
	int nIndex = sText.FindOneOf(_T("<>/"));
	if (nIndex > -1)
	{
		::MessageBox(this->GetSafeHwnd(),m_sMsgCharError,m_sMessageCap,MB_ICONEXCLAMATION | MB_OK);
		sText.Delete(nIndex);
		m_wndEdit1.SetWindowTextW(sText);
		m_wndEdit1.SetSel(sText.GetLength(),sText.GetLength());

	}
}


void CMDIInfrCostsFormView::OnCBox1Changed(void)
{
	//Kolla om kostnadsmallen ing�r i en best�ndsmall i s� fall inte kunna spara den om status inte �r satt till klar att anv�nda #2297 J�
	BOOL bCheck = isCostTemplInStandTamplate();
	int nCheck2 = m_wndCBox1.GetCurSel();
	if( bCheck && nCheck2 == 1 )	// konstandsmall ing�r i best�ndsmall och har status "Under utveckling"
	{
		if( m_nPrevStatus != nCheck2 )	// kolla s� att vi inte har valt samma status som sist, ingen id� att uppdatera gr�nssnittet d�
			populateData(m_nDBIndex, FALSE);
	}
	else	// spara kostnadsmall och uppdatera gr�nssnitt
	{
		saveObjectCostData();
		populateData(m_nDBIndex);
	}

	m_nPrevStatus = nCheck2;

	LockControls();
	CheckTabView(0);
}

void CMDIInfrCostsFormView::LockControls()
{
	// Lock fields; 111026 Peter
	m_bDoLock = (m_wndCBox1.GetCurSel() == 0);
	//G�r en extra koll h�r om l�gg till tr�dslagsknapparna skall t�ndas eller sl�ckas bug #2521 20111109 J�
	if ((m_vecTransaction_costtempl.size() <= 0) && (m_enumTemplateState != COSTTMPL_NEW))
		m_bDoLock=TRUE;


	CMDIInfrCostsFormFrame *pFrame = (CMDIInfrCostsFormFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	if (pFrame != NULL)
			pFrame->setSpeciesTBtn( !m_bDoLock );

	if (!m_bDoLock) setEnable(TRUE);

	// Check if we should Lock data; 100602 p�d
	m_wndEdit2.EnableWindow(!m_bDoLock);
	m_wndEdit2.SetReadOnly(m_bDoLock);
	m_wndEdit3.EnableWindow(!m_bDoLock);
	m_wndEdit3.SetReadOnly(m_bDoLock);
	m_wndEdit4.EnableWindow(!m_bDoLock);
	m_wndEdit4.SetReadOnly(m_bDoLock);

	CTransportFormView *pTransport = getTransportPage();
	if( pTransport ) pTransport->setLockTransportView(m_bDoLock);
	CCuttingFormView *pCutting = getCuttingPage();
	if( pCutting ) pCutting->setLockCuttingView(m_bDoLock);
	CRemainingFormView *pRemaining = getRemainingPage();
	if( pRemaining ) pRemaining->setLockRemainingView(m_bDoLock);
}

//Lagt till en koll p� vilken tab som �r aktiverad, om �vriga kostnader sl�ck tr�dslagsknappar
//Bug #2565 20111117 J�
void CMDIInfrCostsFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	CheckTabView(1);
}
//Lagt till en koll p� vilken tab som �r aktiverad, om �vriga kostnader sl�ck tr�dslagsknappar
//Bug #2565 20111122 J�
void CMDIInfrCostsFormView::CheckTabView(int nDoLock)
{
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		CMDIInfrCostsFormFrame *pFrame = (CMDIInfrCostsFormFrame*)getFormViewByID(IDD_FORMVIEW2)->GetParent();
		switch (pItem->GetIndex())
		{
		case 2:			
			if (pFrame != NULL)
			{					
				pFrame->setSpeciesTBtn( FALSE );
			}
			break;
		default:
			if(nDoLock)
				LockControls();
			break;
		};
	}	// if (pItem != NULL)
}