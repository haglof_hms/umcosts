#pragma once

#include "UMCostsTmplDB.h"

#include "Resource.h"

// CSelectSpecieDlg dialog

class CSelectSpecieDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectSpecieDlg)
	CString m_sAbrevLangSet;
	// Setup language filename; 051214 p�d
  CString m_sLangFN;

	vecTransactionSpecies m_vecSpecies;
	vecTransactionSpecies m_vecSpeciesSelected;

	vecIntegers m_vecSpeciesUsed;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	CUMCostsTmplDB *m_pDB;

protected:
	CXTListCtrl m_wndListCtrl;
	CXTHeaderCtrl   m_header;

	CMyExtStatic m_wndLbl1;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	void setupListCtrl(void);
	void setLanguage(void);

	BOOL isSpecieUsed(int spc_id);

	CTransaction_species* getSpecie(int spc_id);

public:
	CSelectSpecieDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSpecieDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	vecTransactionSpecies getSpeciesSelected(void);
	void setSpeciesUsed(vecIntegers &spc_used);

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSpeciesDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSelectSpeciesDlg)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
