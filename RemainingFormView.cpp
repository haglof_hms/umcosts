// RemainingFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MDIInfrCostsFormView.h"
#include "RemainingFormView.h"
#include "ResLangFileReader.h"

// CRemainingFormView

IMPLEMENT_DYNCREATE(CRemainingFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CRemainingFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_BN_CLICKED(IDC_ADD_ROW4, &CRemainingFormView::OnBnClickedAddRow4)
	ON_BN_CLICKED(IDC_DEL_ROW4, &CRemainingFormView::OnBnClickedDelRow4)
	ON_BN_CLICKED(IDC_ADD_ROW5, &CRemainingFormView::OnBnClickedAddRow5)
	ON_BN_CLICKED(IDC_DEL_ROW5, &CRemainingFormView::OnBnClickedDelRow5)
END_MESSAGE_MAP()

CRemainingFormView::CRemainingFormView()
	: CXTResizeFormView(CRemainingFormView::IDD)
{
	m_bInitialized = FALSE;
}

CRemainingFormView::~CRemainingFormView()
{
}

void CRemainingFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP_OTHER3_3, m_wndGrpOther3_3);
	DDX_Control(pDX, IDC_ADD_ROW5, m_wndBtnAddRow2);
	DDX_Control(pDX, IDC_DEL_ROW5, m_wndBtnDelRow2);
//}}AFX_DATA_MAP
}


void CRemainingFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndBtnAddRow2.SetXButtonStyle(BS_XT_WINXP_COMPAT );
		m_wndBtnDelRow2.SetXButtonStyle(BS_XT_WINXP_COMPAT );

		setupReport3();	// Also set strings; 080208 p�d

		getCostTemplateFromDB();

		// Setup enabled/disabled depending on
		// if there's any data in "cost_template_table"
		// for cost templates "intr�ngsv�rdering"; 080211 p�d
		setEnable(m_vecTransaction_costtempl.size() > 0);

		m_bInitialized = TRUE;
	}
}

// CRemainingFormView diagnostics

#ifdef _DEBUG
void CRemainingFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CRemainingFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CRemainingFormView message handlers
BOOL CRemainingFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CRemainingFormView::OnSize(UINT nType,int cx,int cy)
{
	if (m_wndGrpOther3_3.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndGrpOther3_3,2,5,cx-215,cy-10,TRUE);
	}

	if (m_wndReport3.m_hWnd)
	{
		// Need to set size of Report control; 051219 p�d
		setResize(&m_wndReport3,10,55,cx-240,cy-65);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

LRESULT CRemainingFormView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			m_enumCostTmplState = COSTTMPL_NEW;
			setupNewCostTemplate();
			setEnable(TRUE);
		}
		break;
	};
	return 0L;
}

void CRemainingFormView::setEnable(BOOL enbaled)
{
	m_wndBtnAddRow2.EnableWindow(enbaled);
	m_wndBtnDelRow2.EnableWindow(enbaled);;
}

void CRemainingFormView::setupNewCostTemplate(void)
{
	// Add a row; "�vriga fasta kostnader, kan v�lja om det skall l�ggas p� Moms p� kostnad"
	m_wndReport3.ClearReport();
	m_wndReport3.AddRecord(new COtherCostsReportRec());
	m_wndReport3.Populate();
	m_wndReport3.UpdateWindow();

}
////////////////////////////////////////////////////////////////////
// Database handling; 080213 p�d
void CRemainingFormView::getCostTemplateFromDB(void)
{
	m_vecTransaction_costtempl.clear();
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Get Object costtemplates (Redy to use and Under development); 080212 p�d	
			m_pDB->getCostTmpls(m_vecTransaction_costtempl);
		}
	}
}

void CRemainingFormView::setOtherCostTable(vecObjectCostTemplate_other_cost_table &vec)
{

	CObjectCostTemplate_other_cost_table table;
	// Clear report
	m_wndReport3.ResetContent();
	m_wndReport3.ClearReport();
	// Start adding records to  report; 080213 p�d
	for (UINT i = 0;i < vec.size();i++)
	{
		table = vec[i];

		m_wndReport3.AddRecord(new COtherCostsReportRec(table.getPrice(),table.getNotes(),table.getIsVAT()));
	}
	m_wndReport3.Populate();
	m_wndReport3.UpdateWindow();

}

BOOL CRemainingFormView::getOtherCostsTable(CString &xml)
{

	CString sData;
	CString sOther;
	CXTPReportRecords *pRecs = m_wndReport3.GetRecords();
	COtherCostsReportRec *pRec = NULL;
	// Check for inforamtion
	if (pRecs == NULL)
		return NULL;

	CString csText;
	for (int i = 0;i < pRecs->GetCount();i++)
	{
		pRec = (COtherCostsReportRec *)pRecs->GetAt(i);

		csText = pRec->getColumnText(COLUMN_1);
		TextToHtml(&csText);

		sData += formatData(NODE_OBJ_COSTTMPL_OTHER_ATTR1_1,
						pRec->getColumnFloat(COLUMN_0),
						csText,
						pRec->getColumnFloat(COLUMN_2));
		sOther += formatData(NODE_OBJ_COSTTMPL_OTHER_ITEM,sData);
		sData.Empty();
	}	// for (int i = 0;i < pRecs->GetCount();i++)

	xml = sOther;

	return TRUE;
}
//------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////
// Database handling methods; 080207 p�d

BOOL CRemainingFormView::setupReport3(void)
{
	if (m_wndReport3.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport3.Create(this, IDC_REPORT_3_3,FALSE,FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport3.GetSafeHwnd() != NULL)
				{
				
					// Setup interface strings; 080207 p�d
					m_wndGrpOther3_3.SetWindowText((xml->str(IDS_STRING1309)));
					m_wndBtnAddRow2.SetWindowText((xml->str(IDS_STRING1301)));
					m_wndBtnDelRow2.SetWindowText((xml->str(IDS_STRING1302)));

					m_wndReport3.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport3.AddColumn(new CXTPReportColumn(COLUMN_0, ((xml->str(IDS_STRING1310))),60,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_RIGHT);

					pCol = m_wndReport3.AddColumn(new CXTPReportColumn(COLUMN_1, ((xml->str(IDS_STRING1311))),200));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);

					pCol = m_wndReport3.AddColumn(new CXTPReportColumn(COLUMN_2, ((xml->str(IDS_STRING1312))),40,FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->SetAlignment(DT_CENTER);

					m_wndReport3.GetReportHeader()->AllowColumnRemove(TRUE);
					m_wndReport3.SetMultipleSelection( FALSE );
					m_wndReport3.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport3.FocusSubItems(TRUE);
					m_wndReport3.AllowEdit(TRUE);
					m_wndReport3.GetPaintManager()->SetFixedRowHeight(FALSE);
				}	// if (m_wndReport3.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CRemainingFormView::OnBnClickedAddRow4()
{
	// NOT USED 2008-05-20 P�D
}

void CRemainingFormView::OnBnClickedDelRow4()
{
	// NOT USED 2008-05-20 P�D
}

void CRemainingFormView::OnBnClickedAddRow5()
{

	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	// Add a row; "�vriga fasta kostnader, kan v�lja om det skall l�ggas p� Moms p� kostnad"
	m_wndReport3.AddRecord(new COtherCostsReportRec());
	m_wndReport3.Populate();
	m_wndReport3.UpdateWindow();
	pRows = m_wndReport3.GetRows();
	if (pRows != NULL)
	{
		pRow = pRows->GetAt(pRows->GetCount() - 1);
		if (pRow != NULL)
		{
			m_wndReport3.SetFocusedRow(pRow);
			m_wndReport3.SetFocus();
		}
	}
}

void CRemainingFormView::OnBnClickedDelRow5()
{

	if (m_wndReport3.GetRows()->GetCount() > 0)
	{
		CXTPReportRow *pRow = m_wndReport3.GetFocusedRow(); 
		if (pRow != NULL)
		{
			if (pRow->GetIndex() >= 0)
			{
				CXTPReportRecord *pRec = pRow->GetRecord();		
				if (pRec != NULL)
				{
					pRec->Delete();			
					m_wndReport3.Populate();
					m_wndReport3.UpdateWindow();
				}
			}	// if (pRow->GetIndex() > 0)
		}	// if (pRow != NULL)
	}	// if (m_wndReport2.GetRows()->GetCount() > 0)

}
