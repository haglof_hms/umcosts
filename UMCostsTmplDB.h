#if !defined(AFX_UMCOSTSTMPLDB_H)
#define AFX_UMCOSTSTMPLDB_H

#include "StdAfx.h"

class CUMCostsTmplDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL templateExist(CTransaction_costtempl &);
public:
	CUMCostsTmplDB(void);
	CUMCostsTmplDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMCostsTmplDB(DB_CONNECTION_DATA &db_connection);

	// Template database items; 070824 p�d
	BOOL getCostTmpls(vecTransaction_costtempl &,int tmpl_type);
	BOOL getCostTmpls(vecTransaction_costtempl &);
	BOOL getCostTmplsExclude(vecTransaction_costtempl &,int);
	BOOL addCostTmpl(CTransaction_costtempl &);
	BOOL updCostTmpl(CTransaction_costtempl &);
	BOOL delCostTmpl(CTransaction_costtempl &);

	// External database item.
	BOOL getSpecies(vecTransactionSpecies &);

	// Template database items; 070824 p�d
	BOOL getTemplates(vecTransactionTemplate &);

};

#endif