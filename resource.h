//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMCosts.rc
//
#define IDD_FORMVIEW                    5150
#define IDD_FORMVIEW1                   5151
#define IDD_FORMVIEW2                   5152
#define IDD_FORMVIEW3                   5153
#define IDD_FORMVIEW4                   5154
#define IDD_FORMVIEW5                   5155
#define IDD_FORMVIEW6                   5156
#define IDD_FORMVIEW7                   5157
#define IDD_FORMVIEW8                   5158
#define IDI_FORMVIEW                    9000
#define IDC_ADD_SPECIE                  9000
#define IDC_GRP_TRANSPORT               9001
#define IDC_GRP_CUTTING                 9002
#define IDR_TOOLBAR1                    9002
#define IDC_GRP_OTHER                   9003
#define IDC_EDIT1                       9004
#define IDD_DIALOG1                     9004
#define IDC_EDIT2                       9005
#define IDD_DIALOG2                     9005
#define IDC_EDIT3                       9006
#define IDC_EDIT4                       9007
#define IDR_TOOLBAR2                    9007
#define IDC_GRP_NOTES                   9008
#define IDC_LBL2_1                      9009
#define IDC_LBL2_2                      9010
#define IDC_LBL2_3                      9011
#define IDC_LBL2_4                      9012
#define IDC_EDIT5                       9013
#define IDC_LBL2_5                      9013
#define IDC_EDIT6                       9014
#define IDC_EDIT7                       9015
#define IDC_LBL3_1                      9016
#define IDC_LBL3_2                      9017
#define IDC_EDIT8                       9018
#define IDC_GROUP                       9019
#define IDC_LBL0_1                      9020
#define IDC_ADD_COLS                    9021
#define IDC_EDIT9                       9022
#define IDC_LBL0_2                      9023
#define IDC_DEL_COL                     9024
#define IDC_EDIT10                      9025
#define IDC_LBL1_1                      9026
#define IDC_ADD_ROW                     9029
#define IDC_LBL0_3                      9030
#define IDC_GROUP_TRANSPORT1_1          9032
#define IDC_GROUP_TRANSPORT1_2          9033
#define IDC_DEL_SPECIE                  9034
#define IDC_GROUP_CUTTING2_1            9035
#define IDC_GROUP_CUTTING2_2            9036
#define IDC_ADD_COLS2                   9037
#define IDC_DEL_COLS2                   9038
#define IDC_DEL_ROW                     9039
#define IDC_ADD_ROW2                    9040
#define IDC_DEL_ROW2                    9042
#define IDC_EDIT2_1                     9043
#define IDC_GROUP_OTHER3_1              9043
#define IDC_EDIT1_1                     9044
#define IDC_GROUP_OTHER3_2              9044
#define IDC_ADD_ROW3                    9045
#define IDC_DEL_ROW3                    9046
#define IDC_GROUP_OTHER3_3              9047
#define IDC_ADD_ROW4                    9048
#define IDC_COMBO1                      9048
#define IDC_DEL_ROW4                    9049
#define IDC_GROUP_TRANSPORT1_3          9049
#define IDC_ADD_ROW5                    9050
#define IDC_GROUP_TRANSPORT1_4          9050
#define IDC_DEL_ROW5                    9051
#define IDC_RADIO1                      9053
#define IDC_RADIO2                      9054
#define IDC_GROUP_CUTTING2_3            9055
#define IDC_GROUP_CUTTING2_4            9056
#define IDC_RADIO3                      9057
#define IDC_EDIT2_2                     9058
#define IDC_EDIT2_3                     9059
#define IDC_EDIT2_4                     9060
#define IDC_EDIT2_5                     9061
#define IDC_LIST1                       9062
#define IDC_LBL1                        9063
#define IDC_LBL_SET                     9066
#define ID_BUTTON32775                  32775
#define ID_TBTN_PREVIEW                 32775
#define ID_BUTTON32776                  32776
#define ID_TBTN_PRINT_OUT               32776
#define ID_BUTTON32777                  32777
#define ID_TBBTN_SETTINGS               32777
#define ID_TBTN_IMPORT                  33771
#define ID_TBTN_ADD_SPECIES             33772
#define ID_TBTN_DEL_SPECIE              33773
#define ID_TBTN_EXPORT                  33774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        9009
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         9067
#define _APS_NEXT_SYMED_VALUE           9000
#endif
#endif
