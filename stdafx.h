// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <SQLAPI.h> // main SQLAPI++ header

#define _USE_MATH_DEFINES
#include <math.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

// ... in PAD_HMSFuncLib
#include "pad_hms_miscfunc.h"
#include "pad_transaction_classes.h"	// HMSFuncLib header
#include "coststmplparser.h"
#include "templateparser.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"


#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally
#define MSG_IN_SUITE2				 				(WM_USER + 11)		// This identifer's used to send messages internally

#define ID_TABCONTROL_1								0x9001
#define ID_TABCONTROL_2								0x9002

#define IDC_REPORT									0x9010
#define IDC_REPORT_1_1								0x9011
#define IDC_REPORT_1_2								0x9012
#define IDC_REPORT_DGV								0x9013
#define IDC_REPORT_M3FUB							0x9014
#define IDC_REPORT_3_1								0x9015
#define IDC_REPORT_3_2								0x9016
#define IDC_REPORT_3_3								0x9017
#define IDC_REPORT_4								0x9018
#define IDC_TEMPLATE_LIST							0x9020

#define ID_PRINT_OUT_PRL							0x9021

#define ID_SHOWVIEW_MSG								0x9116

/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING100							100
#define IDS_STRING200							200
#define IDS_STRING300							300
#define IDS_STRING400							400
#define IDS_STRING500							500
#define IDS_STRING600							600
#define IDS_STRING700							700
#define IDS_STRING701							701

#define IDS_STRING800							800

#define IDS_STRING8010							8010
#define IDS_STRING8011							8011

#define IDS_STRING900							900

#define IDS_STRING1000							1000
#define IDS_STRING1001							1001
#define IDS_STRING1002							1002
#define IDS_STRING1003							1003
#define IDS_STRING1004							1004
#define IDS_STRING1005							1005

#define IDS_STRING1100							1100
#define IDS_STRING1101							1101
#define IDS_STRING1102							1102
#define IDS_STRING1103							1103
#define IDS_STRING1104							1104
#define IDS_STRING1105							1105

#define IDS_STRING1200							1200
#define IDS_STRING1201							1201
#define IDS_STRING1202							1202
#define IDS_STRING1203							1203
#define IDS_STRING1204							1204
#define IDS_STRING1205							1205
#define IDS_STRING1206							1206
#define IDS_STRING1207							1207

#define IDS_STRING1300							1300
#define IDS_STRING1301							1301
#define IDS_STRING1302							1302
#define IDS_STRING1303							1303
#define IDS_STRING1304							1304
#define IDS_STRING1305							1305
#define IDS_STRING1306							1306
#define IDS_STRING1307							1307
#define IDS_STRING1308							1308
#define IDS_STRING1309							1309
#define IDS_STRING1310							1310
#define IDS_STRING1311							1311
#define IDS_STRING1312							1312

#define IDS_STRING1400							1400
#define IDS_STRING1401							1401
#define IDS_STRING1402							1402
#define IDS_STRING1403							1403
#define IDS_STRING1404							1404
#define IDS_STRING1405							1405

#define IDS_STRING1500							1500
#define IDS_STRING1501							1501
#define IDS_STRING1502							1502

#define IDS_STRING1600							1600
#define IDS_STRING1601							1601

#define IDS_STRING1602							1602
#define IDS_STRING1603							1603
#define IDS_STRING1604							1604
#define IDS_STRING1605							1605

#define IDS_STRING1700							1700
#define IDS_STRING1701							1701
#define IDS_STRING1702							1702
#define IDS_STRING1703							1703
#define IDS_STRING1704							1704
#define IDS_STRING1705							1705
#define IDS_STRING1706							1706
#define IDS_STRING1707							1707
#define IDS_STRING1708							1708
#define IDS_STRING1709							1709
#define IDS_STRING1710							1710
#define IDS_STRING1711							1711
#define IDS_STRING1712							1712
#define IDS_STRING1713							1713

#define IDS_STRING1800							1800
#define IDS_STRING1801							1801

#define IDS_STRING1810							1810
#define IDS_STRING1811							1811

#define IDS_STRING1900							1900

#define IDS_STRING2000							2000
#define IDS_STRING2001							2001
#define IDS_STRING2002							2002
#define IDS_STRING2003							2003

#define IDS_STRING2100							2100

#define IDS_STRING2300							2300
#define IDS_STRING2301							2301


#define IDS_STRING3000							3000
#define IDS_STRING3001							3001
#define IDS_STRING3002							3002

#define IDS_STRING4000							4000
#define IDS_STRING4001							4001
#define IDS_STRING4002							4002

#define IDS_STRING4100							4100
#define IDS_STRING4101							4101
#define IDS_STRING4102							4102
#define IDS_STRING4103							4103
#define IDS_STRING4104							4104
#define IDS_STRING4105							4105
#define IDS_STRING4106							4106

#define IDS_STRING4200							4200
#define IDS_STRING4201							4201

#define IDS_STRING4202							4202
#define IDS_STRING4203							4203
#define IDS_STRING4204							4204

//////////////////////////////////////////////////////////////////////////////////////////
// Type of cost template; 071008 p�d
#define COST_TYPE_1							1	// "Kostnader f�r Rotpost"
#define COST_TYPE_2							2	// "Kostnader f�r Rotpost i LandValue"
#define COST_TYPE_NOT_DONE_2				-2	// "Kostnader f�r Rotpost i LandValue UNDER UTVECKLING"

/////////////////////////////////////////////////////////////////////////////////////////////////
// enumrated column values used in:

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3
};


//////////////////////////////////////////////////////////////////////////////////////////
// this enuerated type is used to set Action: New or Update; 071010 p�d
typedef enum { 
								COSTTMPL_NONE,
								COSTTMPL_NEW,
								COSTTMPL_OPEN,
						 } enumCostTmplState;

//////////////////////////////////////////////////////////////////////////////////////////
// Register keys

const LPCTSTR REG_COSTS_TEMPLATE_ENTRY_KEY				= _T("UMCosts\\CostsTemplateEntry\\Placement");

const LPCTSTR REG_COSTS_TEMPLATE_LIST_ENTRY_KEY			= _T("UMCosts\\CostsTemplateListEntry\\Placement");

const LPCTSTR REG_COSTS_INFR_TEMPLATE_ENTRY_KEY			= _T("UMCosts\\CostsInfrTemplateEntry\\Placement");

const LPCTSTR REG_COSTS_INFR_TEMPLATE_LIST_ENTRY_KEY	= _T("UMCosts\\CostsInfrTemplateListEntry\\Placement");

const LPCTSTR REG_PRINT_PRICELIST_KEY					= _T("UMAvgAssort\\PrintPricelists\\Placement");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_X_SIZE				= 670;
const int MIN_Y_SIZE				= 370;

const int MIN_X_SIZE_LIST			= 350;
const int MIN_Y_SIZE_LIST			= 200;

const int MIN_X_SIZE_INFR			= 700;
const int MIN_Y_SIZE_INFR			= 515;

const int MIN_X_SIZE_LIST_INFR		= 350;
const int MIN_Y_SIZE_LIST_INFR		= 200;

const int MIN_X_SIZE_PRINT_OUT		= 350;
const int MIN_Y_SIZE_PRINT_OUT		= 200;

// Use this root key in my own registry settings; 070628 p�d
const LPCTSTR PROGRAM_NAME					= _T("UMCosts");				// Used for Languagefile, registry entries etc.; 071004 p�d

const LPCTSTR SQL_SERVER_SQRIPT_DIRECTORY	= _T("Microsoft SQL Server");

const LPCTSTR COSTS_TEMPLATE_TABLES			= _T("Create_costs_template_table.sql");
//////////////////////////////////////////////////////////////////////////////////////////
// Name of tables in database; 070808 p�d
const LPCTSTR TBL_COSTS_TEMPLATE			= _T("cost_template_table");
const LPCTSTR TBL_SPECIES					= _T("fst_species_table");

const LPCTSTR TBL_TEMPLATE					= _T("tmpl_template_table");

const LPCTSTR TBL_TRAKT_MISC_DATA			= _T("esti_trakt_misc_data_table");

//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d

//const LPCTSTR TOOLBAR_RES_DLL									= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d
const LPCTSTR TOOLBAR_RES_DLL				= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_ADD						= 0;
const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;
const int RES_TB_DEL						= 28;
const int RES_TB_PREVIEW					= 31;
const int RES_TB_PRINT						= 36;

//////////////////////////////////////////////////////////////////////////////////////////
// Buffer
#define BUFFER_SIZE		1024*100		// 100 kb buffer

//////////////////////////////////////////////////////////////////////////////////////////
//
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");

//////////////////////////////////////////////////////////////////////////////////////////
// XML Template tags for "Trakt"; 071010 p�d
#ifdef UNICODE
const LPCTSTR XML_FILE_HEADER					= _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
#else
const LPCTSTR XML_FILE_HEADER					= _T("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
#endif

const LPCTSTR NODE_COSTTMPL_START				= _T("<cost_tmpl>");
const LPCTSTR NODE_COSTTMPL_END					= _T("</cost_tmpl>");

const LPCTSTR NODE_COSTTMPL_HEADER_START		= _T("<cost_tmpl_header>");
//const LPCTSTR NODE_COSTTMPL_NAME						= _T("<cost_tmpl_name>%s</cost_tmpl_name>");
//const LPCTSTR NODE_COSTTMPL_DONE_BY				= _T("<cost_tmpl_done_by>%s</cost_tmpl_done_by>");
//const LPCTSTR NODE_COSTTMPL_DATE						= _T("<cost_tmpl_date>%s</cost_tmpl_date>");
const LPCTSTR NODE_COSTTMPL_HEADER_END			= _T("</cost_tmpl_header>");

const LPCTSTR NODE_COSTTMPL_CUTTING_START		= _T("<cost_tmpl_cutting>");
const LPCTSTR NODE_COSTTMPL_CUTTING_CUT_1		= _T("<cost_tmpl_cut_1>%.1f</cost_tmpl_cut_1>");
const LPCTSTR NODE_COSTTMPL_CUTTING_CUT_2		= _T("<cost_tmpl_cut_2>%.1f</cost_tmpl_cut_2>");
const LPCTSTR NODE_COSTTMPL_CUTTING_CUT_3		= _T("<cost_tmpl_cut_3>%.1f</cost_tmpl_cut_3>");
const LPCTSTR NODE_COSTTMPL_CUTTING_CUT_4		= _T("<cost_tmpl_cut_4>%.1f</cost_tmpl_cut_4>");
const LPCTSTR NODE_COSTTMPL_CUTTING_END			= _T("</cost_tmpl_cutting>");

const LPCTSTR NODE_COSTTMPL_TRANSP_START		= _T("<cost_tmpl_transport>");
const LPCTSTR NODE_COSTTMPL_TRANSP_SPECIE		= _T("<cost_tmpl_transport_spc id=\"%d\" name=\"%s\" distance=\"%.1f\" cost=\"%.1f\" max_cost=\"%.1f\"/>");
const LPCTSTR NODE_COSTTMPL_TRANSP_END			= _T("</cost_tmpl_transport>");

const LPCTSTR NODE_COSTTMPL_OTHERS_START		= _T("<cost_tmpl_others>");
const LPCTSTR NODE_COSTTMPL_OTHERS_COST			= _T("<cost_tmpl_others_cost>%.1f</cost_tmpl_others_cost>");
const LPCTSTR NODE_COSTTMPL_OTHERS_TYPEOF		= _T("<cost_tmpl_others_typeof>%s</cost_tmpl_others_typeof>");
const LPCTSTR NODE_COSTTMPL_OTHERS_END			= _T("</cost_tmpl_others>");

//////////////////////////////////////////////////////////////////////////////////////////
// XML Template tags for "Objekt"; 080211 p�d

const LPCTSTR NODE_OBJ_COSTTMPL_START			= _T("<obj_cost_tmpl>");
const LPCTSTR NODE_OBJ_COSTTMPL_END				= _T("</obj_cost_tmpl>");
// Header information
const LPCTSTR NODE_OBJ_COSTTMPL_HEADER_START		= _T("<obj_cost_tmpl_header>");
const LPCTSTR NODE_OBJ_COSTTMPL_NAME_ITEM				= _T("<obj_cost_tmpl_name>%s</obj_cost_tmpl_name>");
const LPCTSTR NODE_OBJ_COSTTMPL_DONE_BY_ITEM		= _T("<obj_cost_tmpl_done_by>%s</obj_cost_tmpl_done_by>");
const LPCTSTR NODE_OBJ_COSTTMPL_DATE_ITEM				= _T("<obj_cost_tmpl_date>%s</obj_cost_tmpl_date>");
const LPCTSTR NODE_OBJ_COSTTMPL_HEADER_END			= _T("</obj_cost_tmpl_header>");
// Transport table "Transport fram till v�g ex. skotare"
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_START	= _T("<obj_cost_tmpl_transp>");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_ITEM		= _T("<transp_table %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_ATTR1	= _T("id=\"%d\" spc=\"%s\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_ATTR2	= _T("col%d=\"%d\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP_END		= _T("</obj_cost_tmpl_transp>");
// Transport table "Transport till Industri (Massaved)"
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP2_START	= _T("<obj_cost_tmpl_transp2>");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP2_ITEM	= _T("<transp2_table id=\"%d\" spc=\"%s\" distance=\"%.1f\" cost=\"%.2f\" max_cost=\"%.1f\" />");
const LPCTSTR NODE_OBJ_COSTTMPL_TRANSP2_END		= _T("</obj_cost_tmpl_transp2>");

const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_START	= _T("<obj_cost_tmpl_cutting set=\"%d\">");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_ITEM	= _T("<cutting_table %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_ATTR1	= _T("id=\"%d\" spc=\"%s\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_ATTR2	= _T("col%d=\"%d\" ");

const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_ITEM	= _T("<cutting_table_2 %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_ATTR1	= _T("id=\"%d\" spc=\"%s\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_ATTR2	= _T("col%d=\"%.2f\" ");

const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_CUT1	= _T("<cost_tmpl_cut_1>%.1f</cost_tmpl_cut_1>");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_CUT2	= _T("<cost_tmpl_cut_2>%.1f</cost_tmpl_cut_2>");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_CUT3	= _T("<cost_tmpl_cut_3>%.1f</cost_tmpl_cut_3>");
const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING2_CUT4	= _T("<cost_tmpl_cut_4>%.1f</cost_tmpl_cut_4>");

const LPCTSTR NODE_OBJ_COSTTMPL_CUTTING_END		= _T("</obj_cost_tmpl_cutting>");
/*	NOT USED 2008-03-31 P�D
const LPCTSTR NODE_OBJ_COSTTMPL_HCOSTS_START	= _T("<obj_cost_tmpl_hcosts>");
const LPCTSTR NODE_OBJ_COSTTMPL_HCOSTS_ITEM		= _T("<hcosts_table %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_HCOSTS_ATTR1	= _T("m3sk=\"%.0f\" price_m3sk=\"%.0f\" sum_price=\"%.0f\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_HCOSTS_END		= _T("</obj_cost_tmpl_hcosts>");
		NOT USED 2008-05-20 P�D
const LPCTSTR NODE_OBJ_COSTTMPL_OINFR_START		= _T("<obj_cost_tmpl_oinfr>");
const LPCTSTR NODE_OBJ_COSTTMPL_OINFR_ITEM		= _T("<oinfr_table %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_OINFR_ATTR1		= _T("price=\"%.0f\" note=\"%s\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_OINFR_END		= _T("</obj_cost_tmpl_oinfr>");
*/
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_START		= _T("<obj_cost_tmpl_other>");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_ITEM		= _T("<other_table %s/>");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_ATTR1_1	= _T("price=\"%.0f\" note=\"%s\" vat=\"%.0f\" ");
const LPCTSTR NODE_OBJ_COSTTMPL_OTHER_END			= _T("</obj_cost_tmpl_other>");

//////////////////////////////////////////////////////////////////////////////////////////
// Icons in Resource dll; 080211 p�d
const LPCTSTR RSTR_TB_IMPORT					= _T("Import");
const LPCTSTR RSTR_TB_EXPORT					= _T("Export");
const LPCTSTR RSTR_TB_ADD						= _T("Add");
const LPCTSTR RSTR_TB_DEL						= _T("Minus");


//////////////////////////////////////////////////////////////////////////////////////////
// Extension for exported/imported cost-templates; 081201 p�d
const LPCTSTR COSTTEMPLATE_FN_EXT				= _T(".hcxl");		// Uses ordinary xml-file; 081201 p�d

//////////////////////////////////////////////////////////////////////////////////////////
//	Create tables; 080701 p�d
const LPCTSTR table_Costs = _T("CREATE TABLE dbo.%s (")
													 _T("cost_id INT NOT NULL IDENTITY(1,1),")
													 _T("cost_name NVARCHAR(50),")
													 _T("cost_type_of int,")			/* Idetifies type of template; ex. 1 = Estimate rotpost, 2 = ELV rotpost */
													 _T("cost_template NTEXT,")			/* Template in xml format */
													 _T("cost_notes ntext,")			/* Noteringar f�r Mall */
													 _T("created_by NVARCHAR(20),")
													 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
													 _T("PRIMARY KEY (cost_id));");

const LPCTSTR PRL_VIEW_FN = _T("tmp_prl_view.htm");


class CTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:
	CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
	{
		m_sText = sValue;
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_sText = szText;
			SetValue(m_sText);
	}

	CString getTextItem(void)	
	{ 
		return m_sText; 
	}
	
	void setTextItem(LPCTSTR text)	
	{ 
		m_sText = text; 
		SetValue(m_sText);
	}
};


class CIntItem : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
public:
	CIntItem(void) : CXTPReportRecordItemNumber(0)
	{
		m_nValue = 0;
	}

	CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
	{
		m_nValue = nValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
	}

	int getIntItem(void)	
	{ 
		return m_nValue; 
	}
	
	void setIntItem(int value)	
	{ 
		m_nValue = value; 
		SetValue(m_nValue);
	}
};

class CFloatItem : public CXTPReportRecordItemNumber
{
//private:
	double m_fValue;
public:
	CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
			CXTPReportRecordItemNumber(fValue)
	{
		SetFormatString(fmt_str);
		m_fValue = fValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
	{
		m_fValue = (double)_tstof(szText);
		SetValue(m_fValue);
		CXTPReportControl *pCtrl = NULL;
		if (pItemArgs != NULL)
		{
			pCtrl = pItemArgs->pControl;
			if (pCtrl != NULL)
			{
				pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
			}
		}
	}

	void setFloatItem(double value)	
	{ 
		m_fValue = value; 
		SetValue(value);
	}
	double getFloatItem(void)	{ return m_fValue; }
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CCheckItem : public CXTPReportRecordItem
{
public:
	// Constructs record item with the initial checkbox value.
	CCheckItem(BOOL bCheck)
	{
		HasCheckbox(TRUE);
		SetChecked(bCheck);
	}

	virtual BOOL getChecked(void)
	{
		return IsChecked()? TRUE: FALSE;
	}

	virtual void setChecked(BOOL bCheck)
	{
		SetChecked(bCheck);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CTransportReportRec_DGV

class CTransportReportRec_DGV : public CXTPReportRecord
{
	//private:
protected:
	
public:
	CTransportReportRec_DGV(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CTransportReportRec_DGV(int idx)
	{
		if (idx == 0)
		{
			AddItem(new CTextItem(_T("")));
		}
		else
		{
			AddItem(new CIntItem());
		}
	}

	CTransportReportRec_DGV(int num,LPCTSTR value)
	{
		for (int i = 0;i < num;i++)
		{
			if (i == 0)
			{
				AddItem(new CTextItem(value));
			}
			else
			{
				AddItem(new CIntItem());
			}
		}
	}

	CTransportReportRec_DGV(CTransaction_transport_data rec)
	{
		AddItem(new CTextItem(rec.getSpcName()));
		if (rec.getInts().size() > 0)
		{
			for (UINT i = 0;i < rec.getInts().size();i++)
			{
				AddItem(new CIntItem(rec.getInts()[i]));
			}
		}
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			default : sData.Format(_T("%d"),getColumnInt(item)); break;
		};
		return sData;
	}

	int getColumnInt(int item)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}
	
	void setColumnInt(int item,int value)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			((CIntItem*)GetItem(item))->setIntItem(value);
	}

};


/////////////////////////////////////////////////////////////////////////////
// CTransportReportRec_M3FUB

class CTransportReportRec_M3FUB : public CXTPReportRecord
{
	//private:
protected:
	
public:
	CTransportReportRec_M3FUB(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CTransportReportRec_M3FUB(int idx)
	{
		if (idx == 0)
		{
			AddItem(new CTextItem(_T("")));
		}
		else
		{
			AddItem(new CFloatItem(0.0,sz1dec));
		}
	}

	CTransportReportRec_M3FUB(int num,LPCTSTR value,LPCTSTR dec)
	{
		for (int i = 0;i < num;i++)
		{
			if (i == 0)
			{
				AddItem(new CTextItem(value));
			}
			else
			{
				AddItem(new CFloatItem(0.0,dec));
			}
		}
	}

	CTransportReportRec_M3FUB(CTransaction_transport_data rec)
	{
		AddItem(new CTextItem(rec.getSpcName()));
		if (rec.getFloats().size() > 0)
		{
			for (UINT i = 0;i < rec.getFloats().size();i++)
			{
				AddItem(new CFloatItem(rec.getFloats()[i]));
			}
		}
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			default : sData.Format(_T("%.1f"),getColumnFloat(item)); break;
		};
		return sData;
	}

	double getColumnFloat(int item)	
	{ 
		if ((CFloatItem*)GetItem(item) != NULL)
			return ((CFloatItem*)GetItem(item))->getFloatItem();
		else
			return 0;
	}
	
	void setColumnFloat(int item,double value)	
	{ 
		if ((CFloatItem*)GetItem(item) != NULL)
			((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

};


/////////////////////////////////////////////////////////////////////////////
// CCuttingReportRec

class CCuttingReportRec : public CXTPReportRecord
{
	//private:
protected:

public:
	CCuttingReportRec(void)
	{
		AddItem(new CTextItem(_T("")));
	}

	CCuttingReportRec(int idx)
	{
		if (idx == 0)
		{
			AddItem(new CTextItem(_T("")));
		}
		else
		{
			AddItem(new CIntItem());
		}
	}

	CCuttingReportRec(int num,LPCTSTR value)
	{
		for (int i = 0;i < num;i++)
		{
			if (i == 0)
			{
				AddItem(new CTextItem(value));
			}
			else
			{
				AddItem(new CIntItem());
			}
		}
	}

	CCuttingReportRec(CTransaction_transport_data rec)
	{
		AddItem(new CTextItem(rec.getSpcName()));
		if (rec.getInts().size() > 0)
		{
			for (UINT i = 0;i < rec.getInts().size();i++)
			{
				AddItem(new CIntItem(rec.getInts()[i]));
			}
		}
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

// Special method; returns all colum items as text; 060405 p�d
	CString getColumnsInText(int item)	
	{ 
		CString sData;
		switch (item)
		{
			case 0 : sData = getColumnText(item); break;
			default : sData.Format(_T("%d"),getColumnInt(item)); break;
		};
		return sData;
	}

	int getColumnInt(int item)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}
	
	void setColumnInt(int item,int value)	
	{ 
		if ((CIntItem*)GetItem(item) != NULL)
			((CIntItem*)GetItem(item))->setIntItem(value);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CHigherCostReportRec

class CHigherCostReportRec : public CXTPReportRecord
{
	//private:
protected:

	
public:
	CHigherCostReportRec(void)
	{
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CFloatItem(0.0,(sz0dec)));
	}

	CHigherCostReportRec(double volume_m3sk,double price,double sum_price)
	{
		AddItem(new CFloatItem(volume_m3sk,(sz0dec)));
		AddItem(new CFloatItem(price,(sz0dec)));
		AddItem(new CFloatItem(sum_price,(sz0dec)));
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

};


/////////////////////////////////////////////////////////////////////////////
// COtherInfrCostsReportRec
class COtherInfrCostsReportRec : public CXTPReportRecord
{
	//private:
protected:
	
public:
	COtherInfrCostsReportRec(void)
	{
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CTextItem(_T("")));
	}

	COtherInfrCostsReportRec(double value,LPCTSTR text)
	{
		AddItem(new CFloatItem(value,sz0dec));
		AddItem(new CTextItem(text));
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

/////////////////////////////////////////////////////////////////////////////
// COtherCostsReportRec
class COtherCostsReportRec : public CXTPReportRecord
{
	//private:
protected:
	
public:
	COtherCostsReportRec(void)
	{
		AddItem(new CFloatItem(0.0,(sz0dec)));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz0dec));
	}

	COtherCostsReportRec(double value,LPCTSTR text,double vat)
	{
		AddItem(new CFloatItem(value,(sz0dec)));
		AddItem(new CTextItem(text));
		AddItem(new CFloatItem(vat,sz0dec));
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}
};

/////////////////////////////////////////////////////////////////////////////
// CTemplateListReportDataRec

class CTemplateListReportDataRec : public CXTPReportRecord
{
	int m_nID;
protected:

public:

	CTemplateListReportDataRec(void)
	{
		m_nID	= -1;	
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CTemplateListReportDataRec(int id,LPCTSTR tmpl_name,LPCTSTR done_by,LPCTSTR date)
	{
		m_nID	= id;
		AddItem(new CTextItem(tmpl_name));
		AddItem(new CTextItem(done_by));
		AddItem(new CTextItem(date));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

/////////////////////////////////////////////////////////////////////////////
// CSpecieReportRec

class CSpecieReportRec : public CXTPReportRecord
{
protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_fValue = (double)_tstof(szText);
				SetValue(m_fValue);
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CSpecieReportRec(void)
	{
		AddItem(new CTextItem(_T("")));					// Name of Specie 
		AddItem(new CFloatItem(0.0,sz1dec));		// Distance in km
		AddItem(new CFloatItem(0.0,sz1dec));		// Cost in kr/m3fub
		AddItem(new CFloatItem(0.0,sz1dec));		// Max Cost in kr/m3fub
	}

	CSpecieReportRec(LPCTSTR spc_name,double distance,double cost,double max_cost)
	{
		AddItem(new CTextItem((spc_name)));				// Name of Specie
		AddItem(new CFloatItem(distance,sz1dec));		// // Distance in km
		AddItem(new CFloatItem(cost,sz2dec));				// Cost in kr/m3fub
		AddItem(new CFloatItem(max_cost,sz1dec));		// Max cost in kr/m3fub
	}

	double getColumnFloat(int item)	
	{ 
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item,double value)	
	{ 
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	CString getColumnText(int item)	
	{ 
		if (item >= 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item >= 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}
};



//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
CString getToolBarResourceFN(void);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp);
CView *getFormViewByID(int idd);

CString formatData(LPCTSTR,...);

void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);
