#pragma once

#include "UMCostsTmplDB.h"

#include "Resource.h"

// CRemainingFormView form view

class CRemainingFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CRemainingFormView)
	BOOL m_bInitialized;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CXTResizeGroupBox m_wndGrpOther3_3;

	CXTButton m_wndBtnAddRow2;
	CXTButton m_wndBtnDelRow2;

	enumCostTmplState m_enumCostTmplState;

	vecTransaction_costtempl m_vecTransaction_costtempl;
	void getCostTemplateFromDB();

	void setEnable(BOOL);

	void setupNewCostTemplate(void);

	// Database connection datamemebers; 070228 p�d
	CUMCostsTmplDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CRemainingFormView();           // protected constructor used by dynamic creation
	virtual ~CRemainingFormView();

	CMyReportCtrl m_wndReport3;	// "�vriga fasta kostnader, kan v�lja om det skall l�ggas p� Moms p� kostnad"
	BOOL setupReport3(void);

public:
	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

		BOOL getOtherCostsTable(CString&);
		void setOtherCostTable(vecObjectCostTemplate_other_cost_table&);

		void setLockRemainingView(BOOL lock)		
		{		
			m_wndReport3.EnableWindow(!lock);	
			m_wndBtnAddRow2.EnableWindow(!lock);
			m_wndBtnDelRow2.EnableWindow(!lock);
		}

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CPageOneFormView)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddRow4();
	afx_msg void OnBnClickedDelRow4();
	afx_msg void OnBnClickedAddRow5();
	afx_msg void OnBnClickedDelRow5();
};


