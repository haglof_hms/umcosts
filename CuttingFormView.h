#pragma once

#include "UMCostsTmplDB.h"

#include "Resource.h"

// CCuttingFormView form view

class CCuttingFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CCuttingFormView)

	BOOL m_bInitialized;

	BOOL m_bIsDataEnabled;

	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sSpcDGV;
	CString m_sAvgTrunkM3FUB;
	CString m_sPriceM3FUB;

	CString m_sGrpHeadDGV;
	CString m_sGrpHeadM3FUB;

	CXTResizeGroupBox m_wndGrpCutting;
	CXTResizeGroupBox m_wndGrpCutting2;
	CXTResizeGroupBox m_wndGrpCutting3;
	CXTResizeGroupBox m_wndGrpCutting4;

	CMyExtStatic m_wndLbl1;
	CMyExtEdit m_wndEdit1;

	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;

	CXTButton m_wndBtnAddCols;
	CXTButton m_wndBtnDelCol;
	CXTButton m_wndBtnAddRow;
	CXTButton m_wndBtnDelRow;

	CButton m_wndRBtn1;
	CButton m_wndRBtn2;
	CButton m_wndRBtn3;

	enumCostTmplState m_enumCostTmplState;

	vecTransaction_costtempl m_vecTransaction_costtempl;

	vecTransactionSpecies m_vecTransactionSpecies;
	void getSpeciesFromDB(void);
	int getSpecieID(LPCTSTR spc_name);
	CString getSpcNameFromDBSpecies(int id);

	void getCostTemplateFromDB(void);

	void setEnable(BOOL enabled)
	{
		m_wndEdit1.EnableWindow(enabled);
		m_wndEdit1.SetReadOnly(!enabled);

		m_wndBtnAddCols.EnableWindow(enabled);
		m_wndBtnDelCol.EnableWindow(enabled);
		m_wndBtnAddRow.EnableWindow(enabled);
		m_wndBtnDelRow.EnableWindow(enabled);

		m_bIsDataEnabled = enabled;
	}


	void setEnable2(BOOL alt1,BOOL alt2,BOOL alt3,BOOL enabled=TRUE)
	{
		// Enable/Disable "Huggningskostnader i kr/m3"; 080313 p�d
		m_wndEdit2.EnableWindow( alt1 && enabled );
		m_wndEdit2.SetReadOnly(!(alt1 && enabled));
		m_wndEdit3.EnableWindow( alt1 && enabled );
		m_wndEdit3.SetReadOnly(!(alt1 && enabled));
		m_wndEdit4.EnableWindow( alt1 && enabled );
		m_wndEdit4.SetReadOnly(!(alt1 && enabled));
		m_wndEdit5.EnableWindow( alt1 && enabled );
		m_wndEdit5.SetReadOnly(!(alt1 && enabled));
		m_wndLbl2.EnableWindow( alt1 && enabled );
		m_wndLbl3.EnableWindow( alt1 && enabled );
		m_wndLbl4.EnableWindow( alt1 && enabled );
		m_wndLbl5.EnableWindow( alt1 && enabled );

		// Enable/Disable "Huggningskostnader i tabell (dgv)"; 080313 p�d
		// This table can also be used for adding "Huggningskostnader f�r medelstam (m3fub)"; 091106 p�d
		if ((alt2 || alt3) && enabled)
		{
			m_wndEdit1.EnableWindow( TRUE );
			m_wndEdit1.SetReadOnly(FALSE);
			m_wndBtnAddCols.EnableWindow( TRUE );
			m_wndBtnDelCol.EnableWindow( TRUE );
			m_wndBtnAddRow.EnableWindow( TRUE );
			m_wndBtnDelRow.EnableWindow( TRUE );
			m_wndReport_DGV.EnableWindow( TRUE );
			m_wndReport_M3FUB.EnableWindow( TRUE );
			m_wndLbl1.EnableWindow( TRUE );
		}
		else
		{
			m_wndEdit1.EnableWindow( FALSE );
			m_wndEdit1.SetReadOnly();
			m_wndBtnAddCols.EnableWindow( FALSE );
			m_wndBtnDelCol.EnableWindow( FALSE );
			m_wndBtnAddRow.EnableWindow( FALSE );
			m_wndBtnDelRow.EnableWindow( FALSE );
			m_wndReport_DGV.EnableWindow( FALSE );
			m_wndReport_M3FUB.EnableWindow( FALSE );
			m_wndLbl1.EnableWindow( FALSE );
		}

		if (alt2)
		{
			m_wndReport_DGV.ShowWindow(SW_NORMAL);
			m_wndReport_M3FUB.ShowWindow(SW_HIDE);
		}
		if (alt3)
		{
			m_wndReport_M3FUB.ShowWindow(SW_NORMAL);
			m_wndReport_DGV.ShowWindow(SW_HIDE);
		}

		if (alt1)
				m_wndEdit2.SetFocus();
		else if (alt2 || alt3)
				m_wndEdit1.SetFocus();

	}

	// Database connection datamemebers; 070228 p�d
	CUMCostsTmplDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
protected:
	CCuttingFormView();           // protected constructor used by dynamic creation
	virtual ~CCuttingFormView();

	CMyReportCtrl m_wndReport_DGV;
	CMyReportCtrl m_wndReport_M3FUB;
	BOOL setupReport(short set_rep = 0);


public:
	enum { IDD = IDD_FORMVIEW5 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	void setupNewCostTemplate(short set_item = -1);
	// Set/Get from DB
	BOOL getCuttingTable(CString&,int*);
	//Lagt till m_bDoLock som argument f�r att veta om huggningskostnader kall s�ttas aktiv eller ej
	//Bug #2544 20111117 J�
	void setCuttingTable(vecObjectCostTemplate_table &,vecObjectCostTemplate_table &,CTransaction_costtempl_cutting &,int set_as,int bDoLock);

	// Add species from selection dialog; 080325 p�d
	void addSpecies(vecTransactionSpecies &);
	void delSpecie(void);

	void setLockCuttingView(BOOL lock)
	{
		m_wndEdit1.EnableWindow(!lock);
		m_wndEdit2.EnableWindow(!lock);
		m_wndEdit3.EnableWindow(!lock);
		m_wndEdit4.EnableWindow(!lock);
		m_wndEdit5.EnableWindow(!lock);

		m_wndBtnAddCols.EnableWindow(!lock);
		m_wndBtnDelCol.EnableWindow(!lock);
		m_wndBtnAddRow.EnableWindow(!lock);
		m_wndBtnDelRow.EnableWindow(!lock);

		m_wndRBtn1.EnableWindow(!lock);
		m_wndRBtn2.EnableWindow(!lock);
		m_wndRBtn3.EnableWindow(!lock);

		m_wndReport_DGV.EnableWindow(!lock);
		m_wndReport_M3FUB.EnableWindow(!lock);
	}

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CPageOneFormView)
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnReport1Click(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReport1KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReport2Click(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnReport2KeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddCols();
	afx_msg void OnBnClickedDelCol();
	afx_msg void OnBnClickedAddRow();
	afx_msg void OnBnClickedDelRow();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();
};


