// AddToTemplateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AddToTemplateDlg.h"

#include "ResLangFileReader.h"

// CAddToTemplateDlg dialog

IMPLEMENT_DYNAMIC(CAddToTemplateDlg, CDialog)

BEGIN_MESSAGE_MAP(CAddToTemplateDlg, CDialog)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDOK, &CAddToTemplateDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CAddToTemplateDlg::CAddToTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddToTemplateDlg::IDD, pParent)
{
	m_pDB = NULL;
	m_nActiveCostID = -1;
}

CAddToTemplateDlg::~CAddToTemplateDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CAddToTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP

}

BOOL CAddToTemplateDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


BOOL CAddToTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupReport();

	populateData();

	return 0;
}

BOOL CAddToTemplateDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CUMCostsTmplDB(m_dbConnectionData);
		}

	}
	return CDialog::OnCopyData(pWnd, pData);
}

// CAddToTemplateDlg message handlers
// CTransportFormView message handlers
BOOL CAddToTemplateDlg::setupReport(void)
{

	if (m_wndReport.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_wndReport.Create(this, IDC_REPORT_4, TRUE, FALSE))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
	}

	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				if (m_wndReport.GetSafeHwnd() != NULL)
				{

					SetWindowText((xml->str(IDS_STRING1700)));
					m_wndLbl1.SetWindowText((xml->str(IDS_STRING1707)));
					m_wndBtnOK.SetWindowText((xml->str(IDS_STRING1705)));
					m_wndBtnCancel.SetWindowText((xml->str(IDS_STRING1706)));
				
					// Setup interface strings; 080207 p�d
					m_wndReport.ShowWindow( SW_NORMAL );

					// Set Dialog caption; 070219 p�d

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING500)),200,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING600)),100,FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING700)),100,FALSE));
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
					m_wndReport.GetReportHeader()->AllowColumnReorder(FALSE);
					m_wndReport.GetReportHeader()->AllowColumnResize( TRUE );
					m_wndReport.GetReportHeader()->AllowColumnSort( FALSE );
					m_wndReport.GetReportHeader()->SetAutoColumnSizing( TRUE );
					m_wndReport.SetMultipleSelection( FALSE );
					m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
					m_wndReport.AllowEdit(FALSE);
					m_wndReport.FocusSubItems(TRUE);

					CRect rect;
					GetClientRect(&rect);
					setResize(&m_wndReport,2,30,rect.right - 4,rect.bottom - 70);

				}	// if (m_wndReport.GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}


void CAddToTemplateDlg::populateData(void)
{
	vecTransaction_costtempl vecCosttempl;
	CTransaction_costtempl rec;
	if (m_pDB != NULL)
	{
		m_pDB->getCostTmplsExclude(vecCosttempl,m_nActiveCostID);
	}

	if (vecCosttempl.size() > 0)
	{
		m_wndReport.ResetContent();
		for (UINT i = 0;i < vecCosttempl.size();i++)
		{
			rec = vecCosttempl[i];
			m_wndReport.AddRecord(new CTemplateListReportDataRec(rec.getID(),rec.getTemplateName(),rec.getCreatedBy(),rec.getCreated()));
		}

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}

}

void CAddToTemplateDlg::OnBnClickedOk()
{
	vecTransaction_costtempl vecCosttempl;
	CTransaction_costtempl rec;
	CTemplateListReportDataRec *pRec = NULL;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		// Reload
		if (m_pDB != NULL)
		{
			m_pDB->getCostTmplsExclude(vecCosttempl,m_nActiveCostID);
		}
		pRec = (CTemplateListReportDataRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_costtmplSelected = CTransaction_costtempl();	// Reset
			for (UINT i = 0;i < vecCosttempl.size();i++)
			{
				rec = vecCosttempl[i];
				if (rec.getID() == pRec->getID())
				{
					m_costtmplSelected = rec;
					break;
				}	// if (rec.getID() == pRec->getID())
			}	// for (UINT i = 0;i < m_vecTransaction_costtempl.size();i++)
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)

	
	OnOK();
}
